package com.fennix.android.prod.model.eventbus;

public class EventBusAddComponent {
    private String componentHtml;

    public EventBusAddComponent(String componentHtml) {
        this.componentHtml = componentHtml;
    }

    public String getComponentHtml() {
        return componentHtml;
    }

    public void setComponentHtml(String componentHtml) {
        this.componentHtml = componentHtml;
    }
}
