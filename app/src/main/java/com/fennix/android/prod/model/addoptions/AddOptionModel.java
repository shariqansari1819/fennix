package com.fennix.android.prod.model.addoptions;

import android.widget.TextView;

public class AddOptionModel {
    private String mTvAddOptions;
    private Boolean isSelected;

    public AddOptionModel(String mTvAddOptions, Boolean isSelected) {
        this.mTvAddOptions = mTvAddOptions;
        this.isSelected = isSelected;
    }


    public String getmTvAddOptions() {
        return mTvAddOptions;
    }

    public void setmTvAddOptions(String mTvAddOptions) {
        this.mTvAddOptions = mTvAddOptions;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }
}
