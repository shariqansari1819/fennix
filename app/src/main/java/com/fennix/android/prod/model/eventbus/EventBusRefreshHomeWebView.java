package com.fennix.android.prod.model.eventbus;

public class EventBusRefreshHomeWebView {
    private boolean refreshWebView;

    public EventBusRefreshHomeWebView(boolean refreshWebView) {
        this.refreshWebView = refreshWebView;
    }

    public boolean isRefreshWebView() {
        return refreshWebView;
    }

    public void setRefreshWebView(boolean refreshWebView) {
        this.refreshWebView = refreshWebView;
    }
}
