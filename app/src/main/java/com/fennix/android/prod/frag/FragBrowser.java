package com.fennix.android.prod.frag;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fennix.android.prod.databinding.FragBrowserBinding;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;

public class FragBrowser extends Fragment {

    private FragBrowserBinding mBind;

    public FragBrowser() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_browser, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
//        if (getArguments().getString("mFlag", "").contentEquals("terms")) {
//            ((MainActivity) getActivity()).mFScreenName = "TERMS";
//        }
        mBind.mWebView.getSettings().setJavaScriptEnabled(true);
        mBind.mWebView.getSettings().setLoadWithOverviewMode(true);
        mBind.mWebView.getSettings().setUseWideViewPort(true);
        mBind.mWebView.getSettings().setBuiltInZoomControls(true);
        mBind.mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mBind.mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (!mBind.mPb.isShown()) {
                    mBind.mPb.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (mBind.mPb.isShown()) {
                    mBind.mPb.setVisibility(View.GONE);
                }
            }
        });

        if (getArguments().getString("mFlag").contentEquals("browser")) {
            if (GlobalClass.getInstance().mPref.getString("language", "").contentEquals("en")) {
                mBind.mWebView.loadUrl("https://english.creamisitioweb.com/");
            } else {
                mBind.mWebView.loadUrl("https://spanish.creamisitioweb.com/");
            }
        }
    }

}