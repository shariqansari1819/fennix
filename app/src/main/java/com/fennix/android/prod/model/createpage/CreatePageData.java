
package com.fennix.android.prod.model.createpage;

public class CreatePageData {

    private String new_page_id;

    public String getNew_page_id() {
        return new_page_id;
    }

    public void setNew_page_id(String new_page_id) {
        this.new_page_id = new_page_id;
    }

}
