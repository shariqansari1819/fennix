package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.databinding.FragLanguageBinding;
import com.fennix.android.prod.utils.GlobalClass;


public class FragLanguage extends Fragment implements View.OnClickListener {

    private FragLanguageBinding mBind;

    public FragLanguage() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_language, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mTvEnglish.setOnClickListener(this);
        mBind.mTvEspanol.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ((MainActivity) getActivity()).mFragManager.popBackStack();
        switch (view.getId()) {
            case R.id.mTvEnglish:
                GlobalClass.getInstance().mPrefEditor.putString("language", "en").apply();
                ((MainActivity) getActivity()).fnLanguageChange("en");
                if (getArguments().getString("mFlag", "").equals("splash")) {
                    ((MainActivity) getActivity()).fnLoadFragReplace("LOGIN", null);
                } else {
                    ((MainActivity) getActivity()).mFragManager.popBackStack();
                    ((MainActivity) getActivity()).mPosPrevious = 5;
                    ((MainActivity) getActivity()).mBind.mImgProfile.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    ((MainActivity) getActivity()).fnLoadFragReplace("PROFILE", null);
                }
                break;
            case R.id.mTvEspanol:
                GlobalClass.getInstance().mPrefEditor.putString("language", "es").apply();
                ((MainActivity) getActivity()).fnLanguageChange("es");
                if (getArguments().getString("mFlag", "").equals("splash")) {
                    ((MainActivity) getActivity()).fnLoadFragReplace("LOGIN", null);
                } else {
                    ((MainActivity) getActivity()).mFragManager.popBackStack();
                    ((MainActivity) getActivity()).mPosPrevious = 5;
                    ((MainActivity) getActivity()).mBind.mImgProfile.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                    ((MainActivity) getActivity()).fnLoadFragReplace("PROFILE", null);
                }
                break;
        }
    }

}