package com.fennix.android.prod.frag;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.ProductAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragCouponEditAddBinding;
import com.fennix.android.prod.model.addcoupons.AddCouponMainObject;
import com.fennix.android.prod.model.coupons.getcoupons.updatecoupon.GetCouponDetailMainObject;
import com.fennix.android.prod.model.coupons.getcoupons.updatecoupon.GetDetailCouponData;
import com.fennix.android.prod.model.eventbus.EventBusGetCoupons;
import com.fennix.android.prod.model.products.ProductsData;
import com.fennix.android.prod.model.products.ProductsMainObject;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragCouponEdit extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private FragCouponEditAddBinding mBind;
    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialog1;
    private ListView listViewProducts;
    private ProgressBar progressBarProducts;

    private String couponEditType = "";
    private String couponId;
    private String appliedProductId = "";

    private Call<AddCouponMainObject> mCallAddCoupon;
    private Call<GetCouponDetailMainObject> mCallCouponDetail;
    private Call<AddCouponMainObject> mCallUpdateProducts;
    private Call<ProductsMainObject> mCallProducts;

    private GetCouponDetailMainObject getCouponDetailMainObject;
    private List<ProductsData> productsDataList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private AlertDialog productsDialog;

    public FragCouponEdit() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_coupon_edit_add, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getArguments() != null) {
            couponEditType = getArguments().getString("coupon_edit_type", "");
            if (couponEditType != null) {
                if (couponEditType.equals("add")) {
                    mBind.mTvUsed.setVisibility(View.GONE);
                } else {
                    mBind.mTvUsed.setVisibility(View.VISIBLE);
                    couponId = getArguments().getString("coupon_id", "");
                    if (!couponId.equals("")) {
                        reqDetails(couponId);
                    }
                }
            }
        }
        layoutInflater = LayoutInflater.from(getActivity());

        mBind.mImgDate.setOnClickListener(this);
        mBind.mImgEndDate.setOnClickListener(this);
        mBind.mImgBack.setOnClickListener(this);
        mBind.mImgTick.setOnClickListener(this);
        mBind.mImgArrowAplyTo.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallAddCoupon != null && mCallAddCoupon.isExecuted()) {
            mCallAddCoupon.cancel();
        }
        if (mCallCouponDetail != null && mCallCouponDetail.isExecuted()) {
            mCallCouponDetail.cancel();
        }
        if (mCallProducts != null && mCallProducts.isExecuted()) {
            mCallProducts.cancel();
        }
        if (mCallUpdateProducts != null && mCallUpdateProducts.isExecuted()) {
            mCallUpdateProducts.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgDate:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                mBind.mTvDate.setText(year + "-" + (monthOfYear + 1) + "-" + (dayOfMonth));


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.mImgEndDate:
                final Calendar c1 = Calendar.getInstance();
                int mYear1 = c1.get(Calendar.YEAR);
                int mMonth1 = c1.get(Calendar.MONTH);
                int mDay1 = c1.get(Calendar.DAY_OF_MONTH);
                datePickerDialog1 = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                mBind.mTvEndDatee.setText(year + "-" + (monthOfYear + 1) + "-" + (dayOfMonth));

                            }
                        }, mYear1, mMonth1, mDay1);
                datePickerDialog1.show();
                break;
            case R.id.mImgBack:
                getActivity().onBackPressed();
                break;
            case R.id.mImgTick:
                if (couponEditType.equals("add")) {
                    if (mBind.mPb.getVisibility() == View.VISIBLE) {
                        return;
                    }
                    if (!((MainActivity) getActivity()).fnIsisOnline()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        return;
                    }
                    if (mBind.mEtCouponCode == null || mBind.mEtCouponCode.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_code), R.color.error);
                    } else if (mBind.mEtCouponName == null || mBind.mEtCouponName.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_name), R.color.error);
                    } else if (mBind.mEtAmount == null || mBind.mEtAmount.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_amount), R.color.error);
                    } else if (mBind.mTvDate.getText().toString().equals("None")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_start_date), R.color.error);
                    } else {
//                        boolean isEndDateGreater = true;
//                        if (!mBind.mTvEndDatee.getText().toString().equals("None")) {
//                            if (!isDateAfter(mBind.mTvDate.getText().toString(), mBind.mTvEndDatee.getText().toString())) {
//                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.start_date_greater_than_end_date), R.color.error);
//                                isEndDateGreater = false;
//                            }
//                        }
                        reqAddCoupon(GlobalClass.getInstance().mPref.getString("site_id", ""), mBind.mEtCouponCode.getText().toString(), mBind.mEtCouponName.getText().toString(),
                                mBind.mEtAmount.getText().toString(), appliedProductId,
                                mBind.mRgDiscount.getCheckedRadioButtonId() == mBind.mRbtnFixed.getId() ? "1" : "0",
                                mBind.mScActive.isChecked() ? "1" : "0", mBind.mTvDate.getText().toString(), mBind.mTvEndDatee.getText().toString());

                    }
                } else {
                    if (mBind.mPb.getVisibility() == View.VISIBLE) {
                        return;
                    }
                    if (!((MainActivity) getActivity()).fnIsisOnline()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        return;
                    }
                    if (mBind.mEtCouponCode == null || mBind.mEtCouponCode.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_code), R.color.error);
                    } else if (mBind.mEtCouponName == null || mBind.mEtCouponName.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_name), R.color.error);
                    } else if (mBind.mEtAmount == null || mBind.mEtAmount.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_coupon_amount), R.color.error);
                    } else if (mBind.mTvDate.getText().toString().equals("None")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_start_date), R.color.error);
                    } else {
//                        boolean isEndDateGreater = true;
//                        if (!mBind.mTvEndDatee.getText().toString().equals("None")) {
//                            if (!isDateAfter(mBind.mTvDate.getText().toString(), mBind.mTvEndDatee.getText().toString())) {
//                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.start_date_greater_than_end_date), R.color.error);
//                                isEndDateGreater = false;
//                            }
//                        }
                        reqUpdateCoupon(GlobalClass.getInstance().mPref.getString("site_id", ""), couponId, mBind.mEtCouponCode.getText().toString(), mBind.mEtCouponName.getText().toString(),
                                mBind.mEtAmount.getText().toString(), appliedProductId,
                                mBind.mRgDiscount.getCheckedRadioButtonId() == mBind.mRbtnFixed.getId() ? "1" : "0",
                                mBind.mScActive.isChecked() ? "1" : "0", mBind.mTvDate.getText().toString(), mBind.mTvEndDatee.getText().toString());

                    }
                }
                break;
            case R.id.mImgArrowAplyTo:
                AlertDialog.Builder productsDialogBuilder = new AlertDialog.Builder(getActivity());
                final View productsDialogView = layoutInflater.inflate(R.layout.dialog_pages_list, null);
                productsDialogBuilder.setView(productsDialogView);
                productsDialog = productsDialogBuilder.create();
                productsDialog.show();
                listViewProducts = productsDialogView.findViewById(R.id.mLstPages);
                progressBarProducts = productsDialogView.findViewById(R.id.mPb);
                if (productsDataList.size() != 0) {
                    setSiteProducts();
                } else {
                    getProducts();
                }
                TextView textViewCancel = productsDialogView.findViewById(R.id.mTvCancel);
                listViewProducts.setOnItemClickListener(this);
                textViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        productsDialog.dismiss();
                    }
                });
                break;
        }

    }

    private void setSiteProducts() {
        String arr[] = new String[productsDataList.size() + 1];
        for (int i = 0; i < productsDataList.size(); i++) {
            arr[i] = productsDataList.get(i).getPro_title();
        }
        arr[productsDataList.size()] = "All Products";
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, arr);
        if (listViewProducts != null)
            listViewProducts.setAdapter(arrayAdapter);
    }

    public static boolean isDateAfter(String startDate, String endDate) {
        try {
            String myFormatString = "yyyy-M-dd";
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date date = df.parse(endDate);
            Date startingDate = df.parse(startDate);
            if (date.before(startingDate))
                return false;
            else
                return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void reqAddCoupon(String siteId, String couponCode, String couponName, String
            couponAmount, String couponApplied,
                             String couponType, String couponStatus, String couponStart, String couponExpiry) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallAddCoupon = Api.WEB_SERVICE.add_coupon_list("55555", siteId, couponName, couponCode, couponType, couponAmount,
                    couponStart, couponStatus, couponApplied, couponExpiry);
            mCallAddCoupon.enqueue(new Callback<AddCouponMainObject>() {
                @Override
                public void onResponse(Call<AddCouponMainObject> call, Response<AddCouponMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.coupon_add), R.color.success);
                                EventBus.getDefault().post(new EventBusGetCoupons());
                                getActivity().onBackPressed();
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<AddCouponMainObject> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void reqDetails(String couponId) {
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallCouponDetail = Api.WEB_SERVICE.coupon_detail("55555", couponId);
        mCallCouponDetail.enqueue(new Callback<GetCouponDetailMainObject>() {
            @Override
            public void onResponse(Call<GetCouponDetailMainObject> call, Response<GetCouponDetailMainObject> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            getCouponDetailMainObject = response.body();
                            if (getCouponDetailMainObject != null) {
                                GetDetailCouponData getDetailCouponData = getCouponDetailMainObject.getData();
                                if (getCouponDetailMainObject != null) {
                                    mBind.mEtCouponCode.setText(getDetailCouponData.getCoupon_code());
                                    mBind.mEtCouponName.setText(getDetailCouponData.getCoupon_name());
                                    mBind.mEtAmount.setText(getDetailCouponData.getCoupon_amount());

                                    if (getDetailCouponData.getCoupon_type().equals("0")) {
                                        mBind.mRbtnPercentage.setChecked(true);
                                    } else {
                                        mBind.mRbtnFixed.setChecked(true);
                                    }
                                    mBind.mScActive.setChecked(!getDetailCouponData.getCoupon_status().equals("0"));
                                    mBind.mTvDate.setText(getDetailCouponData.getCoupon_created());
                                    if (getDetailCouponData.getCoupon_expiry() != null && !getDetailCouponData.getCoupon_expiry().isEmpty()) {
                                        if (getDetailCouponData.getCoupon_expiry().equals("0000-00-00")) {
                                            mBind.mTvEndDatee.setText("None");
                                        } else {
                                            mBind.mTvEndDatee.setText(getDetailCouponData.getCoupon_expiry());
                                        }
                                    }
                                    if (getDetailCouponData.getCoupon_applied() != null && !getDetailCouponData.getCoupon_applied().isEmpty()) {
                                        appliedProductId = getDetailCouponData.getCoupon_applied();
                                        getProducts();
                                    }
                                }
                            }
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GetCouponDetailMainObject> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null && getActivity() != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    if (getActivity() != null)
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    public void reqUpdateCoupon(String siteId, String couponId, String couponCode, String
            couponName, String couponAmount, String couponApplied,
                                String couponType, String couponStatus, String couponStart, String couponExpiry) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallUpdateProducts = Api.WEB_SERVICE.update_coupon_list("55555", couponId, siteId, couponName, couponCode, couponType, couponAmount, couponStart, couponStatus, couponApplied, couponExpiry);
            mCallUpdateProducts.enqueue(new Callback<AddCouponMainObject>() {
                @Override
                public void onResponse(Call<AddCouponMainObject> call, Response<AddCouponMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.products_updated), R.color.success);
                                EventBus.getDefault().post(new EventBusGetCoupons());
                                getActivity().onBackPressed();
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<AddCouponMainObject> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void getProducts() {
        try {
            if (progressBarProducts != null)
                progressBarProducts.setVisibility(View.VISIBLE);
            mCallProducts = Api.WEB_SERVICE.getProductsList("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
            mCallProducts.enqueue(new Callback<ProductsMainObject>() {
                @Override
                public void onResponse(@NonNull Call<ProductsMainObject> call, @NonNull Response<ProductsMainObject> response) {
                    if (progressBarProducts != null)
                        progressBarProducts.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                productsDataList = response.body().getData();
                                if (appliedProductId != null && !appliedProductId.equals("")) {
                                    if (appliedProductId.contains(",")) {
                                        mBind.mTvAllProducts.setText("All Products");
                                    } else {
                                        for (int i = 0; i < productsDataList.size(); i++) {
                                            if (appliedProductId.equals(productsDataList.get(i).getPro_id())) {
                                                mBind.mTvAllProducts.setText(productsDataList.get(i).getPro_title());
                                                break;
                                            }
                                        }
                                    }
                                }
                                setSiteProducts();
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProductsMainObject> call, @NonNull Throwable error) {
                    if (progressBarProducts != null)
                        progressBarProducts.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            if (progressBarProducts != null)
                progressBarProducts.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == productsDataList.size()) {
            List<String> productIdList = new ArrayList<>();
            for (int j = 0; j < productsDataList.size(); j++) {
                productIdList.add(productsDataList.get(j).getPro_id());
            }
            appliedProductId = productIdList.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
            mBind.mTvAllProducts.setText("All Products");
        } else {
            appliedProductId = productsDataList.get(i).getPro_id();
            mBind.mTvAllProducts.setText(productsDataList.get(i).getPro_title());
        }
        if (productsDialog != null) {
            productsDialog.dismiss();
        }
    }

}

