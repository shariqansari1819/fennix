package com.fennix.android.prod.utils.expand_recyclerview

import android.support.annotation.ColorInt
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fennix.android.prod.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_child.*

open class ItemChildHolder(@ColorInt val color: Int, val mImg: String,val blocks_id: String, val component_html: String) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
//        viewHolder.mTvChildName.text = number.toString()
        Glide.with(viewHolder.mImgChild).load(mImg)
//                .apply(RequestOptions().centerCrop())
                .apply(RequestOptions().placeholder(R.drawable.placeholder_wait))
                .apply(RequestOptions().error(R.drawable.palceholer_error))
                .into(viewHolder.mImgChild)
    }

    override fun getLayout() = R.layout.item_child



    override fun getSpanSize(spanCount: Int, position: Int) = spanCount / 2
}