package com.fennix.android.prod.model;

public class CreateSiteModel {
    private String status;
    private String site_id;
    private String message;
    private String sub_domain;
    private String custom_domain;

    public CreateSiteModel(String status, String site_id, String message) {
        this.status = status;
        this.site_id = site_id;
        this.message = message;
    }

    public String getSub_domain() {
        return sub_domain;
    }

    public void setSub_domain(String sub_domain) {
        this.sub_domain = sub_domain;
    }

    public String getCustom_domain() {
        return custom_domain;
    }

    public void setCustom_domain(String custom_domain) {
        this.custom_domain = custom_domain;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
