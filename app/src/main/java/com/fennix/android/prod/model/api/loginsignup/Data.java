
package com.fennix.android.prod.model.api.loginsignup;


import java.util.List;

public class Data {

    private String profile_image;
    private String id;
    private String package_id;
    private String username;
    private String email;
    private String password;
    private String first_name;
    private String last_name;
    private String activation_code;
    private String forgot_code;
    private String remember_code;
    private String last_login;
    private String stripe_cus_id;
    private String stripe_sub_id;
    private String paypal_token;
    private String paypal_profile_id;
    private String paypal_profile_status;
    private String paypal_last_transaction_id;
    private String current_subscription_gateway;
    private String payer_id;
    private String paypal_next_payment_date;
    private String paypal_previous_payment_date;
    private String type;
    private String status;
    private String created_at;
    private String modified_at;
    private String is_version;
    private String facebook_uid;
    private String sitelimit;
    private String package_type;
    private String notification_status;
    private String isCommerece;
    private String chat_status;
    private List<Siteslist> siteslist = null;

    public String getIsCommerece() {
        return isCommerece;
    }

    public void setIsCommerece(String isCommerece) {
        this.isCommerece = isCommerece;
    }

    public String getChat_status() {
        return chat_status;
    }

    public void setChat_status(String chat_status) {
        this.chat_status = chat_status;
    }

    public String getNotification_status() {
        return notification_status;
    }

    public void setNotification_status(String notification_status) {
        this.notification_status = notification_status;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getSitelimit() {
        return sitelimit;
    }

    public void setSitelimit(String sitelimit) {
        this.sitelimit = sitelimit;
    }

    public List<Siteslist> getSiteslist() {
        return siteslist;
    }

    public void setSiteslist(List<Siteslist> siteslist) {
        this.siteslist = siteslist;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }

    public String getForgot_code() {
        return forgot_code;
    }

    public void setForgot_code(String forgot_code) {
        this.forgot_code = forgot_code;
    }

    public String getRemember_code() {
        return remember_code;
    }

    public void setRemember_code(String remember_code) {
        this.remember_code = remember_code;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getStripe_cus_id() {
        return stripe_cus_id;
    }

    public void setStripe_cus_id(String stripe_cus_id) {
        this.stripe_cus_id = stripe_cus_id;
    }

    public String getStripe_sub_id() {
        return stripe_sub_id;
    }

    public void setStripe_sub_id(String stripe_sub_id) {
        this.stripe_sub_id = stripe_sub_id;
    }

    public String getPaypal_token() {
        return paypal_token;
    }

    public void setPaypal_token(String paypal_token) {
        this.paypal_token = paypal_token;
    }

    public String getPaypal_profile_id() {
        return paypal_profile_id;
    }

    public void setPaypal_profile_id(String paypal_profile_id) {
        this.paypal_profile_id = paypal_profile_id;
    }

    public String getPaypal_profile_status() {
        return paypal_profile_status;
    }

    public void setPaypal_profile_status(String paypal_profile_status) {
        this.paypal_profile_status = paypal_profile_status;
    }

    public String getPaypal_last_transaction_id() {
        return paypal_last_transaction_id;
    }

    public void setPaypal_last_transaction_id(String paypal_last_transaction_id) {
        this.paypal_last_transaction_id = paypal_last_transaction_id;
    }

    public String getCurrent_subscription_gateway() {
        return current_subscription_gateway;
    }

    public void setCurrent_subscription_gateway(String current_subscription_gateway) {
        this.current_subscription_gateway = current_subscription_gateway;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public String getPaypal_next_payment_date() {
        return paypal_next_payment_date;
    }

    public void setPaypal_next_payment_date(String paypal_next_payment_date) {
        this.paypal_next_payment_date = paypal_next_payment_date;
    }

    public String getPaypal_previous_payment_date() {
        return paypal_previous_payment_date;
    }

    public void setPaypal_previous_payment_date(String paypal_previous_payment_date) {
        this.paypal_previous_payment_date = paypal_previous_payment_date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public String getIs_version() {
        return is_version;
    }

    public void setIs_version(String is_version) {
        this.is_version = is_version;
    }

    public String getFacebook_uid() {
        return facebook_uid;
    }

    public void setFacebook_uid(String facebook_uid) {
        this.facebook_uid = facebook_uid;
    }

}
