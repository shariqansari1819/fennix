package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.CouponsAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragCouponsBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.coupons.getcoupons.GetCouponsData;
import com.fennix.android.prod.model.coupons.getcoupons.GetCouponsMainObject;
import com.fennix.android.prod.model.eventbus.EventBusCouponId;
import com.fennix.android.prod.model.eventbus.EventBusGetCoupons;
import com.fennix.android.prod.model.products.ProductsData;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragCoupons extends Fragment implements View.OnClickListener {

    private FragCouponsBinding mBind;
    private Call<GetCouponsMainObject> mCallCoupons;
    private Call<GenericModelStatusMessage> mCallDelete;
    private List<GetCouponsData> couponsDataList = new ArrayList<>();
    private CouponsAdapter couponsAdapter;

    public FragCoupons() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_coupons, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        EventBus.getDefault().register(this);
        mBind.mRvCoupons.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mBind.mRvCoupons.setItemAnimator(new DefaultItemAnimator());
        getCoupons();
        mBind.mTvAddCoupons.setOnClickListener(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusCouponClick(EventBusCouponId eventBusCouponId) {
        String couponId = couponsDataList.get(eventBusCouponId.getPosition()).getCoupon_id();
        if (eventBusCouponId.getClickType().equals("edit")) {
            Bundle bundle = new Bundle();
            bundle.putString("coupon_edit_type", "edit");
            bundle.putString("coupon_id", couponId);
            ((MainActivity) getActivity()).fnLoadFragAdd("COUPON EDIT", true, bundle);
        } else if (eventBusCouponId.getClickType().equals("delete")) {

            reqDelete(couponId);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusGetCoupons(EventBusGetCoupons eventBusGetCoupons) {
        getCoupons();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallCoupons != null && mCallCoupons.isExecuted()) {
            mCallCoupons.cancel();
        }
        if (mCallDelete != null && mCallDelete.isExecuted()) {
            mCallDelete.cancel();
        }
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvAddCoupons:
                Bundle bundle = new Bundle();
                bundle.putString("coupon_edit_type", "add");
                ((MainActivity) getActivity()).fnLoadFragAdd("COUPON EDIT", true, bundle);
                break;
        }
    }

    public void getCoupons() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallCoupons = Api.WEB_SERVICE.getCouponsList("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
            mCallCoupons.enqueue(new Callback<GetCouponsMainObject>() {
                @Override
                public void onResponse(@NonNull Call<GetCouponsMainObject> call, @NonNull Response<GetCouponsMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                couponsDataList = response.body().getData();
                                if (couponsDataList.size() == 0) {
                                    mBind.mTvNoCouponFound.setVisibility(View.VISIBLE);
                                } else {
                                    mBind.mTvNoCouponFound.setVisibility(View.GONE);
                                }
                                couponsAdapter = new CouponsAdapter(getActivity(), couponsDataList);
                                mBind.mRvCoupons.setAdapter(couponsAdapter);
                                mBind.mTvAllCouponsCount.setText("(" + response.body().getData().size() + ")");
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetCouponsMainObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void reqDelete(String couponId) {
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallDelete = Api.WEB_SERVICE.delete_coupon("55555", couponId);
        mCallDelete.enqueue(new Callback<GenericModelStatusMessage>() {
            @Override
            public void onResponse(Call<GenericModelStatusMessage> call, retrofit2.Response<GenericModelStatusMessage> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.product_delete_sucessfully), R.color.success);
                            getCoupons();
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GenericModelStatusMessage> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null && getActivity() != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    if (getActivity() != null)
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}
