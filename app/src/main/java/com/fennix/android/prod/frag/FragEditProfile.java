package com.fennix.android.prod.frag;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragEditProfileBinding;
import com.fennix.android.prod.model.api.loginsignup.Data;
import com.fennix.android.prod.model.api.loginsignup.LoginSignup;
import com.fennix.android.prod.model.eventbus.EventBusProfileImagePath;
import com.fennix.android.prod.utils.GlobalClass;
import com.fennix.android.prod.utils.gallery.GalleryActivity;
import com.fennix.android.prod.utils.image_compressor.Luban;
import com.fennix.android.prod.utils.image_compressor.OnCompressListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragEditProfile extends Fragment implements View.OnClickListener {

    private FragEditProfileBinding mBind;
    private Call<LoginSignup> userCall;
    private String profileImagePath = "";

    public FragEditProfile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_edit_profile, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mEtFirstName.setText(GlobalClass.getInstance().mPref.getString("first_name", ""));
        mBind.mEtLastName.setText(GlobalClass.getInstance().mPref.getString("last_name", ""));
        mBind.mEtEmail.setText(GlobalClass.getInstance().mPref.getString("email", ""));
        mBind.mEtPassword.setText(GlobalClass.getInstance().mPref.getString("password", ""));
        EventBus.getDefault().register(this);
        mBind.mImgBack.setOnClickListener(this);
        mBind.mImgProfile.setOnClickListener(this);
        mBind.mTvSave.setOnClickListener(this);
        String mUrl;
        if (GlobalClass.getInstance().mPref.getBoolean("fb", false)) {
            mUrl = GlobalClass.getInstance().mPref.getString("profile_image", "");
        } else {
            mUrl = ((MainActivity) getActivity()).mFBaseUrlImages + "/" + GlobalClass.getInstance().mPref.getString("profile_image", "");
        }
        Glide.with(getActivity()).load(mUrl)
                .apply(new RequestOptions().centerCrop())
                .apply(new RequestOptions().circleCrop())
                .apply(new RequestOptions().placeholder(R.drawable.prifile_image))
                .apply(new RequestOptions().error(R.drawable.prifile_image))
                .into(mBind.mImgProfile);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgBack:
                (getActivity()).onBackPressed();
                break;
            case R.id.mTvSave:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (mBind.mEtFirstName.getText().toString().trim().isEmpty() || mBind.mEtEmail.getText().toString().trim().isEmpty()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.fill_fields), R.color.error);
                } else {
                    if (funValidateEmail(mBind.mEtEmail.getText().toString())) {
                        if (!profileImagePath.equals("")) {
                            Luban.with(getActivity())
                                    .load(profileImagePath)
                                    .ignoreBy(100)
                                    .setCompressListener(new OnCompressListener() {
                                        @Override
                                        public void onStart() {

                                        }

                                        @Override
                                        public void onSuccess(File file) {
                                            reqEdit(file.getAbsolutePath(), mBind.mEtFirstName.getText().toString(), mBind.mEtLastName.getText().toString(), mBind.mEtEmail.getText().toString(), mBind.mEtPassword.getText().toString());
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                        }
                                    }).launch();
                        } else {
                            reqEdit(profileImagePath, mBind.mEtFirstName.getText().toString(), mBind.mEtLastName.getText().toString(), mBind.mEtEmail.getText().toString(), mBind.mEtPassword.getText().toString());
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.invalid_email), R.color.error);
                    }
                }
                break;
            case R.id.mImgProfile:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (((MainActivity) getActivity()).fnCheckPermission()) {
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra("mFlag", "single");
                    getActivity().startActivityForResult(intent, 1);
                } else {
                    ((MainActivity) getActivity()).fnRequestPermission(2);
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (userCall != null && userCall.isExecuted()) {
            userCall.cancel();
        }
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventBusProfileImagePath event) {
        profileImagePath = event.getImagePath();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();
        requestOptions.circleCrop();
        Glide.with(getActivity()).load(profileImagePath)
                .apply(requestOptions)
                .into(mBind.mImgProfile);
    }

    private boolean funValidateEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z0-9]+\\.+[a-z]+";
        if (email.matches(emailPattern)) {
            return true;
        } else {
            return false;
        }
    }

    public void reqEdit(String imagePath, String firstName, String lastName, String email, String password) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            final RequestBody first_name = RequestBody.create(MediaType.parse("text/plain"), firstName);
            final RequestBody last_name = RequestBody.create(MediaType.parse("text/plain"), lastName);
            final RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), GlobalClass.getInstance().mPref.getString("id", ""));
            final RequestBody user_email = RequestBody.create(MediaType.parse("text/plain"), email);
            final RequestBody user_password = RequestBody.create(MediaType.parse("text/plain"), password);

            if (!imagePath.equals("")) {
                File file = new File(imagePath);
                MultipartBody.Part filePart = MultipartBody.Part.createFormData("profile_image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
                userCall = Api.WEB_SERVICE.editProfile("55555", filePart, first_name, last_name, user_id, user_email, user_password);
            } else {
                userCall = Api.WEB_SERVICE.editProfile("55555", null, first_name, last_name, user_id, user_email, user_password);
            }
            userCall.enqueue(new Callback<LoginSignup>() {
                @Override
                public void onResponse(Call<LoginSignup> call, Response<LoginSignup> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                Data mObj = response.body().getData();
                                GlobalClass.getInstance().mPrefEditor.putString("id", mObj.getId());
                                GlobalClass.getInstance().mPrefEditor.putString("package_id", mObj.getPackage_id());
                                GlobalClass.getInstance().mPrefEditor.putString("username", mObj.getUsername());
                                GlobalClass.getInstance().mPrefEditor.putString("email", mObj.getEmail());
                                GlobalClass.getInstance().mPrefEditor.putString("first_name", mObj.getFirst_name());
                                GlobalClass.getInstance().mPrefEditor.putString("last_name", mObj.getLast_name());
                                GlobalClass.getInstance().mPrefEditor.putString("activation_code", mObj.getActivation_code());
                                GlobalClass.getInstance().mPrefEditor.putString("forgot_code", mObj.getForgot_code());
                                GlobalClass.getInstance().mPrefEditor.putString("remember_code", mObj.getRemember_code());
                                GlobalClass.getInstance().mPrefEditor.putString("last_login", mObj.getLast_login());
                                GlobalClass.getInstance().mPrefEditor.putString("stripe_cus_id", mObj.getStripe_cus_id());
                                GlobalClass.getInstance().mPrefEditor.putString("stripe_sub_id", mObj.getStripe_sub_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_token", mObj.getPaypal_token());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_profile_id", mObj.getPaypal_profile_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_last_transaction_id", mObj.getPaypal_last_transaction_id());
                                GlobalClass.getInstance().mPrefEditor.putString("current_subscription_gateway", mObj.getCurrent_subscription_gateway());
                                GlobalClass.getInstance().mPrefEditor.putString("payer_id", mObj.getPayer_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_next_payment_date", mObj.getPaypal_next_payment_date());
                                GlobalClass.getInstance().mPrefEditor.putString("type", mObj.getType());
                                GlobalClass.getInstance().mPrefEditor.putString("status", mObj.getStatus());
                                GlobalClass.getInstance().mPrefEditor.putString("created_at", mObj.getCreated_at());
                                GlobalClass.getInstance().mPrefEditor.putString("modified_at", mObj.getModified_at());
                                GlobalClass.getInstance().mPrefEditor.putString("facebook_uid", mObj.getFacebook_uid());
                                GlobalClass.getInstance().mPrefEditor.putBoolean("login", true);
                                GlobalClass.getInstance().mPrefEditor.putString("profile_image", mObj.getProfile_image());
                                GlobalClass.getInstance().mPrefEditor.putString("is_commerce", mObj.getIsCommerece());
                                GlobalClass.getInstance().mPrefEditor.apply();
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.profile_updated), R.color.success);
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<LoginSignup> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (
                Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

}
