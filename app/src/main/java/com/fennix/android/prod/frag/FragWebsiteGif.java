package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.databinding.FragWebsiteGifBinding;


public class FragWebsiteGif extends Fragment {

    private FragWebsiteGifBinding mBind;
    private Handler mHandler;

    public FragWebsiteGif() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_website_gif, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getActivity()).mPosPrevious = 1;
                ((MainActivity) getActivity()).mBind.mImgHome.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                ((MainActivity) getActivity()).mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                if (GlobalClass.getInstance().mPref.getString("page_preview", "").equals("")) {
//                    ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", false, null);
//                } else {
                ((MainActivity) getActivity()).fnLoadFragReplace("HOME", null);
//                    }
            }
        }, 5000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
    }

}
