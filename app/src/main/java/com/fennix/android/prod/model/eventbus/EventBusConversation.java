package com.fennix.android.prod.model.eventbus;

public class EventBusConversation {
    private int position;

    public EventBusConversation(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
