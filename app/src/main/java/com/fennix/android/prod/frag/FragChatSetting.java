package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragChatSettingBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.utils.GlobalClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragChatSetting extends Fragment implements CompoundButton.OnCheckedChangeListener {

    private FragChatSettingBinding mBind;
    private Call<GenericModelStatusMessage> mCallNotificationOnOff;

    public FragChatSetting() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_chat_setting, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallNotificationOnOff != null && mCallNotificationOnOff.isExecuted()) {
            mCallNotificationOnOff.cancel();
        }
    }

    private void init() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mBind.mPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        mBind.mScNotification.setChecked(GlobalClass.getInstance().mPref.getString("notification_status", "1").equals("1"));
        mBind.mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        mBind.mScNotification.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (mBind.mPb.getVisibility() == View.VISIBLE) {
            return;
        }
        if (b) {
            reqNotificationOnOff("1");
        } else {
            reqNotificationOnOff("0");
        }
    }

    private void reqNotificationOnOff(final String flag) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallNotificationOnOff = Api.WEB_SERVICE.notification_on_off("55555", GlobalClass.getInstance().mPref.getString("id", ""), flag);
            mCallNotificationOnOff.enqueue(new Callback<GenericModelStatusMessage>() {
                @Override
                public void onResponse(@NonNull Call<GenericModelStatusMessage> call, @NonNull Response<GenericModelStatusMessage> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Notification status changed successfully.", R.color.success);
                                GlobalClass.getInstance().mPrefEditor.putString("notification_status", flag).apply();
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GenericModelStatusMessage> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}