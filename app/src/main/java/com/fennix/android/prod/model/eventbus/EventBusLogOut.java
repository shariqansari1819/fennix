package com.fennix.android.prod.model.eventbus;

public class EventBusLogOut {

    private boolean logOut;

    public EventBusLogOut(boolean logOut) {
        this.logOut = logOut;
    }

    public boolean isLogOut() {
        return logOut;
    }

    public void setLogOut(boolean logOut) {
        this.logOut = logOut;
    }
}
