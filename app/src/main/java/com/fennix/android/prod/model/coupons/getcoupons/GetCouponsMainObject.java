
package com.fennix.android.prod.model.coupons.getcoupons;

import java.util.List;

public class GetCouponsMainObject {

    private String status;
    private List<GetCouponsData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GetCouponsData> getData() {
        return data;
    }

    public void setData(List<GetCouponsData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
