package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragReferBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.utils.GlobalClass;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragRefer extends Fragment implements View.OnClickListener {

    private FragReferBinding mBind;
    private Call<GenericModelStatusMessage> mRetrofitCall;

    public FragRefer() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_refer, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mTvTerms.setOnClickListener(this);
        mBind.mBtnReferral.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRetrofitCall != null && mRetrofitCall.isExecuted()) {
            mRetrofitCall.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnReferral:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (mBind.mEtFullName.getText().toString().trim().isEmpty() || mBind.mEtFullName.getText().toString().trim().length() == 0) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Name Is Empty", R.color.error);
                } else if (mBind.mEtPhone.getText().toString().trim().isEmpty() || mBind.mEtPhone.getText().toString().trim().length() == 0) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Phone Is Empty", R.color.error);
                }
//                else if (mBind.mEtEmail.getText().toString().trim().isEmpty() || mBind.mEtEmail.getText().toString().trim().length() == 0) {
//                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Email Is Empty", R.color.error);
//                }
//                else if (mBind.mEtBusiness.getText().toString().trim().isEmpty() || mBind.mEtBusiness.getText().toString().trim().length() == 0) {
//                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Business Is Empty", R.color.error);
//                }
                else {
                    reqRefer();
                }
                break;
            case R.id.mTvTerms:
                Bundle bundle = new Bundle();
                bundle.putString("mFlag", "terms");
                ((MainActivity) getActivity()).fnLoadFragAdd("TERMS", true, bundle);
                break;
        }
    }

    private void reqRefer() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mRetrofitCall = Api.WEB_SERVICE.refer("55555", GlobalClass.getInstance().mPref.getString("id", ""), GlobalClass.getInstance().mPref.getString("email", ""), mBind.mEtFullName.getText().toString().trim(), mBind.mEtBusiness.getText().toString().trim(), mBind.mEtPhone.getText().toString().trim());
            mRetrofitCall.enqueue(new Callback<GenericModelStatusMessage>() {
                @Override
                public void onResponse(@NonNull Call<GenericModelStatusMessage> call, @NonNull Response<GenericModelStatusMessage> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.refer_link_sent), R.color.success);
                                Bundle bundle = new Bundle();
                                bundle.putString("mFlag", "refer");
                                ((MainActivity) getActivity()).fnLoadFragAdd("SUCCESS EMAIL PHONE", true, bundle);
//                                mBind.mEtBusiness.setText("");
//                                mBind.mEtEmail.setText("");
                                mBind.mEtPhone.setText("");
                                mBind.mEtFullName.setText("");
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GenericModelStatusMessage> call, @NonNull Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
