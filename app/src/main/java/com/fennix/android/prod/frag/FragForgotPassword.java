package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragForgotPasswordBinding;
import com.fennix.android.prod.model.api.loginsignup.LoginSignup;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragForgotPassword extends Fragment implements View.OnClickListener {

    private FragForgotPasswordBinding mBind;

    public FragForgotPassword() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_forgot_password, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mBind.mPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        mBind.mBtnForgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnForgot:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (mBind.mEtEmail.getText().toString().trim().isEmpty() || mBind.mEtEmail.getText().toString().trim().length() == 0) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.email_is_empty), R.color.error);
                } else {
                    reqForgotPassword();
                }
                break;
        }
    }

    private void fnMessageShow(String mMsg, String mStatus) {
        switch (mStatus) {
            case "0":
                if (mMsg.contentEquals("The Email field must contain a valid email address.")) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.forgot_pass1), R.color.error);
                } else if (mMsg.contentEquals("No user found with this email.")) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.forgot_pass2), R.color.error);
                } else if (mMsg.contentEquals("User did not activated account!")) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.forgot_pass3), R.color.error);
                } else if (mMsg.contentEquals("Something went wrong")) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.forgot_pass4), R.color.error);
                }
                break;
            case "1":
                if (mMsg.contentEquals("Please check your email to resset Password")) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.forgot_pass5), R.color.success);
                }
                break;
        }
    }

    private void reqForgotPassword() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            Call<LoginSignup> userCall = Api.WEB_SERVICE.forgotpassword("55555", mBind.mEtEmail.getText().toString().trim());
            userCall.enqueue(new Callback<LoginSignup>() {
                @Override
                public void onResponse(@NonNull Call<LoginSignup> call, @NonNull Response<LoginSignup> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                fnMessageShow(response.body().getMessage(), "1");
                                ((MainActivity) getActivity()).mFragManager.popBackStack();
                                break;
                            case "0":
                                fnMessageShow(response.body().getMessage(), "0");
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginSignup> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
