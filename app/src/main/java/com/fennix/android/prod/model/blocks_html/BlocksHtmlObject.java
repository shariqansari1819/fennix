
package com.fennix.android.prod.model.blocks_html;

import java.util.HashMap;
import java.util.Map;

public class BlocksHtmlObject {

    private String status;
    private BlocksHtmlData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BlocksHtmlData getData() {
        return data;
    }

    public void setData(BlocksHtmlData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
