
package com.fennix.android.prod.model.addcoupons;


public class AddCouponMainObject {

    private String status;
    private AddCouponData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AddCouponData getData() {
        return data;
    }

    public void setData(AddCouponData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
