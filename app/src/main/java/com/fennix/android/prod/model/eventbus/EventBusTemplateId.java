package com.fennix.android.prod.model.eventbus;

public class EventBusTemplateId {
    private int mPos;

    public EventBusTemplateId(int mPos) {
        this.mPos = mPos;
    }

    public int getmPos() {
        return mPos;
    }

    public void setmPos(int mPos) {
        this.mPos = mPos;
    }
}
