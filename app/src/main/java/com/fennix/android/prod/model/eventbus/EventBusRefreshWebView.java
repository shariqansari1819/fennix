package com.fennix.android.prod.model.eventbus;

public class EventBusRefreshWebView {

    private boolean refreshWebView;

    public EventBusRefreshWebView(boolean refreshWebView) {
        this.refreshWebView = refreshWebView;
    }

    public boolean isRefreshWebView() {
        return refreshWebView;
    }

    public void setRefreshWebView(boolean refreshWebView) {
        this.refreshWebView = refreshWebView;
    }

}