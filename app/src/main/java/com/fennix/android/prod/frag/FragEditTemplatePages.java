package com.fennix.android.prod.frag;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragEditTemplateBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.TemplateImageUploadModel;
import com.fennix.android.prod.model.createpage.CreatePageObject;
import com.fennix.android.prod.model.edittemplate.EditTemplateData;
import com.fennix.android.prod.model.edittemplate.EditTemplateModel;
import com.fennix.android.prod.model.eventbus.EventBusAddBlock;
import com.fennix.android.prod.model.eventbus.EventBusAddComponent;
import com.fennix.android.prod.model.eventbus.EventBusAddNewPage;
import com.fennix.android.prod.model.eventbus.EventBusRefreshHomeWebView;
import com.fennix.android.prod.model.eventbus.EventBusRefreshWebView;
import com.fennix.android.prod.model.sitepages.SitePagesData;
import com.fennix.android.prod.model.sitepages.SitePagesObject;
import com.fennix.android.prod.utils.GlobalClass;
import com.fennix.android.prod.utils.gallery.GalleryActivity;
import com.fennix.android.prod.utils.image_compressor.Luban;
import com.fennix.android.prod.utils.image_compressor.OnCompressListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.fennix.android.prod.R;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragEditTemplatePages extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener {

    //    Android fields....
    private FragEditTemplateBinding mBind;
    private LayoutInflater layoutInflater;
    private BottomSheetBehavior bottomSheetBehavior;
    private AlertDialog editTemplateDialog, pagesDialog;
    private ListView listViewPages;
    private ProgressBar progressBarPages;

    //    Array list fields....
    private List<EditTemplateData> editTemplateDataList = new ArrayList<>();
    private ArrayList<WebView> webViewArrayList = new ArrayList<>();
    private List<SitePagesData> sitePagesDataList = new ArrayList<>();

    //    Retrofit calls fields....
    private Call<EditTemplateModel> mCallback;
    private Call<CreatePageObject> createPageCallback;
    private Call<SitePagesObject> sitePagesCallback;
    private Call<TemplateImageUploadModel> imageUploadCallback;
    private Call<GenericModelStatusMessage> saveTemplateCallback;

    //    Instance fields....
    private String lastSelectedImagePath, newImagePath;
    private int selectedWebViewIndex, addBlockType, anchorDialogCounter;
    private boolean isTemplateEditable = true, isDataChanged, isDataDraggable;
    private String currentPageId = "";

    public FragEditTemplatePages() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_edit_template, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mBind.mPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        }
//        Android fields initialization....
        layoutInflater = LayoutInflater.from(getActivity());
        bottomSheetBehavior = BottomSheetBehavior.from(mBind.mClBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setHideable(false);

//        Getting template blocks....
        fnLoadTemplateBlocks(currentPageId);
        fnGetSitePages();

//        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
//                    mBind.mImgArrowDown.setImageResource(R.drawable.arrow_up_icon);
//                    showTemplatePages();
//                } else {
//                    mBind.mImgArrowDown.setImageResource(R.drawable.arrow_down_icon);
//                }
//                mBind.mImgArrowDown.setColorFilter(ContextCompat.getColor(getActivity(), R.color.black));
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });

//        All event listeners....
        mBind.mScEditTemplate.setOnCheckedChangeListener(this);
        mBind.mImgSaveEditTemplate.setOnClickListener(this);
        mBind.mImgArrowDown.setOnClickListener(this);
        mBind.mImgTemplate.setOnClickListener(this);
        mBind.mImgBlocks.setOnClickListener(this);
        mBind.mImgPages.setOnClickListener(this);
        mBind.mImgComponents.setOnClickListener(this);
//        mBind.mImgMoreEditTemplate.setOnClickListener(this);
        mBind.mImgPagesArrow.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallback != null && mCallback.isExecuted()) {
            mCallback.cancel();
        }
        if (imageUploadCallback != null && imageUploadCallback.isExecuted()) {
            imageUploadCallback.cancel();
        }
        if (saveTemplateCallback != null && saveTemplateCallback.isExecuted()) {
            saveTemplateCallback.cancel();
        }
        if (sitePagesCallback != null && sitePagesCallback.isExecuted()) {
            sitePagesCallback.cancel();
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @SuppressLint({"AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgSaveEditTemplate:
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
//                if (!isDataChanged) {
//                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Nothing to save.", R.color.error);
//                    return;
//                }
                if (webViewArrayList.size() > 0 && editTemplateDataList.size() > 0) {
                    for (int i = 0; i < webViewArrayList.size(); i++) {
                        WebView webView = webViewArrayList.get(i);
                        webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'," + i + ");");
                    }
                }
                break;
            case R.id.mImgTemplate:
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.mImgBlocks:
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                showBlockFragment("blocks");
                break;
            case R.id.mImgComponents:
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                showBlockFragment("components");
                break;
            case R.id.mImgPages:
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
//            case R.id.mImgArrowDown:
//                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                else
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                break;
            case R.id.mImgMoreEditTemplate:
                final View editTemplateDialogView = layoutInflater.inflate(R.layout.dialog_change_template, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(editTemplateDialogView);
                editTemplateDialog = builder.create();
                editTemplateDialog.show();
                editTemplateDialogView.findViewById(R.id.mTvChangeTemplate).setOnClickListener(this);
                editTemplateDialogView.findViewById(R.id.mTvAddNewPage).setOnClickListener(this);
                editTemplateDialogView.findViewById(R.id.mTvCancel).setOnClickListener(this);
                editTemplateDialogView.findViewById(R.id.mTvAddComponent).setOnClickListener(this);
                final TextView textViewDrag = editTemplateDialogView.findViewById(R.id.mTvEnableDrag);
                if (isDataDraggable)
                    textViewDrag.setText(getResources().getString(R.string.disable_drag));
                else
                    textViewDrag.setText(getResources().getString(R.string.enable_drag));
                textViewDrag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editTemplateDialog.dismiss();
                        if (isDataDraggable) {
                            textViewDrag.setText(getResources().getString(R.string.enable_drag));
                            isDataDraggable = false;

                        } else {
                            textViewDrag.setText(getResources().getString(R.string.disable_drag));
                            isDataDraggable = true;
                        }
                        mBind.mLlEditTemplate.removeAllViews();
                        webViewArrayList.clear();
                        for (int i = 0; i < editTemplateDataList.size(); i++) {
                            fnPopulateWebViews(i);
                        }
                    }
                });
                break;
            case R.id.mTvChangeTemplate:
                if (editTemplateDialog != null) {
                    editTemplateDialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString("mFlag", "edit_template");
                    ((MainActivity) getActivity()).mFScreenName = "SELECT TEMPLATE";
                    ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", true, bundle);
                }
                break;
            case R.id.mTvAddNewPage:
                if (editTemplateDialog != null) {
                    editTemplateDialog.dismiss();
                    AlertDialog.Builder addPageBuilder = new AlertDialog.Builder(getActivity());
                    View addPageView = layoutInflater.inflate(R.layout.dialog_page_name, null);
                    addPageBuilder.setView(addPageView);
                    final AlertDialog addPageDialog = addPageBuilder.create();
                    addPageDialog.setCancelable(false);
                    addPageDialog.show();
                    final AppCompatEditText editText = addPageView.findViewById(R.id.mEtPageName);
                    Button button = addPageView.findViewById(R.id.mBtnSavePageName);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addPageDialog.dismiss();
                            fnCreateNewPage(editText.getText().toString());
                        }
                    });
                    addPageView.findViewById(R.id.mBtnCancelPageName).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addPageDialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.mTvCancel:
                if (editTemplateDialog != null) {
                    editTemplateDialog.dismiss();
                }
                break;
            case R.id.mTvAddComponent:
                if (editTemplateDialog != null) {
                    editTemplateDialog.dismiss();
                }
                showBlockFragment("components");
                break;
            case R.id.mImgPagesArrow:
                AlertDialog.Builder pagesDialogBuilder = new AlertDialog.Builder(getActivity());
                final View dialogPagesView = layoutInflater.inflate(R.layout.dialog_pages_list, null);
                pagesDialogBuilder.setView(dialogPagesView);
                pagesDialog = pagesDialogBuilder.create();
                pagesDialog.show();
                listViewPages = dialogPagesView.findViewById(R.id.mLstPages);
                progressBarPages = dialogPagesView.findViewById(R.id.mPb);
                if (sitePagesDataList.size() > 0) {
                    setSitePages();
                } else {
                    fnGetSitePages();
                }
                TextView textViewCancel = dialogPagesView.findViewById(R.id.mTvCancel);
                listViewPages.setOnItemClickListener(this);
                textViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pagesDialog.dismiss();
                    }
                });
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        isTemplateEditable = b;
        mBind.mLlEditTemplate.removeAllViews();
        webViewArrayList.clear();
        for (int i = 0; i < editTemplateDataList.size(); i++) {
            fnPopulateWebViews(i);
        }
//        if (b)
//            mBind.mImgMoreEditTemplate.setVisibility(View.VISIBLE);
//        else
//            mBind.mImgMoreEditTemplate.setVisibility(View.GONE);
    }

    //    Method to get the list of pages related to site....
    private void setSitePages() {
        String[] arr = new String[sitePagesDataList.size()];
        for (int i = 0; i < sitePagesDataList.size(); i++) {
            arr[i] = sitePagesDataList.get(i).getPages_name();
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, arr);
        if (listViewPages != null)
            listViewPages.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (pagesDialog != null) {
            ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
            pagesDialog.dismiss();
            webViewArrayList.clear();
            editTemplateDataList.clear();
            mBind.mLlEditTemplate.removeAllViews();
            if (i == 0)
                currentPageId = "";
            else
                currentPageId = sitePagesDataList.get(i).getPages_id();
            fnLoadTemplateBlocks(currentPageId);
        }
    }

    //    Method to open FragBuilderBlock to load blocks or components related to flag....
    private void showBlockFragment(String flag) {
        ((MainActivity) getActivity()).mFScreenName = "BLOCK ITEMS";
        Bundle bundle = new Bundle();
        bundle.putString("mFlag", flag);
        ((MainActivity) getActivity()).fnLoadFragAdd("BLOCK ITEMS", true, bundle);
    }

    //    Java script interface to get html of all the webviews one by one with indexes of webviews....
    class MyJavaScriptInterface {

        //        Getting html and index in this method....
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(final String html, final int i) {
            //  Handle all html of all web views
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Document document = Jsoup.parse(html);
                    document.head().getElementsByAttributeValue("href", "http://fennixpro.com/dragdropassets/demo.css").remove();
                    document.head().getElementsByAttributeValue("href", "http://www.jqueryscript.net/css/jquerysctipttop.css").remove();
                    document.head().getElementsByAttributeValue("href", "http://fennixpro.com/dragdropassets/draganddrop.css").remove();
                    document.head().getElementsByAttributeValue("href", "https://bootswatch.com/flatly/bootstrap.min.css").remove();
                    document.head().getElementsByAttributeValue("src", "http://fennixpro.com/dragdropassets/jquery-1.12.4.min.js").remove();
                    document.head().getElementsByAttributeValue("src", "http://fennixpro.com/dragdropassets/draganddrop.js").remove();
                    document.head().getElementsByAttributeValue("src", "http://fennixpro.com/dragdropassets/demo.js").remove();
                    editTemplateDataList.get(i).setFrames_content(document.html());
                    parseTemplateHtml(false, false, i);
                    if (i == editTemplateDataList.size() - 1) {
                        final String json = new Gson().toJson(editTemplateDataList);
                        if (currentPageId.equals(""))
                            saveEditedTemplate(json, "0");
                        else
                            saveEditedTemplate(json, "1");
                    }
                }
            });
        }
    }

    //    Java script interface to detect if any element was edited by clicking....
    class ContentChangeScriptInterface {

        //        Method trigger when any text change in webview tags....
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void onChange() {
            isDataChanged = true;
        }
    }

    //    Java script interface to detect if any element was dragged....
    class DragChangeScript {

        //        Method triggers when any element inside webview is dragged....
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void onDrag(boolean isDragStart) {
            isDataChanged = true;
            if (isDragStart)
                mBind.mNsvEditTemplate.setEnableScrolling(false);
            else
                mBind.mNsvEditTemplate.setEnableScrolling(true);
        }
    }

    //     Java script interface to detect if any anchor is clicked if clicked then get index....
    class AnchorClickedScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void onClick(final int index) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    anchorDialogCounter++;
                    if (anchorDialogCounter == 1) {
                        final Document document = Jsoup.parse(editTemplateDataList.get(selectedWebViewIndex).getFrames_content());
                        final Elements anchors = document.select("a");
//            Toast.makeText(getActivity(), "Link: " + anchors.get(index).attributes().get("href") + " Text: " + anchors.get(index).text() + " Webview Index: " + selectedWebViewIndex, Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        View view = layoutInflater.inflate(R.layout.dialog_anchor_link, null);
                        builder.setView(view);
                        final AlertDialog dialog = builder.create();
                        dialog.setCancelable(false);
                        dialog.show();
                        final AppCompatEditText editTextText = view.findViewById(R.id.mEtAnchorText);
                        final AppCompatEditText editTextLink = view.findViewById(R.id.mEtAnchorLink);
                        Button buttonSave = view.findViewById(R.id.mBtnSave);
                        Button buttonCancel = view.findViewById(R.id.mBtnCancel);
                        final String text = anchors.get(index).text();
                        final String link = anchors.get(index).attributes().get("href");
                        editTextText.setText(anchors.get(index).text());
                        editTextLink.setText(anchors.get(index).attributes().get("href"));
                        buttonSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                boolean isExist = false;
                                for (int i = 0; i < sitePagesDataList.size(); i++) {
                                    if (editTextLink.getText().toString().contains(sitePagesDataList.get(i).getPages_name())) {
                                        isExist = true;
                                        break;
                                    }
                                }
                                if (isExist) {
                                    dialog.dismiss();
                                    if (text.equalsIgnoreCase(editTextText.getText().toString()) && link.equalsIgnoreCase(editTextLink.getText().toString())) {
                                        return;
                                    }
                                    anchors.get(index).text(editTextText.getText().toString());
                                    anchors.get(index).attributes().put("href", editTextLink.getText().toString());
                                    editTemplateDataList.get(selectedWebViewIndex).setFrames_content(document.html());
                                    mBind.mLlEditTemplate.removeViewAt(selectedWebViewIndex);
                                    webViewArrayList.remove(selectedWebViewIndex);
                                    fnPopulateWebViews(selectedWebViewIndex);
                                } else {
                                    Toast.makeText(getActivity(), "Page does not exist.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        buttonCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
            });
        }

    }

    //    TODO: Method to parse html using Jsoup to inject custom design and classes inside html elements....
    private void parseTemplateHtml(boolean edit, boolean isDraggable, int i) {
        boolean isEditable = true;
        int borderWidth = 1;
        if (!edit) {
            isEditable = false;
            borderWidth = 0;
        }
        Document document = Jsoup.parse(editTemplateDataList.get(i).getFrames_content());
        Elements elements = document.select("div");
        Elements paragraphs = document.select("p");
        Elements headingOne = document.select("h1");
        Elements buttons = document.select("button");
        Elements headingTwo = document.select("h2");
        Elements headingThree = document.select("h3");
        Elements headingFour = document.select("h4");
        Elements anchors = document.select("a");

        if (elements.size() > 0)
            elements.get(0).attr("style", "border: " + borderWidth + "px dashed #FCB945");
        for (int k = 0; k < buttons.size(); k++) {
            String lastAttr = buttons.get(k).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            buttons.get(k).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            buttons.get(k).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!buttons.get(k).hasClass("drag"))
                    addClassOnTag(buttons.get(k), "drag");
            } else {
                removeClassFromTag(buttons.get(k), "drag");
                removeClassFromTag(buttons.get(k), "draggable");
                removeClassFromTag(buttons.get(k), "dragaware");
            }
        }
        for (int j = 0; j < paragraphs.size(); j++) {
            String lastAttr = paragraphs.get(j).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            paragraphs.get(j).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            paragraphs.get(j).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!paragraphs.get(j).hasClass("drag"))
                    addClassOnTag(paragraphs.get(j), "drag");
            } else {
                removeClassFromTag(paragraphs.get(j), "drag");
                removeClassFromTag(paragraphs.get(j), "draggable");
                removeClassFromTag(paragraphs.get(j), "dragaware");
            }
        }
        for (int k = 0; k < headingThree.size(); k++) {
            String lastAttr = headingThree.get(k).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            headingThree.get(k).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            headingThree.get(k).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!headingThree.get(k).hasClass("drag"))
                    addClassOnTag(headingThree.get(k), "drag");
            } else {
                removeClassFromTag(headingThree.get(k), "drag");
                removeClassFromTag(headingThree.get(k), "draggable");
                removeClassFromTag(headingThree.get(k), "dragaware");
            }
        }
        for (int l = 0; l < headingFour.size(); l++) {
            String lastAttr = headingFour.get(l).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            headingFour.get(l).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            headingFour.get(l).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!headingFour.get(l).hasClass("drag"))
                    addClassOnTag(headingFour.get(l), "drag");
            } else {
                removeClassFromTag(headingFour.get(l), "drag");
                removeClassFromTag(headingFour.get(l), "draggable");
                removeClassFromTag(headingFour.get(l), "dragaware");
            }
        }
        for (int m = 0; m < anchors.size(); m++) {
            String lastAttr = anchors.get(m).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            anchors.get(m).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
//            anchors.get(m).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!anchors.get(m).hasClass("drag"))
                    addClassOnTag(anchors.get(m), "drag");
            } else {
                removeClassFromTag(anchors.get(m), "drag");
                removeClassFromTag(anchors.get(m), "draggable");
                removeClassFromTag(anchors.get(m), "dragaware");
            }
        }
        for (int n = 0; n < headingTwo.size(); n++) {
            String lastAttr = headingTwo.get(n).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            headingTwo.get(n).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            headingTwo.get(n).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!headingTwo.get(n).hasClass("drag"))
                    addClassOnTag(headingTwo.get(n), "drag");
            } else {
                removeClassFromTag(headingTwo.get(n), "drag");
                removeClassFromTag(headingTwo.get(n), "draggable");
                removeClassFromTag(headingTwo.get(n), "dragaware");
            }
        }
        for (int o = 0; o < headingOne.size(); o++) {
            String lastAttr = headingOne.get(o).attr("style").replace("border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;", "");
            headingOne.get(o).attr("style", lastAttr + "border: " + borderWidth + "px dashed #ff0000; border-radius: 5px;");
            headingOne.get(o).attr("contenteditable", isEditable);
            if (edit && isDraggable) {
                if (!headingOne.get(o).hasClass("drag"))
                    addClassOnTag(headingOne.get(o), "drag");
            } else {
                removeClassFromTag(headingOne.get(o), "drag");
                removeClassFromTag(headingOne.get(o), "draggable");
                removeClassFromTag(headingOne.get(o), "dragaware");
            }
        }

        editTemplateDataList.get(i).setFrames_content(document.html());
    }

    //    Method to inject new class on any element....
    private void addClassOnTag(Element element, String className) {
        element.addClass(className);
    }

    //    Method to remove class from any element....
    private void removeClassFromTag(Element element, String className) {
        element.removeClass(className);
    }

    //    TODO: Method to create web views and show them inside linearLayout one by one related to index....
    @SuppressLint({"ClickableViewAccessibility", "SetJavaScriptEnabled", "AddJavascriptInterface", "JavascriptInterface"})
    private void fnPopulateWebViews(final int index) {

        final View rowView = layoutInflater.inflate(R.layout.cus_edit_template, null);
        mBind.mLlEditTemplate.addView(rowView, index);

        final ImageView imageView = rowView.findViewById(R.id.mImgEditTemplateRow);
        final WebView webView = rowView.findViewById(R.id.mWvEditTemplateRow);

        parseTemplateHtml(isTemplateEditable, isDataDraggable, index);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setVerticalScrollBarEnabled(true);
        webView.setWebViewClient(new MyWebViewClient(imageView));
        webView.setWebChromeClient(new WebChromeClient());

//        Injecting custom interface to detect changes....
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
        webView.addJavascriptInterface(new ContentChangeScriptInterface(), "CHANGED");
        webView.addJavascriptInterface(new DragChangeScript(), "DRAG");
        webView.addJavascriptInterface(new AnchorClickedScriptInterface(), "ONANCHORCLICK");
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            webView.loadData(URLEncoder.encode(editTemplateDataList.get(index).getFrames_content()).replaceAll("\\+", " "), "text/html", Xml.Encoding.UTF_8.toString());
        } else {
            webView.loadDataWithBaseURL(null, editTemplateDataList.get(index).getFrames_content(), "text/html; charset=utf-8", "UTF-8", null);
        }
//        webView.loadDataWithBaseURL(null, , "text/html; charset=utf-8", "UTF-8", null);

        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (isTemplateEditable) {
                    WebView.HitTestResult hitTestResult = ((WebView) view).getHitTestResult();
                    if (hitTestResult != null) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            if (((MainActivity) getActivity()).fnCheckPermission()) {
                                if (hitTestResult.getType() == WebView.HitTestResult.IMAGE_TYPE) {
                                    lastSelectedImagePath = hitTestResult.getExtra();
                                    selectedWebViewIndex = index;
                                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                                    intent.putExtra("mFlag", "single");
                                    startActivityForResult(intent, 10);
                                } else if (hitTestResult.getType() == WebView.HitTestResult.SRC_ANCHOR_TYPE || hitTestResult.getType() == WebView.HitTestResult.ANCHOR_TYPE) {
                                    selectedWebViewIndex = index;
                                    anchorDialogCounter = 0;
                                    webView.loadUrl(
                                            "javascript:" +
                                                    "$('a').click(function(){window.ONANCHORCLICK.onClick($('a').index(this));});");
                                }

                            } else {
                                ((MainActivity) getActivity()).fnRequestPermission(10);
                            }
                        }
                    }
//                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
//                        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                return false;
            }
        });

        webViewArrayList.add(index, webView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View dialogView = layoutInflater.inflate(R.layout.dialog_edit_template, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialogView);
                final AlertDialog dialog = builder.create();
                dialog.show();
                dialogView.findViewById(R.id.mTvCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
//                        showTemplatePages();
//                        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                });
                dialogView.findViewById(R.id.mTvDelete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        editTemplateDataList.remove(index);
                        mBind.mLlEditTemplate.removeAllViews();
                        webViewArrayList.clear();
                        isDataChanged = true;
                        for (int i = 0; i < editTemplateDataList.size(); i++) {
                            fnPopulateWebViews(i);
                        }
                    }
                });
                dialogView.findViewById(R.id.mTvAddAbove).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        selectedWebViewIndex = index;
                        addBlockType = 0;
                        showBlockFragment("blocks");
//                        showBlockComponents();
//                        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });
                dialogView.findViewById(R.id.mTvAddBelow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        selectedWebViewIndex = index;
                        addBlockType = 1;
                        showBlockFragment("blocks");
//                        showBlockComponents();
//                        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                });
//                editTemplateDialog.findViewById(R.id.mTvAddComponent).setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        editTemplateDialog.dismiss();
//                        selectedWebViewIndex = finalI;
//                        showBlockComponents();
//                        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
//                            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    }
//                });
            }
        });
    }

    private class MyWebViewClient extends WebViewClient {

        ImageView imageView;

        MyWebViewClient(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mBind.mPb.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (isTemplateEditable)
                imageView.setVisibility(View.VISIBLE);
            mBind.mPb.setVisibility(View.GONE);
//                Injecting custom javascript inside webview....
            if (isTemplateEditable) {
                view.loadUrl(
                        "javascript:" +
                                "var contents = $('p,h1,h2,h3,h4,a').html();"
                                + "$('p,h1,h2,h3,h4,a').blur(function() {"
                                + "if (contents!=$(this).html()){"
                                + "contents = $(this).html();"
                                + "window.CHANGED.onChange();"
                                + " }" +
                                "});");
                view.loadUrl(
                        "javascript:" +
                                "$('.drag').on('dragstart',function(event){ window.DRAG.onDrag(true); });" +
                                "$('.drag').on('dragstop',function(event){ window.DRAG.onDrag(false); });"
                );
//                view.loadUrl(
//                        "javascript:" +
//                                "$('a').click(function(){window.ONANCHORCLICK.onClick($('a').index(this));});"
//                );

            }
//                if (isTemplateEditable) {
//                    if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
//                        bottomSheetBehavior.setPeekHeight(mBind.mImgArrowDown.getHeight());
//                    }
//                } else {
//                    bottomSheetBehavior.setPeekHeight(0);
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
        }
    }

//    private void showBlockComponents() {
//        mBind.mImgTemplate.setVisibility(View.GONE);
//        mBind.mImgPages.setVisibility(View.GONE);
//        mBind.mTvTemplate.setVisibility(View.GONE);
//        mBind.mTvPages.setVisibility(View.GONE);
//        mBind.mImgBlocks.setVisibility(View.VISIBLE);
//        mBind.mImgComponents.setVisibility(View.VISIBLE);
//        mBind.mTvBlocks.setVisibility(View.VISIBLE);
//        mBind.mTvComponents.setVisibility(View.VISIBLE);
//    }
//
//    private void showTemplatePages() {
//        mBind.mImgTemplate.setVisibility(View.VISIBLE);
//        mBind.mImgPages.setVisibility(View.VISIBLE);
//        mBind.mTvTemplate.setVisibility(View.VISIBLE);
//        mBind.mTvPages.setVisibility(View.VISIBLE);
//        mBind.mImgBlocks.setVisibility(View.GONE);
//        mBind.mImgComponents.setVisibility(View.GONE);
//        mBind.mTvBlocks.setVisibility(View.GONE);
//        mBind.mTvComponents.setVisibility(View.GONE);
//    }

    //    Getting image selected by clicking img tag....
    @SuppressLint("AddJavascriptInterface")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if (resultCode == Activity.RESULT_OK) {
                Gson mGson = new Gson();
                List<String> mData = mGson.fromJson(data.getStringExtra("result"), new TypeToken<List<String>>() {
                }.getType());
                if (mData.size() > 0) {
                    newImagePath = mData.get(0);
                    Luban.with(getActivity())
                            .load(newImagePath)
                            .ignoreBy(100)
                            .setCompressListener(new OnCompressListener() {
                                @Override
                                public void onStart() {

                                }

                                @Override
                                public void onSuccess(File file) {
                                    uploadImage(file.getAbsolutePath());
                                }

                                @Override
                                public void onError(Throwable e) {
                                }
                            }).launch();
                }
            }
        }
    }

    //    Event bus method trigger when new template is changed or refresh webview whose image is uploaded....
    @SuppressLint({"ClickableViewAccessibility", "AddJavascriptInterface", "SetJavaScriptEnabled"})
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefreshTemplate(EventBusRefreshWebView event) {
        if (event.isRefreshWebView()) {
            webViewArrayList.clear();
            mBind.mLlEditTemplate.removeAllViews();
            editTemplateDataList.clear();
            fnLoadTemplateBlocks(currentPageId);
        } else {
            Document document = Jsoup.parse(editTemplateDataList.get(selectedWebViewIndex).getFrames_content());
            Elements imageElements = document.getElementsByAttributeValue("src", lastSelectedImagePath);
            if (imageElements.size() > 0) {
                imageElements.get(0).attributes().put("src", newImagePath);
                editTemplateDataList.get(selectedWebViewIndex).setFrames_content(document.html());
            }
            mBind.mLlEditTemplate.removeViewAt(selectedWebViewIndex);
            webViewArrayList.remove(selectedWebViewIndex);
            fnPopulateWebViews(selectedWebViewIndex);
        }
    }

    //    Event bus method trigger when new block is added above or below of specific webview....
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddBlock(EventBusAddBlock eventBusAddBlock) {
        EditTemplateData editTemplateData = new EditTemplateData();
        editTemplateData.setPages_id(editTemplateDataList.get(selectedWebViewIndex).getPages_id());
        editTemplateData.setSites_id(GlobalClass.getInstance().mPref.getString("site_id", ""));
        editTemplateData.setFrames_content(eventBusAddBlock.getBlocksHtmlData().getBlock_html());
        editTemplateData.setFrames_height(eventBusAddBlock.getBlocksHtmlData().getBlock_height());
        editTemplateData.setFrames_original_url(eventBusAddBlock.getBlocksHtmlData().getBlock_original_url());
        final int newBlockIndex;
        if (addBlockType == 0) {
            newBlockIndex = selectedWebViewIndex;
        } else {
            newBlockIndex = selectedWebViewIndex + 1;
        }
        editTemplateDataList.add(newBlockIndex, editTemplateData);
        mBind.mLlEditTemplate.removeAllViews();
        webViewArrayList.clear();
        isDataChanged = true;
        for (int i = 0; i < editTemplateDataList.size(); i++) {
            fnPopulateWebViews(i);
        }
    }

    //    Event bus method trigger when new component is added inside web view....
    @SuppressLint("SetJavaScriptEnabled")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddComponent(EventBusAddComponent eventBusAddComponent) {
        Document document = Jsoup.parse(editTemplateDataList.get(0).getFrames_content());
        Elements elements = document.getElementsByClass("block");
        elements.get(0).append(eventBusAddComponent.getComponentHtml());
        editTemplateDataList.get(0).setFrames_content(document.html());
        mBind.mLlEditTemplate.removeViewAt(0);
        webViewArrayList.remove(0);
        isDataChanged = true;
        fnPopulateWebViews(0);
    }

    //    Event bus method trigger when new component is added inside web view....
    @SuppressLint("SetJavaScriptEnabled")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAddNewPage(EventBusAddNewPage eventBusAddNewPage) {
        fnGetSitePages();
    }

    //    Method to load template blocks....
    private void fnLoadTemplateBlocks(String pageId) {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallback = Api.WEB_SERVICE.loadTemplateBlocks("55555", GlobalClass.getInstance().mPref.getString("site_id", ""), pageId);
        mCallback.enqueue(new Callback<EditTemplateModel>() {
            @Override
            public void onResponse(Call<EditTemplateModel> call, Response<EditTemplateModel> response) {
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            editTemplateDataList = response.body().getData();
                            for (int i = 0; i < editTemplateDataList.size(); i++) {
                                Document document = Jsoup.parse(editTemplateDataList.get(i).getFrames_content());
                                document.select("script").remove();
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://fennixpro.com/dragdropassets/demo.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://www.jqueryscript.net/css/jquerysctipttop.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://fennixpro.com/dragdropassets/draganddrop.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "https://bootswatch.com/flatly/bootstrap.min.css");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/jquery-1.12.4.min.js");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/draganddrop.js");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/demo.js");
                                editTemplateDataList.get(i).setFrames_content(document.html());
                                fnPopulateWebViews(i);
                                mBind.mImgPagesArrow.setVisibility(View.VISIBLE);
                            }
                            break;
                        case "0":
                            mBind.mPb.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<EditTemplateModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    //    Method to create new page....
    private void fnCreateNewPage(String pageName) {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        mBind.mPb.setVisibility(View.VISIBLE);
        createPageCallback = Api.WEB_SERVICE.createNewPage("55555", GlobalClass.getInstance().mPref.getString("site_id", ""), pageName);
        createPageCallback.enqueue(new Callback<CreatePageObject>() {
            @Override
            public void onResponse(Call<CreatePageObject> call, Response<CreatePageObject> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            Bundle bundle = new Bundle();
                            bundle.putString("mFlag", "create_page");
                            bundle.putString("new_page_id", response.body().getData().getNew_page_id());
                            ((MainActivity) getActivity()).mFScreenName = "SELECT TEMPLATE";
                            ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", true, bundle);
                            break;
                        case "0":
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            mBind.mPb.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CreatePageObject> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    //    Method to get pages related to current site....
    private void fnGetSitePages() {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (progressBarPages != null)
            progressBarPages.setVisibility(View.VISIBLE);
        sitePagesCallback = Api.WEB_SERVICE.getSitePages("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
        sitePagesCallback.enqueue(new Callback<SitePagesObject>() {
            @Override
            public void onResponse(Call<SitePagesObject> call, Response<SitePagesObject> response) {
                if (progressBarPages != null)
                    progressBarPages.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            sitePagesDataList = response.body().getData();
                            setSitePages();
                            break;
                        case "0":
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (progressBarPages != null)
                                progressBarPages.setVisibility(View.INVISIBLE);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<SitePagesObject> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                if (progressBarPages != null)
                    progressBarPages.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    //    Method to upload image....
    private void uploadImage(String image) {
        mBind.mPb.setVisibility(View.VISIBLE);
        if (!image.equals("")) {
            File file = new File(image);
            MultipartBody.Part imagePart = MultipartBody.Part.createFormData("tempImage", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            imageUploadCallback = Api.WEB_SERVICE.uploadTemplateImage("55555", imagePart);
            imageUploadCallback.enqueue(new Callback<TemplateImageUploadModel>() {
                @Override
                public void onResponse(Call<TemplateImageUploadModel> call, Response<TemplateImageUploadModel> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                newImagePath = response.body().getData();
                                isDataChanged = true;
                                EventBus.getDefault().post(new EventBusRefreshWebView(false));
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Could not upload image.", R.color.error);
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<TemplateImageUploadModel> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        }
    }

    //    Method to save edited template page....
    private void saveEditedTemplate(String json, String flag) {
        mBind.mPb.setVisibility(View.VISIBLE);
        saveTemplateCallback = Api.WEB_SERVICE.saveEditedTemplate("55555", json, "1", flag);
        saveTemplateCallback.enqueue(new Callback<GenericModelStatusMessage>() {
            @Override
            public void onResponse(Call<GenericModelStatusMessage> call, Response<GenericModelStatusMessage> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            isDataChanged = false;
                            EventBus.getDefault().post(new EventBusRefreshHomeWebView(true));
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.website_saved), R.color.success);
                            getActivity().onBackPressed();
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericModelStatusMessage> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}