package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragProfileBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;

public class FragProfile extends Fragment implements View.OnClickListener {

    private FragProfileBinding mBind;
    private Call<GenericModelStatusMessage> mCallLogOut;

    public FragProfile() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_profile, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallLogOut != null && mCallLogOut.isExecuted()) {
            mCallLogOut.cancel();
        }
    }

    private void init() {
        mBind.mTvName.setText(GlobalClass.getInstance().mPref.getString("first_name", ""));
        mBind.mTvEmail.setText(GlobalClass.getInstance().mPref.getString("email", ""));
        mBind.mTvEditProfile.setOnClickListener(this);
        mBind.mTvLogout.setOnClickListener(this);
        mBind.mTvChangeLangugae.setOnClickListener(this);
        mBind.mTvHelpSupport.setOnClickListener(this);

        String mUrl;
        if (GlobalClass.getInstance().mPref.getBoolean("fb", false)) {
            mUrl = GlobalClass.getInstance().mPref.getString("profile_image", "");
        } else {
            mUrl = ((MainActivity) getActivity()).mFBaseUrlImages + "/" + GlobalClass.getInstance().mPref.getString("profile_image", "");
        }
        Glide.with(getActivity()).load(mUrl)
                .apply(new RequestOptions().centerCrop())
                .apply(new RequestOptions().circleCrop())
                .apply(new RequestOptions().placeholder(R.drawable.prifile_image))
                .apply(new RequestOptions().error(R.drawable.prifile_image))
                .into(mBind.mImgProfile);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvEditProfile:
                ((MainActivity) getActivity()).fnLoadFragAdd("EDIT PROFILE", true, null);
                break;
            case R.id.mTvLogout:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                reqLogOut();
//                ((MainActivity) getActivity()).fnLogOut();
                break;
            case R.id.mTvChangeLangugae:
                Bundle bundle = new Bundle();
                bundle.putString("mFlag", "profile");
                ((MainActivity) getActivity()).fnLoadFragAdd("LANGUAGE", true, bundle);
                break;
            case R.id.mTvHelpSupport:
                Bundle bundle1 = new Bundle();
                bundle1.putString("mFlag", "help");
                ((MainActivity) getActivity()).fnLoadFragAdd("TERMS", true, bundle1);
                break;
        }
    }

    private void reqLogOut() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallLogOut = Api.WEB_SERVICE.log_out("55555", GlobalClass.getInstance().mPref.getString("id", ""), "3", GlobalClass.getInstance().mPref.getString("fcm_token", ""));
        mCallLogOut.enqueue(new Callback<GenericModelStatusMessage>() {
            @Override
            public void onResponse(Call<GenericModelStatusMessage> call, retrofit2.Response<GenericModelStatusMessage> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            ((MainActivity) getActivity()).fnLogOut();
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GenericModelStatusMessage> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null && getActivity() != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    if (getActivity() != null)
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }
}