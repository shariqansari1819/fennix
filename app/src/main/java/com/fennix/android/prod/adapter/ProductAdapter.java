package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.model.ProductModel;
import com.fennix.android.prod.model.eventbus.EventBusProductId;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;
import com.fennix.android.prod.model.products.ProductsData;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductsHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<ProductsData> productList;

    public ProductAdapter(Context context, List<ProductsData> userTemplatesListData) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.productList = userTemplatesListData;
    }

    @NonNull
    @Override
    public ProductsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_products, parent, false);
        return new ProductsHolder(view, new ProductsHolder.OnProductClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusProductId(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsHolder holder, int position) {
        ProductsData data = productList.get(position);
        Glide.with(context)
                .load(((MainActivity) context).mSiteThumbnailBaseUrl + "/shop/" + data.getProimgPath())
                .into(holder.mImgProduct);
        holder.textViewName.setText(data.getPro_title());
        holder.textViewPrice.setText(data.getPro_currency() + " " + data.getPro_price());
        if (data.getPro_stock_status().equalsIgnoreCase("1")) {
            holder.textViewInStock.setText("In Stock");
        } else {
            holder.textViewInStock.setText("Out of Stock");
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class ProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView mImgProduct;
        TextView textViewName, textViewPrice, textViewInStock;
        private OnProductClick onTemplateClickListener;

        ProductsHolder(View itemView, OnProductClick onTemplateClickListener) {
            super(itemView);
            this.onTemplateClickListener = onTemplateClickListener;
            mImgProduct = itemView.findViewById(R.id.mImgProduct);
            textViewName = itemView.findViewById(R.id.mTvNameProduct);
            textViewPrice = itemView.findViewById(R.id.mTvPrice);
            textViewInStock = itemView.findViewById(R.id.mTvInStock);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onTemplateClickListener.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnProductClick {
            void rowClick(int position, int id);
        }
    }
}