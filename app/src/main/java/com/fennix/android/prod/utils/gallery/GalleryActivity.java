package com.fennix.android.prod.utils.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.fennix.android.prod.R;
import com.fennix.android.prod.databinding.ActivityGalleryBinding;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class GalleryActivity extends AppCompatActivity implements View.OnClickListener {

    private AsynLoadImages mAsynLoadImages;
    private ActivityGalleryBinding mBind;
    private Context mContext;
    private List<GalleryModel> mLstMedia;
    private GalleryAdopter mAdopter;
    private List<String> mLstSelected;
    String[] mArAlbums;
    private int lastIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBind = DataBindingUtil.setContentView(this, R.layout.activity_gallery);
        mContext = this;
        init();
    }

    private void init() {
        mBind.mImgDone.setOnClickListener(this);
        mBind.mTvFilter.setOnClickListener(this);
        switch (getIntent().getStringExtra("mFlag")) {
            case "single":
                mBind.mTvTitle.setVisibility(View.INVISIBLE);
                break;
        }
        mAsynLoadImages = new AsynLoadImages();
        mAsynLoadImages.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectEvent(EventGalleryModel mObj) {
        switch (getIntent().getStringExtra("mFlag")) {
            case "multiple":
                if (mLstSelected.contains(mLstMedia.get(mObj.getmPos()).getmPath())) {
                    mLstMedia.get(mObj.getmPos()).setmSelected(false);
                    mLstSelected.remove(mLstMedia.get(mObj.getmPos()).getmPath());
                    mAdopter.notifyItemChanged(mObj.getmPos());
                } else {
                    mLstMedia.get(mObj.getmPos()).setmSelected(true);
                    mLstSelected.add(mLstMedia.get(mObj.getmPos()).getmPath());
                    mAdopter.notifyItemChanged(mObj.getmPos());
                }
                mBind.mTvTitle.setText("" + mLstSelected.size());
                break;
            case "single":
                if (lastIndex != mObj.getmPos()) {
                    if (lastIndex != -1)
                        mLstMedia.get(lastIndex).setmSelected(false);
                    mLstMedia.get(mObj.getmPos()).setmSelected(true);
                    if (mLstSelected.size() > 0) {
                        mLstSelected.remove(mLstSelected.size() - 1);
                    }
                    mLstSelected.add(mLstMedia.get(mObj.getmPos()).getmPath());
                    if (lastIndex != -1)
                        mAdopter.notifyItemChanged(lastIndex);
                    mAdopter.notifyItemChanged(mObj.getmPos());
                    lastIndex = mObj.getmPos();
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        lastIndex = -1;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgDone:
                if (mLstSelected.size() == 0) {
                    Toast.makeText(mContext, "Please Select Atleast One Picture", Toast.LENGTH_SHORT).show();
                } else {
                    Gson mGson = new Gson();
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", mGson.toJson(mLstSelected));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
            case R.id.mTvFilter:
                AlertDialog.Builder mAlertBuilder;
                mAlertBuilder = new AlertDialog.Builder(mContext, R.style.MyAlertDialogStyle);
                mAlertBuilder.setItems(mArAlbums, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!mBind.mTvFilter.getText().toString().contentEquals(mArAlbums[i])) {
                            mLstSelected.clear();
                            mBind.mTvTitle.setText("0");
                            int mSizeLst = mLstMedia.size();
                            mLstMedia.clear();
                            mAdopter.notifyItemRangeRemoved(0, mSizeLst);
                            String[] projection = {MediaStore.Images.Media.DATA};
                            switch (mArAlbums[i]) {
                                case "All Images":
                                    Cursor curAll = GalleryActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            projection, // Which columns to return
                                            null,       // Which rows to return (all rows)
                                            null,       // Selection arguments (none)
                                            null        // Ordering
                                    );
                                    if (curAll != null && curAll.moveToFirst()) {
                                        int datColum = curAll.getColumnIndex(MediaStore.Images.Media.DATA);
                                        do {
                                            mLstMedia.add(new GalleryModel("", curAll.getString(datColum), "", false));
                                        } while (curAll.moveToNext());
                                        curAll.close();
                                    } else {
                                        Toast.makeText(mContext, "No Pictures Found", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                default:
                                    Cursor cur = GalleryActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                            projection, // Which columns to return
                                            MediaStore.Images.Media.DATA + " like ? ",       // Which rows to return (all rows)
                                            new String[]{"%" + mArAlbums[i] + "%"},       // Selection arguments (none)
                                            null        // Ordering
                                    );
                                    int index = cur.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                                    while (cur.moveToNext()) {
                                        mLstMedia.add(new GalleryModel("", cur.getString(index), "", false));
                                    }
                                    cur.close();
                                    break;
                            }
                            mAdopter.notifyItemRangeInserted(0, mLstMedia.size());
                            mBind.mTvFilter.setText(mArAlbums[i]);
                        }
                    }
                });
                mAlertBuilder.setNegativeButton("Cancel", null);
                mAlertBuilder.show();
                break;
        }
    }

    private class AsynLoadImages extends AsyncTask<Boolean, Void, Boolean> {
        private AsynLoadImages() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mBind.mPb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            mLstMedia = new ArrayList<>();
            mLstSelected = new ArrayList<>();
            String[] projection = new String[]{
                    MediaStore.Images.Media.DATA
            };
            // content:// style URI for the "primary" external storage volume
            // Make the query.
            Cursor cur = GalleryActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection, // Which columns to return
                    null,       // Which rows to return (all rows)
                    null,       // Selection arguments (none)
                    null        // Ordering
            );
            if (cur != null && cur.moveToFirst()) {
//                int bucketColumn = cur.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
//                int nameColumn = cur.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                int datColum = cur.getColumnIndex(MediaStore.Images.Media.DATA);
                do {
                    mLstMedia.add(new GalleryModel("", cur.getString(datColum), "", false));
                } while (cur.moveToNext());
                cur.close();
                // query for albums
                String[] mAlbums = new String[]{"DISTINCT " + MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME};
                Cursor mCurAlbum = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, mAlbums, null, null, null);
                mArAlbums = new String[mCurAlbum.getCount() + 1];
                mArAlbums[0] = "All Images";
                while (mCurAlbum.moveToNext()) {
                    mArAlbums[mCurAlbum.getPosition() + 1] = mCurAlbum.getString((mCurAlbum.getColumnIndex(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME)));
                }
                mCurAlbum.close();
            } else {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean mVal) {
            super.onPostExecute(mVal);
            mBind.mPb.setVisibility(View.INVISIBLE);
            if (mVal) {
                mBind.mRv.setLayoutManager(new GridLayoutManager(mContext, 4));
                mAdopter = new GalleryAdopter(mContext, mLstMedia);
                mBind.mRv.setAdapter(mAdopter);
            } else {
                Toast.makeText(mContext, "No Pictures Found", Toast.LENGTH_SHORT).show();
            }
        }
    }
}