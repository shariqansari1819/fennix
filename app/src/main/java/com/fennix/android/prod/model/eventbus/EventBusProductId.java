package com.fennix.android.prod.model.eventbus;

public class EventBusProductId {
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public EventBusProductId(int mProId) {
        this.position = mProId;
    }
}
