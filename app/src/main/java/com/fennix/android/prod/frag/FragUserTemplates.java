package com.fennix.android.prod.frag;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.adapter.UserTemplateAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragUserTemplatesBinding;
import com.fennix.android.prod.model.BuildSiteModel;
import com.fennix.android.prod.model.eventbus.EventBusAddNewPage;
import com.fennix.android.prod.model.eventbus.EventBusRefreshHomeWebView;
import com.fennix.android.prod.model.eventbus.EventBusRefreshWebView;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;
import com.fennix.android.prod.model.templates.Data;
import com.fennix.android.prod.model.templates.UserTemplates;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragUserTemplates extends Fragment implements View.OnClickListener {

    private FragUserTemplatesBinding mBind;
    private Call<UserTemplates> templatesCall;
    private Call<BuildSiteModel> buildSiteCall;
    private List<Data> userTemplatesList = new ArrayList<>();
    private UserTemplateAdapter userTemplateAdapter;
    private int lastIndex;

    public FragUserTemplates() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_user_templates, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.getRoot());
        mBind.mWvUserTemplate.getSettings().setJavaScriptEnabled(true);
        mBind.mWvUserTemplate.getSettings().setLoadWithOverviewMode(true);
        mBind.mWvUserTemplate.getSettings().setUseWideViewPort(true);
        mBind.mWvUserTemplate.getSettings().setPluginState(WebSettings.PluginState.ON);
        mBind.mWvUserTemplate.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBind.mPb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBind.mPb.setVisibility(View.INVISIBLE);
            }
        });
        if (getArguments().getString("mFlag", "").equals("edit_template")) {
            mBind.mTvSubDomain.setVisibility(View.GONE);
            mBind.mTvBuild.setText(R.string.change);
        } else {
            mBind.mTvSubDomain.setText(GlobalClass.getInstance().mPref.getString("sub_domain", ""));
            mBind.mTvSubDomain.setVisibility(View.VISIBLE);
        }
        fnGetTemplates();
        mBind.mTvBuild.setOnClickListener(this);
        mBind.mImgBack.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        if (templatesCall != null && templatesCall.isExecuted()) {
            templatesCall.cancel();
        }
        if (buildSiteCall != null && buildSiteCall.isExecuted()) {
            buildSiteCall.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvBuild:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (getArguments().getString("mFlag", "").equals("create_page")) {
                    fnBuildSite("0", getArguments().getString("new_page_id", ""));
                } else {
                    fnBuildSite("0", "");
                }
                break;
            case R.id.mImgBack:
                if (getArguments().getString("mFlag", "").equals("edit_template"))
                    getActivity().onBackPressed();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void userTemplateEvent(EventBusTemplateId event) {
        if (event.getmPos() != lastIndex) {
            userTemplatesList.get(event.getmPos()).setSelected(true);
            userTemplatesList.get(lastIndex).setSelected(false);
            userTemplateAdapter.notifyItemChanged(event.getmPos());
            userTemplateAdapter.notifyItemChanged(lastIndex);
            funLoadWebsite(userTemplatesList.get(event.getmPos()).getPages_preview());
            GlobalClass.getInstance().mPrefEditor.putString("template_cat_id", userTemplatesList.get(0).getTemplate_cat_id());
            GlobalClass.getInstance().mPrefEditor.putString("page_id", userTemplatesList.get(event.getmPos()).getPages_id());
            GlobalClass.getInstance().mPrefEditor.putString("site_thumb", userTemplatesList.get(event.getmPos()).getSitethumb());
            GlobalClass.getInstance().mPrefEditor.putString("temp_site_id", userTemplatesList.get(0).getSites_id());
            GlobalClass.getInstance().mPrefEditor.apply();
            lastIndex = event.getmPos();
        }
    }

    private void funLoadWebsite(String pagePreview) {
        mBind.mWvUserTemplate.loadData(pagePreview, "text/html; charset=utf-8", "UTF-8");
    }

    private void fnGetTemplates() {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        mBind.mPb.setVisibility(View.VISIBLE);
        try {
            templatesCall = Api.WEB_SERVICE.getTemplates("55555", GlobalClass.getInstance().mPref.getString("id", ""));
            templatesCall.enqueue(new Callback<UserTemplates>() {
                @Override
                public void onResponse(Call<UserTemplates> call, Response<UserTemplates> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                userTemplatesList = response.body().getData();
                                mBind.mRvUserTemplates.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                                userTemplateAdapter = new UserTemplateAdapter(getActivity(), userTemplatesList);
                                mBind.mRvUserTemplates.setAdapter(userTemplateAdapter);
                                GlobalClass.getInstance().mPrefEditor.putString("template_cat_id", userTemplatesList.get(0).getTemplate_cat_id());
                                GlobalClass.getInstance().mPrefEditor.putString("page_id", userTemplatesList.get(0).getPages_id());
                                GlobalClass.getInstance().mPrefEditor.putString("site_thumb", userTemplatesList.get(0).getSitethumb());
                                GlobalClass.getInstance().mPrefEditor.putString("temp_site_id", userTemplatesList.get(0).getSites_id());
                                GlobalClass.getInstance().mPrefEditor.apply();
                                userTemplatesList.get(0).setSelected(true);
                                userTemplateAdapter.notifyItemChanged(0);
                                funLoadWebsite(userTemplatesList.get(0).getPages_preview());
                                break;
                            case "0":
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserTemplates> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void fnBuildSite(String type, String flag) {
        mBind.mPb.setVisibility(View.VISIBLE);
        try {
            buildSiteCall = Api.WEB_SERVICE.buildSite("55555", GlobalClass.getInstance().mPref.getString("page_id", ""), GlobalClass.getInstance().mPref.getString("site_id", ""), type, flag);
            buildSiteCall.enqueue(new Callback<BuildSiteModel>() {
                @Override
                public void onResponse(Call<BuildSiteModel> call, Response<BuildSiteModel> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response.isSuccessful() && response != null) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().mPrefEditor.putString("page_preview", response.body().getPages_preview()).apply();
                                if (getArguments().getString("mFlag", "").equals("edit_template")) {
                                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.template_changed), R.color.success);
                                    EventBus.getDefault().post(new EventBusRefreshWebView(true));
                                    EventBus.getDefault().post(new EventBusRefreshHomeWebView(true));
                                    getActivity().onBackPressed();
                                } else if (getArguments().getString("mFlag", "").equals("create_page")) {
                                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Page created successfully.", R.color.success);
                                    getActivity().onBackPressed();
                                    EventBus.getDefault().post(new EventBusAddNewPage());
                                } else {
                                    ((MainActivity) getActivity()).fnLoadFragReplace("WEBSITE GIF", null);
                                }
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.template_not_created), R.color.error);
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<BuildSiteModel> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}