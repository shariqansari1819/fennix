package com.fennix.android.prod.frag;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragProductAddEditBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.eventbus.EventBusGetProduct;
import com.fennix.android.prod.model.eventbus.EventBusProfileImagePath;
import com.fennix.android.prod.model.eventbus.EventBusSelectedOptionSend;
import com.fennix.android.prod.model.eventbus.EventBusSelectedOptions;
import com.fennix.android.prod.model.products.AddProductsMainObject;
import com.fennix.android.prod.model.products.categories.GetCategoriesData;
import com.fennix.android.prod.model.products.categories.GetCategoriesMainObject;
import com.fennix.android.prod.model.products.productdetails.GetProductDetailData;
import com.fennix.android.prod.model.products.productdetails.GetProductDetailMainObject;
import com.fennix.android.prod.utils.GlobalClass;
import com.fennix.android.prod.utils.gallery.GalleryActivity;
import com.fennix.android.prod.utils.image_compressor.Luban;
import com.fennix.android.prod.utils.image_compressor.OnCompressListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragProductEdit extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private FragProductAddEditBinding mBind;

    //    Android fields....
    private String productImagePath = "";
    private int selectedImageIndex = -1;
    String[] productImages = new String[3];
    List<MultipartBody.Part> productImageList = new ArrayList<>();
    String options = "";
    private LayoutInflater layoutInflater;
    private AlertDialog categoryDialog;
    private ListView listViewCategory;
    private ProgressBar progressBarCategory;
    private String categoryId = "";
    List<String> optionsList = new ArrayList<>();
    private String productId;
    private GetProductDetailMainObject getProductDetailMainObject;
    private String productEditType = "";

    //    Array list fields....
    private List<GetCategoriesData> siteCategoryDataList = new ArrayList<>();

    //    Retrofit calls fields....
    private Call<GetCategoriesMainObject> getCategoryCallback;
    private Call<AddProductsMainObject> mCallAddProducts;
    private Call<GenericModelStatusMessage> mCallDelete;
    private Call<GetProductDetailMainObject> mCallDetails;

    public FragProductEdit() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_product_add_edit, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void init() {
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            productEditType = getArguments().getString("product_edit_type", "");
            if (productEditType != null) {
                if (productEditType.equals("add")) {
                    mBind.mTvDeleteProduct.setVisibility(View.GONE);
                } else {
                    mBind.mTvDeleteProduct.setVisibility(View.VISIBLE);
                    productId = getArguments().getString("product_id", "");
                    if (!productId.equals(""))
                        reqDetails(productId);
                }
            }
        }

        mBind.mNsParent.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                mBind.mNsProductImages.getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });
        mBind.mNsProductImages.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;

            }
        });
        mBind.mImProduct.setOnClickListener(this);
        mBind.mImProduct1.setOnClickListener(this);
        mBind.mImProduct2.setOnClickListener(this);
        mBind.mImgTick.setOnClickListener(this);
        mBind.mImgCategory.setOnClickListener(this);
        mBind.mImgProductOption.setOnClickListener(this);
        mBind.mImgBack.setOnClickListener(this);
        mBind.mTvDeleteProduct.setOnClickListener(this);
        layoutInflater = LayoutInflater.from(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCallAddProducts != null && mCallAddProducts.isExecuted()) {
            mCallAddProducts.cancel();
        }
        if (mCallDelete != null && mCallDelete.isExecuted()) {
            mCallDelete.cancel();
        }
        if (mCallDetails != null && mCallDetails.isExecuted()) {
            mCallDetails.cancel();
        }
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectedOptionsClick(EventBusSelectedOptions eventBusSelectedOptions) {
        if (eventBusSelectedOptions.getSelectedOptionsList().size() == 0) {
            mBind.mTvBlackWhite.setText(R.string.no_category_found);
        } else {
            optionsList = eventBusSelectedOptions.getSelectedOptionsList();
            options = eventBusSelectedOptions.getSelectedOptionsList().toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
            mBind.mTvBlackWhite.setText(options);
        }
    }

    @SuppressLint("CheckResult")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventBusProfileImagePath event) {
        productImagePath = event.getImagePath();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();
        productImages[selectedImageIndex] = productImagePath;
        if (selectedImageIndex == 0)
            Glide.with(getActivity()).load(productImagePath)
                    .apply(requestOptions)
                    .into(mBind.mImProduct);
        else if (selectedImageIndex == 1) {
            Glide.with(getActivity()).load(productImagePath)
                    .apply(requestOptions)
                    .into(mBind.mImProduct1);
        } else {
            Glide.with(getActivity()).load(productImagePath)
                    .apply(requestOptions)
                    .into(mBind.mImProduct2);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImProduct:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (((MainActivity) getActivity()).fnCheckPermission()) {
                    selectedImageIndex = 0;
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra("mFlag", "single");
                    getActivity().startActivityForResult(intent, 1);
                } else {
                    ((MainActivity) getActivity()).fnRequestPermission(3);
                }
                break;
            case R.id.mImProduct1:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (((MainActivity) getActivity()).fnCheckPermission()) {
                    selectedImageIndex = 1;
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra("mFlag", "single");
                    getActivity().startActivityForResult(intent, 1);
                } else {
                    ((MainActivity) getActivity()).fnRequestPermission(3);
                }
                break;
            case R.id.mImProduct2:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (((MainActivity) getActivity()).fnCheckPermission()) {
                    selectedImageIndex = 2;
                    Intent intent = new Intent(getActivity(), GalleryActivity.class);
                    intent.putExtra("mFlag", "single");
                    getActivity().startActivityForResult(intent, 1);
                } else {
                    ((MainActivity) getActivity()).fnRequestPermission(3);
                }
                break;
            case R.id.mImgTick:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (productEditType.equals("add")) {
                    if (selectedImageIndex == -1) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_image), R.color.error);
                    } else if (productImages[0] == null || productImages[0].equals("")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_feature_image), R.color.error);
                    } else if (mBind.mEtProName == null || mBind.mEtProName.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_name), R.color.error);
                    } else if (mBind.mEtProPrice == null || mBind.mEtProPrice.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_price), R.color.error);
                    } else if (mBind.mEtProDisc == null || mBind.mEtProDisc.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_description), R.color.error);
                    } else if (categoryId == null || categoryId.isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_category), R.color.error);
                    } else {
                        productImageList.clear();
                        int selectedImageCounter = 0;
                        for (int i = 0; i < productImages.length; i++) {
                            if (productImages[i] != null) {
                                selectedImageCounter++;
                            }
                        }
                        for (int i = 0; i < productImages.length; i++) {
                            if (productImages[i] != null && !productImages[i].equals("")) {
                                final int finalI = i;
                                final int finalSelectedImageCounter = selectedImageCounter;
                                Luban.with(getActivity())
                                        .load(productImages[i])
                                        .ignoreBy(100)
                                        .setCompressListener(new OnCompressListener() {
                                            @Override
                                            public void onStart() {

                                            }

                                            @Override
                                            public void onSuccess(File file) {
                                                productImageList.add(prepareFilePart("pro_images[" + finalI + "]", productImages[finalI]));
                                                if (productImageList.size() == finalSelectedImageCounter) {
                                                    reqAddProduct(GlobalClass.getInstance().mPref.getString("site_id", ""), mBind.mEtProName.getText().toString(), mBind.mEtProDisc.getText().toString(), mBind.mEtProPrice.getText().toString(), mBind.mScSetAsHidden.isChecked() ? "2" : "1", categoryId, options, "0", GlobalClass.getInstance().mPref.getString("id", ""));
                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                            }
                                        }).launch();
                            }
                        }
                    }
                } else {
                    if (mBind.mEtProName == null || mBind.mEtProName.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_name), R.color.error);
                    } else if (mBind.mEtProPrice == null || mBind.mEtProPrice.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_price), R.color.error);
                    } else if (mBind.mEtProDisc == null || mBind.mEtProDisc.getText().toString().isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_enter_product_description), R.color.error);
                    } else if (categoryId == null || categoryId.isEmpty()) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.please_select_category), R.color.error);
                    } else {
                        if (selectedImageIndex == -1) {
                            reqEditProduct(GlobalClass.getInstance().mPref.getString("site_id", ""), mBind.mEtProName.getText().toString(), mBind.mEtProDisc.getText().toString(), mBind.mEtProPrice.getText().toString(), mBind.mScSetAsHidden.isChecked() ? "2" : "1", categoryId, options, "0", getProductDetailMainObject.getData().getPro_id());
                        } else {
                            final int[] imageCounter = {-1};
                            productImageList.clear();
                            int selectedImageCounter = 0;
                            for (int i = 0; i < productImages.length; i++) {
                                if (productImages[i] != null) {
                                    selectedImageCounter++;
                                }
                            }
                            for (int i = 0; i < productImages.length; i++) {
                                if (productImages[i] != null && !productImages[i].equals("")) {
                                    imageCounter[0]++;
                                    final int finalI = i;
                                    final int finalSelectedImageCounter = selectedImageCounter;
                                    final int[] finalImageCounter = {imageCounter[0]};
                                    Luban.with(getActivity())
                                            .load(productImages[i])
                                            .ignoreBy(100)
                                            .setCompressListener(new OnCompressListener() {
                                                @Override
                                                public void onStart() {

                                                }

                                                @Override
                                                public void onSuccess(File file) {
                                                    productImageList.add(prepareFilePart("pro_images[" + finalImageCounter[0] + "]", productImages[finalI]));
                                                    if (productImageList.size() == finalSelectedImageCounter) {
                                                        reqEditProduct(GlobalClass.getInstance().mPref.getString("site_id", ""), mBind.mEtProName.getText().toString(), mBind.mEtProDisc.getText().toString(), mBind.mEtProPrice.getText().toString(), mBind.mScSetAsHidden.isChecked() ? "2" : "1", categoryId, options, "0", getProductDetailMainObject.getData().getPro_id());
                                                        imageCounter[0] = -1;
                                                    }
                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                }
                                            }).launch();
                                }
                            }
                        }
                    }
                }
                break;
            case R.id.mTvDeleteProduct:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (!productId.equals(""))
                    reqDelete(productId);
                break;
            case R.id.mImgCategory:
                AlertDialog.Builder categoryDialogBuilder = new AlertDialog.Builder(getActivity());
                final View dialogCategoryView = layoutInflater.inflate(R.layout.dialog_pages_list, null);
                categoryDialogBuilder.setView(dialogCategoryView);
                categoryDialog = categoryDialogBuilder.create();
                categoryDialog.show();
                listViewCategory = dialogCategoryView.findViewById(R.id.mLstPages);
                progressBarCategory = dialogCategoryView.findViewById(R.id.mPb);
                if (siteCategoryDataList.size() != 0) {
                    setSiteCategory();
                } else {
                    fnGetProductCategory();
                }
                TextView textViewCancel = dialogCategoryView.findViewById(R.id.mTvCancel);
                listViewCategory.setOnItemClickListener(this);
                textViewCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        categoryDialog.dismiss();
                    }
                });
                break;
            case R.id.mImgProductOption:
                ((MainActivity) getActivity()).fnLoadFragAdd("PRODUCT OPTIONS", true, null);
                if (optionsList.size() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new EventBusSelectedOptionSend(optionsList));
                        }
                    }, 100);
                }
                break;
            case R.id.mImgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    private void setSiteCategory() {
        String arr[] = new String[siteCategoryDataList.size()];
        for (int i = 0; i < siteCategoryDataList.size(); i++) {
            arr[i] = siteCategoryDataList.get(i).getCat_title();
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, arr);
        if (listViewCategory != null)
            listViewCategory.setAdapter(arrayAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        categoryDialog.dismiss();
        String category = siteCategoryDataList.get(i).getCat_title();
        categoryId = siteCategoryDataList.get(i).getCat_id();
        mBind.mTvCategoryOne.setText(category);
    }

    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {
        File file = new File(fileUri);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestBody);
    }

    private void fnGetProductCategory() {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (progressBarCategory != null)
            progressBarCategory.setVisibility(View.VISIBLE);
        getCategoryCallback = Api.WEB_SERVICE.getCategories("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
        getCategoryCallback.enqueue(new Callback<GetCategoriesMainObject>() {
            @Override
            public void onResponse(Call<GetCategoriesMainObject> call, Response<GetCategoriesMainObject> response) {
                if (progressBarCategory != null)
                    progressBarCategory.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            siteCategoryDataList = response.body().getData();
                            setSiteCategory();
                            break;
                        case "0":
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            if (progressBarCategory != null)
                                progressBarCategory.setVisibility(View.INVISIBLE);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GetCategoriesMainObject> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                if (progressBarCategory != null)
                    progressBarCategory.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    public void reqEditProduct(String siteId, String proTitle, String proDescription, String proPrice, String proStatus,
                               String proCatId, String optionName, String featureIndex, String productId) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            final RequestBody site_id = RequestBody.create(MediaType.parse("text/plain"), siteId);
            final RequestBody pro_title = RequestBody.create(MediaType.parse("text/plain"), proTitle);
            final RequestBody pro_description = RequestBody.create(MediaType.parse("text/plain"), proDescription);
            final RequestBody pro_price = RequestBody.create(MediaType.parse("text/plain"), proPrice);
            final RequestBody pro_status = RequestBody.create(MediaType.parse("text/plain"), proStatus);
            final RequestBody pro_cat_id = RequestBody.create(MediaType.parse("text/plain"), proCatId);
            final RequestBody option_name = RequestBody.create(MediaType.parse("text/plain"), optionName);
            final RequestBody featuredIndex = RequestBody.create(MediaType.parse("text/plain"), featureIndex);
            final RequestBody product_id = RequestBody.create(MediaType.parse("text/plain"), productId);

            mCallAddProducts = Api.WEB_SERVICE.updateProduct("55555", productImageList, site_id, pro_title, pro_description, pro_price, pro_status, pro_cat_id, option_name, featuredIndex, product_id);
            mCallAddProducts.enqueue(new Callback<AddProductsMainObject>() {
                @Override
                public void onResponse(Call<AddProductsMainObject> call, Response<AddProductsMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.products_updated), R.color.success);
                                EventBus.getDefault().post(new EventBusGetProduct());
                                getActivity().onBackPressed();
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<AddProductsMainObject> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void reqAddProduct(String siteId, String proTitle, String proDescription, String proPrice, String proStatus,
                              String proCatId, String optionName, String featureIndex, String addedBy) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            final RequestBody site_id = RequestBody.create(MediaType.parse("text/plain"), siteId);
            final RequestBody pro_title = RequestBody.create(MediaType.parse("text/plain"), proTitle);
            final RequestBody pro_description = RequestBody.create(MediaType.parse("text/plain"), proDescription);
            final RequestBody pro_price = RequestBody.create(MediaType.parse("text/plain"), proPrice);
            final RequestBody pro_status = RequestBody.create(MediaType.parse("text/plain"), proStatus);
            final RequestBody pro_cat_id = RequestBody.create(MediaType.parse("text/plain"), proCatId);
            final RequestBody option_name = RequestBody.create(MediaType.parse("text/plain"), optionName);
            final RequestBody featuredIndex = RequestBody.create(MediaType.parse("text/plain"), featureIndex);
            final RequestBody added_by = RequestBody.create(MediaType.parse("text/plain"), addedBy);

            mCallAddProducts = Api.WEB_SERVICE.add_product_list("55555", productImageList, site_id, pro_title, pro_description, pro_price, pro_status, pro_cat_id, option_name, featuredIndex, added_by);
            mCallAddProducts.enqueue(new Callback<AddProductsMainObject>() {
                @Override
                public void onResponse(Call<AddProductsMainObject> call, Response<AddProductsMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.products_add), R.color.success);
                                EventBus.getDefault().post(new EventBusGetProduct());
                                getActivity().onBackPressed();
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<AddProductsMainObject> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void reqDelete(String productId) {
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallDelete = Api.WEB_SERVICE.delete_pro("55555", productId);
        mCallDelete.enqueue(new Callback<GenericModelStatusMessage>() {
            @Override
            public void onResponse(Call<GenericModelStatusMessage> call, retrofit2.Response<GenericModelStatusMessage> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.product_delete_sucessfully), R.color.success);
                            EventBus.getDefault().post(new EventBusGetProduct());
                            getActivity().onBackPressed();
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GenericModelStatusMessage> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null && getActivity() != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    if (getActivity() != null)
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    private void reqDetails(String productId) {
        mBind.mPb.setVisibility(View.VISIBLE);
        mCallDetails = Api.WEB_SERVICE.pro_detail("55555", productId);
        mCallDetails.enqueue(new Callback<GetProductDetailMainObject>() {
            @Override
            public void onResponse(Call<GetProductDetailMainObject> call, retrofit2.Response<GetProductDetailMainObject> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "1":
                            getProductDetailMainObject = response.body();
                            if (getProductDetailMainObject != null) {
                                GetProductDetailData getProductDetailData = getProductDetailMainObject.getData();
                                if (getProductDetailData != null) {
                                    mBind.mEtProName.setText(getProductDetailData.getPro_title());
                                    mBind.mEtProPrice.setText(getProductDetailData.getPro_price());
                                    mBind.mEtProDisc.setText(getProductDetailData.getPro_description());
                                    mBind.mTvCategoryOne.setText(getProductDetailData.getCategory_name());
                                    mBind.mTvBlackWhite.setText(getProductDetailData.getOptionname());
                                    mBind.mScSetAsHidden.setChecked(!getProductDetailData.getPro_status().equals("1"));
                                    categoryId = getProductDetailData.getPro_cat_id();
                                    options = getProductDetailData.getOptionname();
                                    if (!options.isEmpty()) {
                                        optionsList = new ArrayList<String>(Arrays.asList(options.split(",")));
                                    }
                                    int featureImageIndex = -1;
                                    if (getProductDetailData.getImagesList().size() > 0) {
                                        for (int i = 0; i < getProductDetailData.getImagesList().size(); i++) {
                                            if (getProductDetailData.getImagesList().get(i).getIs_featured().equals("1")) {
                                                featureImageIndex = i;
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(i).getProimg_path())
                                                        .into(mBind.mImProduct);
                                                break;
                                            }
                                        }
                                        if (getProductDetailData.getImagesList().size() == 2) {
                                            if (featureImageIndex == 0) {
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(1).getProimg_path())
                                                        .into(mBind.mImProduct1);
                                            } else {
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(0).getProimg_path())
                                                        .into(mBind.mImProduct1);
                                            }
                                        }
                                        if (getProductDetailData.getImagesList().size() >= 3) {
                                            if (featureImageIndex == 0) {
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(1).getProimg_path())
                                                        .into(mBind.mImProduct1);
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(2).getProimg_path())
                                                        .into(mBind.mImProduct2);
                                            } else if (featureImageIndex == 1) {
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(0).getProimg_path())
                                                        .into(mBind.mImProduct1);
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(2).getProimg_path())
                                                        .into(mBind.mImProduct2);
                                            } else {
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(0).getProimg_path())
                                                        .into(mBind.mImProduct1);
                                                Glide.with(getActivity())
                                                        .load(((MainActivity) getActivity()).mSiteThumbnailBaseUrl + "/shop/" + getProductDetailData.getImagesList().get(1).getProimg_path())
                                                        .into(mBind.mImProduct2);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<GetProductDetailMainObject> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null && getActivity() != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    if (getActivity() != null)
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }


}