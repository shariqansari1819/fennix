package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fennix.android.prod.R;
import com.fennix.android.prod.model.ProductModel;
import com.fennix.android.prod.model.addoptions.AddOptionModel;
import com.fennix.android.prod.model.eventbus.EventBusAddOptions;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class AddOptionsAdapter extends RecyclerView.Adapter<AddOptionsAdapter.PremiumTemplateHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<AddOptionModel> addOptionsList;

    public AddOptionsAdapter(Context context, List<AddOptionModel> userTemplatesListData) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.addOptionsList = userTemplatesListData;
    }

    @NonNull
    @Override
    public PremiumTemplateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_add_options, parent, false);
        return new PremiumTemplateHolder(view, new PremiumTemplateHolder.OnTemplateClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusAddOptions(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull PremiumTemplateHolder holder, int position) {
        AddOptionModel data = addOptionsList.get(position);
        holder.mTvAddOptons.setText(data.getmTvAddOptions());
        if (data.getSelected() == false) {
            holder.mTvTick.setVisibility(View.GONE);
        } else {
            holder.mTvTick.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return addOptionsList.size();
    }

    static class PremiumTemplateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTvAddOptons;
        private ImageView mTvTick;
        private OnTemplateClick onTemplateClickListener;

        public PremiumTemplateHolder(View itemView, OnTemplateClick onTemplateClickListener) {
            super(itemView);
            this.onTemplateClickListener = onTemplateClickListener;
            mTvAddOptons = itemView.findViewById(R.id.mTvBlack);
            mTvTick = itemView.findViewById(R.id.mImgTick);
            mTvAddOptons.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onTemplateClickListener.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnTemplateClick {
            void rowClick(int position, int id);
        }

    }
}