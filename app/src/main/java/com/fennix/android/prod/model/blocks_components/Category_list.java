
package com.fennix.android.prod.model.blocks_components;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category_list {

    @SerializedName("blocks_id")
    @Expose
    private String blocks_id;
    @SerializedName("blocks_url")
    @Expose
    private String blocks_url;
    @SerializedName("blocks_height")
    @Expose
    private String blocks_height;
    @SerializedName("blocks_thumb")
    @Expose
    private String blocks_thumb;

    public String getBlocks_id() {
        return blocks_id;
    }

    public void setBlocks_id(String blocks_id) {
        this.blocks_id = blocks_id;
    }

    public String getBlocks_url() {
        return blocks_url;
    }

    public void setBlocks_url(String blocks_url) {
        this.blocks_url = blocks_url;
    }

    public String getBlocks_height() {
        return blocks_height;
    }

    public void setBlocks_height(String blocks_height) {
        this.blocks_height = blocks_height;
    }

    public String getBlocks_thumb() {
        return blocks_thumb;
    }

    public void setBlocks_thumb(String blocks_thumb) {
        this.blocks_thumb = blocks_thumb;
    }

}
