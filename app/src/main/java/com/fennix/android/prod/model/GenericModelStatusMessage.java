package com.fennix.android.prod.model;

/**
 * Created by Kashif on 9/4/2018.
 */

public class GenericModelStatusMessage {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
