package com.fennix.android.prod.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fennix.android.prod.frag.FragCoupons;
import com.fennix.android.prod.frag.FragOrders;
import com.fennix.android.prod.frag.FragProducts;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragOrders tab1 = new FragOrders();
                return tab1;
            case 1:
                FragProducts tab2 = new FragProducts();
                return tab2;
            case 2:
                FragCoupons tab3 = new FragCoupons();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}