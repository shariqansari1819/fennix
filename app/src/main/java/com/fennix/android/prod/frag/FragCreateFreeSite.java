package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragCreateFreeSiteBinding;
import com.fennix.android.prod.model.CreateSiteModel;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragCreateFreeSite extends Fragment implements View.OnClickListener {

    private FragCreateFreeSiteBinding mBind;
    private Call<CreateSiteModel> templatesCall;

    public FragCreateFreeSite() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_create_free_site, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mBtnBuildSite.setOnClickListener(this);
        mBind.mEtBusinessName.requestFocus();
        ((MainActivity) getActivity()).fnShowKeyboardFrom(mBind.mEtBusinessName);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnBuildSite:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (!mBind.mEtBusinessName.getText().toString().trim().isEmpty()) {
                    fnCreateSite(mBind.mEtBusinessName.getText().toString());
                }
                break;
        }
    }

    private void fnCreateSite(final String siteName) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            templatesCall = Api.WEB_SERVICE.createSite("55555", GlobalClass.getInstance().mPref.getString("id", ""), siteName);
            templatesCall.enqueue(new Callback<CreateSiteModel>() {
                @Override
                public void onResponse(Call<CreateSiteModel> call, Response<CreateSiteModel> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    switch (response.body().getStatus()) {
                        case "1":
                            ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.mEtBusinessName);
                            GlobalClass.getInstance().mPrefEditor.putBoolean("first_time", true);
                            GlobalClass.getInstance().mPrefEditor.putString("site_id", response.body().getSite_id());
                            GlobalClass.getInstance().mPrefEditor.putString("site_name", siteName);
                            GlobalClass.getInstance().mPrefEditor.putString("sub_domain", response.body().getSub_domain());
                            GlobalClass.getInstance().mPrefEditor.putString("custom_domain", response.body().getCustom_domain());
                            GlobalClass.getInstance().mPrefEditor.apply();
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.site_created_success), R.color.success);
                            ((MainActivity) getActivity()).mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Bundle bundle = new Bundle();
                            bundle.putString("mFlag", "create_site");
                            ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", false, bundle);
                            break;
                        case "0":
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.site_created_error), R.color.success);
                            break;
                    }
                }

                @Override
                public void onFailure(Call<CreateSiteModel> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}