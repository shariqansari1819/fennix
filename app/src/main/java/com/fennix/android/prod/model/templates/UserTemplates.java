
package com.fennix.android.prod.model.templates;

import com.fennix.android.prod.model.templates.Data;

import java.util.List;

public class UserTemplates {

    private String status;
    private List<Data> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
