package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fennix.android.prod.R;
import com.fennix.android.prod.model.chat.ChatDetailData;

import java.util.ArrayList;
import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ChatDetailData> chatDetailDataArrayList = new ArrayList<>();
    private Context context;
    private LayoutInflater layoutInflater;

    public ChatAdapter(Context context, List<ChatDetailData> chatDetailDataArrayList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.chatDetailDataArrayList = chatDetailDataArrayList;
    }


    @Override
    public int getItemCount() {
        return chatDetailDataArrayList.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view = null;
        if (viewType == 0) {
            view = layoutInflater.inflate(R.layout.cus_chat_sender, parent, false);
            viewHolder = new ChatSenderHolder(view, new ChatSenderHolder.OnMessageClick() {
                @Override
                public void rowClick(int position, int id) {

                }
            });
        } else {
            view = layoutInflater.inflate(R.layout.cus_chat_receiver, parent, false);
            viewHolder = new ChatReceiverHolder(view, new ChatReceiverHolder.OnMessageClick() {
                @Override
                public void rowClick(int position, int id) {

                }
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChatDetailData chatDetailData = chatDetailDataArrayList.get(position);
        String message = chatDetailData.getMessage();
        String messageTime = chatDetailData.getMessage_time();
        if (holder.getItemViewType() == 0) {
            ChatSenderHolder chatSenderHolder = (ChatSenderHolder) holder;
            chatSenderHolder.textViewMessage.setText(message);
            chatSenderHolder.textViewTime.setText(messageTime);
        } else {
            ChatReceiverHolder chatReceiverHolder = (ChatReceiverHolder) holder;
            chatReceiverHolder.textViewMessage.setText(message);
            chatReceiverHolder.textViewTime.setText(messageTime);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (chatDetailDataArrayList.get(position).getSender_type().equals("User"))
            return 1;
        else
            return 0;
    }

    static class ChatSenderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnMessageClick onMessageClick;
        TextView textViewMessage, textViewTime;

        ChatSenderHolder(View itemView, OnMessageClick onMessageClick) {
            super(itemView);
            this.onMessageClick = onMessageClick;
            textViewMessage = itemView.findViewById(R.id.mTvMessage);
            textViewTime = itemView.findViewById(R.id.mTvTime);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onMessageClick.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnMessageClick {
            void rowClick(int position, int id);
        }
    }

    static class ChatReceiverHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnMessageClick onMessageClick;
        TextView textViewMessage, textViewTime;

        ChatReceiverHolder(View itemView, OnMessageClick onMessageClick) {
            super(itemView);
            this.onMessageClick = onMessageClick;
            textViewMessage = itemView.findViewById(R.id.mTvMessage);
            textViewTime = itemView.findViewById(R.id.mTvTime);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onMessageClick.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnMessageClick {
            void rowClick(int position, int id);
        }
    }
}
