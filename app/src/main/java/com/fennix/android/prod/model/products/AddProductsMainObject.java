
package com.fennix.android.prod.model.products;


public class AddProductsMainObject {

    private String status;
    private AddProductData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public AddProductData getData() {
        return data;
    }

    public void setData(AddProductData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
