
package com.fennix.android.prod.model.edittemplate;

import java.util.HashMap;
import java.util.Map;

public class EditTemplateData {

    private String pages_id;
    private String sites_id;
    private String frames_content;
    private String frames_height;
    private String frames_original_url;

    public String getPages_id() {
        return pages_id;
    }

    public void setPages_id(String pages_id) {
        this.pages_id = pages_id;
    }

    public String getSites_id() {
        return sites_id;
    }

    public void setSites_id(String sites_id) {
        this.sites_id = sites_id;
    }

    public String getFrames_content() {
        return frames_content;
    }

    public void setFrames_content(String frames_content) {
        this.frames_content = frames_content;
    }

    public String getFrames_height() {
        return frames_height;
    }

    public void setFrames_height(String frames_height) {
        this.frames_height = frames_height;
    }

    public String getFrames_original_url() {
        return frames_original_url;
    }

    public void setFrames_original_url(String frames_original_url) {
        this.frames_original_url = frames_original_url;
    }

}
