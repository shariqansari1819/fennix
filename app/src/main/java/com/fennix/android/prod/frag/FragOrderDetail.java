package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragOrderDetailBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.orderdetail.OrderDetailData;
import com.fennix.android.prod.model.orderdetail.OrderDetailMainObject;
import com.fennix.android.prod.utils.GlobalClass;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragOrderDetail extends Fragment implements View.OnClickListener {

    private FragOrderDetailBinding mBind;

    private Call<GenericModelStatusMessage> mCallUpdateOrderStatus;
    private Call<OrderDetailMainObject> mCallOrderDetail;

    private String orderId;

    public FragOrderDetail() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_order_detail, container, false);

        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getArguments() != null) {
            orderId = getArguments().getString("order_id", "");
            if (!orderId.isEmpty()) {
                getOrderDetail(orderId);
            }
        }

        mBind.mImgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCallUpdateOrderStatus != null && mCallUpdateOrderStatus.isExecuted()) {
            mCallUpdateOrderStatus.cancel();
        }
        if (mCallOrderDetail != null && mCallOrderDetail.isExecuted()) {
            mCallOrderDetail.cancel();
        }
    }

    private void getOrderDetail(final String orderId) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallOrderDetail = Api.WEB_SERVICE.getOrderDetail("55555", orderId);
            mCallOrderDetail.enqueue(new Callback<OrderDetailMainObject>() {
                @Override
                public void onResponse(@NonNull Call<OrderDetailMainObject> call, @NonNull Response<OrderDetailMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                OrderDetailData orderDetailData = response.body().getData();
                                if (orderDetailData != null) {
                                    mBind.mTvOrdersNo.setText("#" + orderDetailData.getProductdetail().getOrder_id());
                                    mBind.mTvOrders.setText(orderDetailData.getProductdetail().getOrder_name());
                                    if (orderDetailData.getProductdetail().getOrder_status().equals("0")) {
                                        mBind.mTvStatus.setText("Pending");
                                        mBind.mTvStatus.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                                    } else if (orderDetailData.getProductdetail().getOrder_status().equals("1")) {
                                        mBind.mTvStatus.setText("Processing");
                                        mBind.mTvStatus.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                                    } else if (orderDetailData.getProductdetail().getOrder_status().equals("2")) {
                                        mBind.mTvStatus.setText("Completed");
                                        mBind.mTvStatus.setBackgroundColor(getResources().getColor(R.color.colorLightBlue));
                                    } else if (orderDetailData.getProductdetail().getOrder_status().equals("3")) {
                                        mBind.mTvStatus.setText("Canceled");
                                        mBind.mTvStatus.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                                    } else {
                                        mBind.mTvStatus.setText("Refused");
                                        mBind.mTvStatus.setBackgroundColor(getResources().getColor(R.color.colorLightGreen));
                                    }
                                    mBind.mTvOrdersPrice.setText("$" + orderDetailData.getProductdetail().getOrderprice());
                                    if (orderDetailData.getProductdetail().getNote().isEmpty()) {
                                        mBind.mTvNotes.setText("No notes provided.");
                                    } else {
                                        mBind.mTvNotes.setText(orderDetailData.getProductdetail().getNote());
                                    }
                                    mBind.mTvCustomerName.setText(orderDetailData.getProductdetail().getOrder_name());
                                    mBind.mTvEmailAddress.setText(orderDetailData.getProductdetail().getOrder_email());
                                    mBind.mTvShippingAddress.setText(orderDetailData.getProductdetail().getOrder_address());
                                    mBind.mTvPhoneNumber.setText(orderDetailData.getProductdetail().getOrder_phone());
                                }
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OrderDetailMainObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void getOrderStatusUpdate(String orderId, String orderStatus, final int position) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallUpdateOrderStatus = Api.WEB_SERVICE.update_order("55555", orderId, orderStatus);
            mCallUpdateOrderStatus.enqueue(new Callback<GenericModelStatusMessage>() {
                @Override
                public void onResponse(@NonNull Call<GenericModelStatusMessage> call, @NonNull Response<GenericModelStatusMessage> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.status_changed_successfully), R.color.success);
//                                getOrdersData.get(position).setOrder_status("2");
//                                ordersAdapter.notifyItemChanged(position);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GenericModelStatusMessage> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
