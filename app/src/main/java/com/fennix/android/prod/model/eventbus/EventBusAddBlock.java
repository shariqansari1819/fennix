package com.fennix.android.prod.model.eventbus;


import com.fennix.android.prod.model.blocks_html.BlocksHtmlData;

public class EventBusAddBlock {

    private BlocksHtmlData blocksHtmlData;

    public EventBusAddBlock(BlocksHtmlData blocksHtmlData) {
        this.blocksHtmlData = blocksHtmlData;
    }

    public BlocksHtmlData getBlocksHtmlData() {
        return blocksHtmlData;
    }

    public void setBlocksHtmlData(BlocksHtmlData blocksHtmlData) {
        this.blocksHtmlData = blocksHtmlData;
    }
}
