package com.fennix.android.prod.model.eventbus;

import java.util.List;

public class EventBusSelectedOptions {
    private List<String> selectedOptionsList;

    public EventBusSelectedOptions(List<String> selectedOptionsList) {
        this.selectedOptionsList = selectedOptionsList;
    }

    public List<String> getSelectedOptionsList() {
        return selectedOptionsList;
    }

    public void setSelectedOptionsList(List<String> selectedOptionsList) {
        this.selectedOptionsList = selectedOptionsList;
    }






}
