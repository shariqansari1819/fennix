
package com.fennix.android.prod.model.orderdetail;

import java.util.List;

public class OrderDetailData {

    private ProductDetail productdetail;
    private List<Order> orders = null;

    public ProductDetail getProductdetail() {
        return productdetail;
    }

    public void setProductdetail(ProductDetail productdetail) {
        this.productdetail = productdetail;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

}
