package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.adapter.ChatAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragChatBinding;
import com.fennix.android.prod.model.chat.ChatDetailData;
import com.fennix.android.prod.model.chat.ChatDetailObject;
import com.fennix.android.prod.model.eventbus.EventBusUpdateConversation;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */

public class FragChatDetail extends Fragment implements View.OnClickListener, TextView.OnEditorActionListener {

    private FragChatBinding mBind;
    private List<ChatDetailData> chatDetailDataArrayList = new ArrayList<>();
    private ChatAdapter chatAdapter;
    private Call<ChatDetailObject> chatDetailCall;
    private String senderName, senderEmail, lastActiveSessionId, ipAddress;

    public FragChatDetail() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_chat, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        senderName = getArguments().getString("sender_name", "");
        senderEmail = getArguments().getString("sender_email", "");
        lastActiveSessionId = getArguments().getString("last_active_session_id", "");
        ipAddress = getArguments().getString("ip_address", "");
        mBind.mTvTitle.setText(senderName);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setStackFromEnd(true);
        mBind.mRvChat.setLayoutManager(layoutManager);
        getUserChatDetail();

        if (GlobalClass.getInstance().mPref.getString("chat_status", "0").equals("0")) {
            mBind.mEtMessage.setVisibility(View.GONE);
            mBind.mImgSend.setVisibility(View.GONE);
            mBind.viewBorderBottom.setVisibility(View.GONE);
        }

        mBind.mImgBack.setOnClickListener(this);
        mBind.mImgSend.setOnClickListener(this);
        mBind.mEtMessage.setOnClickListener(this);
        mBind.mEtMessage.setOnEditorActionListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatDetailCall != null && chatDetailCall.isExecuted()) {
            chatDetailCall.cancel();
        }
        if (chatDetailDataArrayList.size() > 0) {
            chatDetailDataArrayList.clear();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (chatDetailDataArrayList.size() > 0 && mBind.mRvChat.getVerticalScrollbarPosition() < chatDetailDataArrayList.size() - 1) {
            mBind.mRvChat.scrollToPosition(chatDetailDataArrayList.size() - 1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgBack:
                ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.mEtMessage);
                getActivity().onBackPressed();
                EventBus.getDefault().post(new EventBusUpdateConversation());
                break;
            case R.id.mImgSend:
                String message = mBind.mEtMessage.getText().toString();
                if (!TextUtils.isEmpty(message)) {
                    reqSendMessage(message, GlobalClass.getInstance().mPref.getString("id", ""), senderEmail, lastActiveSessionId, ipAddress, "admin");
                    mBind.mEtMessage.setText("");
                }
                break;
            case R.id.mEtMessage:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (chatDetailDataArrayList.size() > 0)
                            mBind.mRvChat.scrollToPosition(chatDetailDataArrayList.size() - 1);
                    }
                }, 300);
                break;
        }

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (i == EditorInfo.IME_ACTION_SEND) {
            String message = mBind.mEtMessage.getText().toString();
            if (!TextUtils.isEmpty(message)) {
                reqSendMessage(message, GlobalClass.getInstance().mPref.getString("id", ""), senderEmail, lastActiveSessionId, ipAddress, "admin");
                mBind.mEtMessage.setText("");
            }
            return true;
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateChatEvent(ChatDetailData chatDetailData) {
        if (chatDetailData.getSent_by().equals(senderEmail)) {
            chatDetailDataArrayList.add(chatDetailData);
            chatAdapter.notifyItemInserted(chatDetailDataArrayList.size() - 1);
        }
    }

    private void getUserChatDetail() {
        try {
            if (!((MainActivity) getActivity()).fnIsisOnline()) {
                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                return;
            }
            mBind.mPb.setVisibility(View.VISIBLE);
            chatDetailCall = Api.WEB_SERVICE.getUserChat("55555", GlobalClass.getInstance().mPref.getString("id", ""), senderEmail);
            chatDetailCall.enqueue(new Callback<ChatDetailObject>() {
                @Override
                public void onResponse(@NonNull Call<ChatDetailObject> call, @NonNull Response<ChatDetailObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "success":
                                chatDetailDataArrayList = response.body().getData();
                                chatAdapter = new ChatAdapter(getActivity(), chatDetailDataArrayList);
                                mBind.mRvChat.setAdapter(chatAdapter);
                                mBind.mRvChat.scrollToPosition(chatDetailDataArrayList.size() - 1);
                                chatAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                                    @Override
                                    public void onItemRangeInserted(int positionStart, int itemCount) {
                                        super.onItemRangeInserted(positionStart, itemCount);
                                        mBind.mRvChat.scrollToPosition(chatDetailDataArrayList.size() - 1);
                                    }
                                });
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChatDetailObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void reqSendMessage(String message, String sentBy, String sentTo, String sessionId, String ipAddress, String senderName) {
        try {
            if (!((MainActivity) getActivity()).fnIsisOnline()) {
                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                return;
            }
            if (mBind.mPb.getVisibility() == View.VISIBLE) {
                return;
            }
            chatDetailCall = Api.WEB_SERVICE.send_message("55555", message, sentBy, sentTo, sessionId, ipAddress, senderName);
            chatDetailCall.enqueue(new Callback<ChatDetailObject>() {
                @Override
                public void onResponse(@NonNull Call<ChatDetailObject> call, @NonNull Response<ChatDetailObject> response) {
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "success":
                                chatDetailDataArrayList.add(response.body().getData().get(0));
                                chatAdapter.notifyItemInserted(chatDetailDataArrayList.size() - 1);
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChatDetailObject> call, @NonNull Throwable error) {
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

}