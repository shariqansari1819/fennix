package com.fennix.android.prod.model.blocks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BlocksComponentsModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("blocks")
    private List<BlocksData> blocksData = null;
    @SerializedName("data")
    private List<ComponentsData> componentsData = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BlocksData> getBlocksData() {
        return blocksData;
    }

    public void setBlocksData(List<BlocksData> blocksData) {
        this.blocksData = blocksData;
    }

    public List<ComponentsData> getComponentsData() {
        return componentsData;
    }

    public void setComponentsData(List<ComponentsData> componentsData) {
        this.componentsData = componentsData;
    }
}
