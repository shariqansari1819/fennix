package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.PremiumTemplateAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragPremiumHomeBinding;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;
import com.fennix.android.prod.model.usersites.Data;
import com.fennix.android.prod.model.usersites.UserSitesModel;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FragPremiumHome extends Fragment implements View.OnClickListener {

    private FragPremiumHomeBinding mBind;
    private Call<UserSitesModel> mUserSitesCall;
    private List<Data> userSitesList = new ArrayList<>();

    public FragPremiumHome() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_premium_home, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).mBind.mBottombar.setVisibility(View.VISIBLE);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        if (mUserSitesCall != null && mUserSitesCall.isExecuted()) {
            mUserSitesCall.cancel();
        }
    }

    private void init() {
        mBind.mFbAddPremiumTemplate.setOnClickListener(this);
        if (getActivity() != null)
            funGetUserSites();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPremiumTemplateEvent(EventBusTemplateId event) {
        if (userSitesList.get(event.getmPos()).getPages_preview().equals("")) {
            ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", true, null);
        } else {
            ((MainActivity) getActivity()).mFScreenName = "EDIT TEMPLATE";
            ((MainActivity) getActivity()).fnLoadFragAdd("EDIT TEMPLATE", true, null);
        }
    }

    private void funGetUserSites() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mUserSitesCall = Api.WEB_SERVICE.getUserSites("55555", GlobalClass.getInstance().mPref.getString("id", ""));
        mUserSitesCall.enqueue(new Callback<UserSitesModel>() {
            @Override
            public void onResponse(Call<UserSitesModel> call, retrofit2.Response<UserSitesModel> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "0":
                            break;
                        case "1":
                            userSitesList = response.body().getData();
                            mBind.mRvPremiumTemplates.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                            mBind.mRvPremiumTemplates.setAdapter(new PremiumTemplateAdapter(getActivity(), userSitesList));
                            if (response.body().getData().size() >= Integer.parseInt(GlobalClass.getInstance().mPref.getString("site_limit", ""))) {
                                mBind.mFbAddPremiumTemplate.setVisibility(View.GONE);
                            } else if (response.body().getData().size() < Integer.parseInt(GlobalClass.getInstance().mPref.getString("site_limit", ""))) {
                                mBind.mFbAddPremiumTemplate.setVisibility(View.VISIBLE);
                            }
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<UserSitesModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mFbAddPremiumTemplate:
                ((MainActivity) getActivity()).fnLoadFragAdd("CREATE SITE", true, null);
                break;
        }
    }

}