
package com.fennix.android.prod.model.products.productdetails;


public class ImagesList {

    private String proimg_path;
    private String is_featured;

    public String getProimg_path() {
        return proimg_path;
    }

    public void setProimg_path(String proimg_path) {
        this.proimg_path = proimg_path;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

}
