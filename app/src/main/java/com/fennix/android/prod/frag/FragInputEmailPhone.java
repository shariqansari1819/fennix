package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragInputEmailPhoneBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.usersites.UserSitesModel;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragInputEmailPhone extends Fragment implements View.OnClickListener {

    private FragInputEmailPhoneBinding mBind;
    private Call<GenericModelStatusMessage> mRetrofitCall;
    private Call<UserSitesModel> mUserSitesCall;

    public FragInputEmailPhone() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_input_email_phone, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (getArguments().getString("mFlag").contentEquals("email")) {
            mBind.mTiEmail.setVisibility(View.VISIBLE);
            mBind.mBtnOk.setText(getResources().getString(R.string.send_me_an_e_mail));
        } else {
            mBind.mTiPhone.setVisibility(View.VISIBLE);
            mBind.mBtnOk.setText(getResources().getString(R.string.call_me));
        }
        mBind.mBtnOk.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRetrofitCall != null && mRetrofitCall.isExecuted()) {
            mRetrofitCall.cancel();
        }
        if (mUserSitesCall != null && mUserSitesCall.isExecuted()) {
            mUserSitesCall.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        if (mBind.mPb.getVisibility() == View.VISIBLE) {
            return;
        }
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (getArguments().getString("mFlag").contentEquals("email")) {
            if (mBind.mEtEmail.getText().toString().trim().isEmpty() || mBind.mEtEmail.getText().toString().length() == 0) {
                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.email_is_empty), R.color.error);
            } else if (!funValidateEmail(mBind.mEtEmail.getText().toString())) {
                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.invalid_email), R.color.error);
            } else {
                ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.mEtEmail);
                reqMail();
            }
        } else {
            if (mBind.mEtPhone.getText().toString().trim().isEmpty() || mBind.mEtPhone.getText().toString().length() == 0) {
                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.empty_phone), R.color.error);
            } else {
                ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.mEtPhone);
                reqMail();
            }
        }

    }

    private boolean funValidateEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern)) {
            return true;
        } else {
            return false;
        }
    }

    private void reqMail() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mRetrofitCall = Api.WEB_SERVICE.email("55555", GlobalClass.getInstance().mPref.getString("id", ""), mBind.mEtEmail.getText().toString().trim(), mBind.mEtPhone.getText().toString().trim());
            mRetrofitCall.enqueue(new Callback<GenericModelStatusMessage>() {
                @Override
                public void onResponse(@NonNull Call<GenericModelStatusMessage> call, @NonNull Response<GenericModelStatusMessage> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                funGetUserSites();
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GenericModelStatusMessage> call, @NonNull Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void funGetUserSites() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mUserSitesCall = Api.WEB_SERVICE.getUserSites("55555", GlobalClass.getInstance().mPref.getString("id", ""));
        mUserSitesCall.enqueue(new Callback<UserSitesModel>() {
            @Override
            public void onResponse(Call<UserSitesModel> call, retrofit2.Response<UserSitesModel> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("mFlag", "email");
                    switch (response.body().getStatus()) {
                        case "0":
//                            ((MainActivity) getActivity()).fnLoadFragAdd("CREATE SITE", true, null);
                            bundle.putBoolean("site_exist", false);
                            break;
                        case "1":
                            bundle.putBoolean("site_exist", true);
                            break;
                    }
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getResources().getString(R.string.mail_sent), R.color.success);
                    ((MainActivity) getActivity()).fnLoadFragAdd("SUCCESS EMAIL PHONE", false, bundle);
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<UserSitesModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}