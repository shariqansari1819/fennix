
package com.fennix.android.prod.model.blocks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComponentList {

    @SerializedName("components_id")
    @Expose
    private String componentsId;
    @SerializedName("components_thumb")
    @Expose
    private String componentsThumb;
    @SerializedName("components_height")
    @Expose
    private String componentsHeight;
    @SerializedName("components_markup")
    @Expose
    private String componentsMarkup;

    public String getComponentsId() {
        return componentsId;
    }

    public void setComponentsId(String componentsId) {
        this.componentsId = componentsId;
    }

    public String getComponentsThumb() {
        return componentsThumb;
    }

    public void setComponentsThumb(String componentsThumb) {
        this.componentsThumb = componentsThumb;
    }

    public String getComponentsHeight() {
        return componentsHeight;
    }

    public void setComponentsHeight(String componentsHeight) {
        this.componentsHeight = componentsHeight;
    }

    public String getComponentsMarkup() {
        return componentsMarkup;
    }

    public void setComponentsMarkup(String componentsMarkup) {
        this.componentsMarkup = componentsMarkup;
    }
}
