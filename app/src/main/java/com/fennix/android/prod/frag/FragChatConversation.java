package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.ConversationAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragConversationBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.conversation.ChatConversationData;
import com.fennix.android.prod.model.conversation.ChatConversationObject;
import com.fennix.android.prod.model.eventbus.EventBusConversation;
import com.fennix.android.prod.model.eventbus.EventBusUpdateConversation;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragChatConversation extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private FragConversationBinding mBind;
    private Call<ChatConversationObject> conversationCall;
    private Call<GenericModelStatusMessage> mCallChatOnOff;
    private List<ChatConversationData> conversationDataList = new ArrayList<>();

    public FragChatConversation() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_conversation, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mScNotification.setChecked(GlobalClass.getInstance().mPref.getString("chat_status", "0").equals("1"));
        mBind.mImgChatSetting.setVisibility(GlobalClass.getInstance().mPref.getString("chat_status", "0").equals("0") ? View.GONE : View.VISIBLE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mBind.mPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        getUserChatConversation();
        mBind.mImgChatSetting.setOnClickListener(this);
        mBind.mScNotification.setOnCheckedChangeListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (conversationCall != null && conversationCall.isExecuted()) {
            conversationCall.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mImgChatSetting:
                ((MainActivity) getActivity()).fnLoadFragAdd("CHAT SETTING", true, null);
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (mBind.mPb.getVisibility() == View.VISIBLE) {
            return;
        }
        if (b) {
            reqChatOnOffStatus("1");
        } else {
            reqChatOnOffStatus("0");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConversationClick(EventBusConversation eventBusConversation) {
        Bundle bundle = new Bundle();
        bundle.putString("sender_email", conversationDataList.get(eventBusConversation.getPosition()).getUser_email());
        bundle.putString("sender_name", conversationDataList.get(eventBusConversation.getPosition()).getSender_name());
        bundle.putString("last_active_session_id", conversationDataList.get(eventBusConversation.getPosition()).getLast_active_session_id());
        bundle.putString("ip_address", conversationDataList.get(eventBusConversation.getPosition()).getVisitor_ip_address());
        ((MainActivity) getActivity()).fnLoadFragAdd("CHAT", true, bundle);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConversationRefresh(EventBusUpdateConversation eventBusUpdateConversation) {
        getUserChatConversation();
    }

    private void getUserChatConversation() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            conversationCall = Api.WEB_SERVICE.getChatConversation("55555", GlobalClass.getInstance().mPref.getString("id", ""));
            conversationCall.enqueue(new Callback<ChatConversationObject>() {
                @Override
                public void onResponse(@NonNull Call<ChatConversationObject> call, @NonNull Response<ChatConversationObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "success":
                                conversationDataList = response.body().getData();
                                Collections.sort(conversationDataList, new Comparator<ChatConversationData>() {
                                    @Override
                                    public int compare(ChatConversationData chatConversationData, ChatConversationData t1) {
                                        return t1.getLast_message_time().compareTo(chatConversationData.getLast_message_time());
                                    }
                                });
                                mBind.mRvConversation.setLayoutManager(new LinearLayoutManager(getActivity()));
                                mBind.mRvConversation.setAdapter(new ConversationAdapter(getActivity(), conversationDataList));
                                if (conversationDataList.size() == 0) {
                                    mBind.mTvNoChat.setVisibility(View.VISIBLE);
                                } else {
                                    mBind.mTvNoChat.setVisibility(View.GONE);
                                }
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChatConversationObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    mBind.mScNotification.setChecked(GlobalClass.getInstance().mPref.getString("chat_status", "0").equals("1"));
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void reqChatOnOffStatus(final String flag) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallChatOnOff = Api.WEB_SERVICE.chat_on_off("55555", GlobalClass.getInstance().mPref.getString("id", ""), flag);
            mCallChatOnOff.enqueue(new Callback<GenericModelStatusMessage>() {
                @Override
                public void onResponse(@NonNull Call<GenericModelStatusMessage> call, @NonNull Response<GenericModelStatusMessage> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Chat status changed successfully.", R.color.success);
                                GlobalClass.getInstance().mPrefEditor.putString("chat_status", flag).apply();
                                mBind.mImgChatSetting.setVisibility(GlobalClass.getInstance().mPref.getString("chat_status", "0").equals("0") ? View.GONE : View.VISIBLE);
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GenericModelStatusMessage> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);

                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}