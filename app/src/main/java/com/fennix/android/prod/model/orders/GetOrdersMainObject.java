
package com.fennix.android.prod.model.orders;

import java.util.List;

public class GetOrdersMainObject {

    private String status;
    private List<GetOrdersData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GetOrdersData> getData() {
        return data;
    }

    public void setData(List<GetOrdersData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
