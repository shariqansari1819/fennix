package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.PagerAdapter;
import com.fennix.android.prod.databinding.FragManageStoreBinding;


public class FragManageStore extends Fragment {

    private FragManageStoreBinding mBind;

    private PagerAdapter adapter;

    public FragManageStore() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_manage_store, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mTabLayout.addTab(mBind.mTabLayout.newTab().setText(getResources().getString(R.string.orders)));
        mBind.mTabLayout.addTab(mBind.mTabLayout.newTab().setText(getResources().getString(R.string.products)));
        mBind.mTabLayout.addTab(mBind.mTabLayout.newTab().setText(getResources().getString(R.string.coupons)));
        mBind.mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new PagerAdapter(getChildFragmentManager(), mBind.mTabLayout.getTabCount());
        mBind.mPager.setAdapter(adapter);
        mBind.mPager.setOffscreenPageLimit(2);
        mBind.mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mBind.mTabLayout));
        mBind.mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBind.mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
