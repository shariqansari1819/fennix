package com.fennix.android.prod.frag;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fennix.android.prod.databinding.FragTermsBinding;
import com.fennix.android.prod.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragTerms extends Fragment {

    private FragTermsBinding mBind;

    public FragTerms() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_terms, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void init() {
        mBind.mWebView.getSettings().setJavaScriptEnabled(true);
        mBind.mWebView.getSettings().setLoadWithOverviewMode(true);
        mBind.mWebView.getSettings().setUseWideViewPort(true);
        mBind.mWebView.getSettings().setBuiltInZoomControls(true);
        mBind.mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mBind.mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (!mBind.mPb.isShown()) {
                    mBind.mPb.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (mBind.mPb.isShown()) {
                    mBind.mPb.setVisibility(View.GONE);
                }
            }
        });
        if (getArguments().getString("mFlag", "").contentEquals("help")) {
            mBind.mWebView.loadUrl("https://help.creamisitioweb.com/");
        } else {
            mBind.mWebView.loadUrl("https://terms.creamisitioweb.com/");
        }
    }

}