package com.fennix.android.prod.utils;


import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Locale;


public class GlobalClass extends Application {
    public boolean mFlanguage = true ;
    public FrameLayout.LayoutParams mParams;
    public Snackbar mSnakeBar;
    private static GlobalClass singleton;
    public SharedPreferences mPref;
    public SharedPreferences.Editor mPrefEditor;
    public boolean mFappOpen;
//    private MyDatabase database;

    public static GlobalClass getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        mPrefEditor = mPref.edit();
//        ViewPump.init(ViewPump.builder()
//                .addInterceptor(new CalligraphyInterceptor(
//                        new CalligraphyConfig.Builder()
//                                .setDefaultFontPath("fonts/RobotoCondensed-Light.ttf")
//                                .setFontAttrId(R.attr.fontPath)
//                                .build()))
//                .build());
    }

//    public void hideKeyboardFrom(Context context, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
//
//    public void showKeyboardFrom(Context context, View view) {
//        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(view, 0);
//    }
//

//    public boolean checkEditText(View mRoot, EditText mEt, String mMsg) {
//        if (mEt.getText().toString().trim().length() == 0) {
//            mEt.getBackground().mutate().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.error), PorterDuff.Mode.SRC_ATOP);
//            snakeBar(mRoot, mMsg, R.color.error);
//            return false;
//        }
//        return true;
//    }

    public void snakeBar(View mRoot, String mMsg, int mBgCol) {
        mSnakeBar = Snackbar.make(mRoot, mMsg, Snackbar.LENGTH_LONG);
        mRoot = mSnakeBar.getView();
        mRoot.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), mBgCol));
        TextView mTv = mRoot.findViewById(android.support.design.R.id.snackbar_text);
        mTv.setMaxLines(5);
        mParams = (FrameLayout.LayoutParams) mRoot.getLayoutParams();
        mParams.gravity = Gravity.TOP;
        mRoot.setLayoutParams(mParams);
        mSnakeBar.show();
    }
//    public boolean isOnline(Context context) {
//        boolean haveConnectedWifi = false;
//        boolean haveConnectedMobile = false;
//        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        return haveConnectedWifi || haveConnectedMobile;
//    }

//    public void openDatabae() {
//        database = Room.databaseBuilder(getApplicationContext(), MyDatabase.class, "MyDatabase")
//                .fallbackToDestructiveMigration()
//                .allowMainThreadQueries()
//                .build();
//    }
//
//
//    public MyDatabase getDB() {
//        return database;
//    }


}
