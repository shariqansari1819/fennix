package com.fennix.android.prod.utils.gallery;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.fennix.android.prod.utils.gallery.EventGalleryModel;
import com.fennix.android.prod.utils.gallery.GalleryModel;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

import com.fennix.android.prod.R;


public class GalleryAdopter extends RecyclerView.Adapter<GalleryAdopter.ViewHolder> {
    private LayoutInflater mInflator;
    public List<GalleryModel> mdata;
    public Context mContext;

    public GalleryAdopter(Context mContext, List<GalleryModel> data) {
        mInflator = LayoutInflater.from(mContext);
        this.mdata = data;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflator.inflate(R.layout.cus_media, parent, false);
        return new ViewHolder(v, new ViewHolder.IMyViewHolderClicks() {
            @Override
            public void rowclick(int pos, int id) {
                EventBus.getDefault().post(new EventGalleryModel(pos));
            }
        });
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final GalleryModel mObjCur = mdata.get(position);
//        holder.name.setText(mObjCur.getmName());
        Glide.with(mContext).load(new File(mObjCur.getmPath()))
                .apply(new RequestOptions().centerCrop())
                .apply(new RequestOptions().override(200, 200))
                .apply(RequestOptions.placeholderOf(R.drawable.placeholder_wait))
                .apply(RequestOptions.errorOf(R.drawable.palceholer_error))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE))
                .into(holder.mImg);
        if (mObjCur.ismSelected()) {
            holder.mImgTic.setVisibility(View.VISIBLE);
        } else {
            holder.mImgTic.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //        TextView name;
        AppCompatImageView mImg, mImgTic;
        public IMyViewHolderClicks mListener;

        public ViewHolder(View itemLayoutView, IMyViewHolderClicks listener) {
            super(itemLayoutView);
            mListener = listener;
//            name = itemLayoutView.findViewById(R.id.mTvName);
            mImg = itemLayoutView.findViewById(R.id.mImg);
            mImgTic = itemLayoutView.findViewById(R.id.mImgCheck);
            itemLayoutView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.rowclick(getAdapterPosition(), v.getId());
        }

        public interface IMyViewHolderClicks {
            void rowclick(int pos, int id);
        }
    }

}
