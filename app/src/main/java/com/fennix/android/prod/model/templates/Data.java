
package com.fennix.android.prod.model.templates;

public class Data {

    private String pages_id;
    private String template_cat_id;
    private String sites_id;
    private String pages_preview;
    private String sitethumb;
    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPages_id() {
        return pages_id;
    }

    public void setPages_id(String pages_id) {
        this.pages_id = pages_id;
    }

    public String getTemplate_cat_id() {
        return template_cat_id;
    }

    public void setTemplate_cat_id(String template_cat_id) {
        this.template_cat_id = template_cat_id;
    }

    public String getSites_id() {
        return sites_id;
    }

    public void setSites_id(String sites_id) {
        this.sites_id = sites_id;
    }

    public String getPages_preview() {
        return pages_preview;
    }

    public void setPages_preview(String pages_preview) {
        this.pages_preview = pages_preview;
    }

    public String getSitethumb() {
        return sitethumb;
    }

    public void setSitethumb(String sitethumb) {
        this.sitethumb = sitethumb;
    }

}
