
package com.fennix.android.prod.model.conversation;

public class ChatConversationData {

    private String last_message;
    private String last_message_time;
    private String visitor_ip_address;
    private String sent_to;
    private String sent_by;
    private String sender_name;
    private String last_active_session_id;
    private String user_email;
    private String unread_messages_count;

    public String getUnread_messages_count() {
        return unread_messages_count;
    }

    public void setUnread_messages_count(String unread_messages_count) {
        this.unread_messages_count = unread_messages_count;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getLast_active_session_id() {
        return last_active_session_id;
    }

    public void setLast_active_session_id(String last_active_session_id) {
        this.last_active_session_id = last_active_session_id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getLast_message_time() {
        return last_message_time;
    }

    public void setLast_message_time(String last_message_time) {
        this.last_message_time = last_message_time;
    }

    public String getVisitor_ip_address() {
        return visitor_ip_address;
    }

    public void setVisitor_ip_address(String visitor_ip_address) {
        this.visitor_ip_address = visitor_ip_address;
    }

    public String getSent_to() {
        return sent_to;
    }

    public void setSent_to(String sent_to) {
        this.sent_to = sent_to;
    }

    public String getSent_by() {
        return sent_by;
    }

    public void setSent_by(String sent_by) {
        this.sent_by = sent_by;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

}
