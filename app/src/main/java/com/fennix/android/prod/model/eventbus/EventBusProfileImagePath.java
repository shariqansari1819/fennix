package com.fennix.android.prod.model.eventbus;

public class EventBusProfileImagePath {
    private String imagePath;

    public EventBusProfileImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
