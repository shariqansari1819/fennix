
package com.fennix.android.prod.model.products.productdetails;


public class GetProductDetailMainObject {

    private String status;
    private GetProductDetailData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GetProductDetailData getData() {
        return data;
    }

    public void setData(GetProductDetailData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
