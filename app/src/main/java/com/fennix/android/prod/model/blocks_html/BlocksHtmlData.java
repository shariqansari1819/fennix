
package com.fennix.android.prod.model.blocks_html;

import java.util.HashMap;
import java.util.Map;

public class BlocksHtmlData {

    private String block_html;
    private String block_original_url;
    private String block_height;

    public String getBlock_html() {
        return block_html;
    }

    public void setBlock_html(String block_html) {
        this.block_html = block_html;
    }

    public String getBlock_original_url() {
        return block_original_url;
    }

    public void setBlock_original_url(String block_original_url) {
        this.block_original_url = block_original_url;
    }

    public String getBlock_height() {
        return block_height;
    }

    public void setBlock_height(String block_height) {
        this.block_height = block_height;
    }

}
