package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.OrdersAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragOrdersBinding;
import com.fennix.android.prod.model.GenericModelStatusMessage;
import com.fennix.android.prod.model.eventbus.EventBusOrderId;
import com.fennix.android.prod.model.orders.GetOrdersData;
import com.fennix.android.prod.model.orders.GetOrdersMainObject;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragOrders extends Fragment {

    private OrdersAdapter ordersAdapter;
    String orderId;

    private Call<GetOrdersMainObject> mCallGetOrders;
    private List<GetOrdersData> getOrdersData = new ArrayList<>();

    private FragOrdersBinding mBind;

    public FragOrders() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_orders, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        EventBus.getDefault().register(this);
        mBind.mrvOrders.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBind.mrvOrders.setItemAnimator(new DefaultItemAnimator());
        getOrders();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusOrderClick(EventBusOrderId eventBusOrderId) {
        orderId = getOrdersData.get(eventBusOrderId.getPosition()).getOrder_id();
//        getOrderStatusUpdate(orderId, "2", eventBusOrderId.getPosition());
        Bundle bundle = new Bundle();
        bundle.putString("order_id", orderId);
        ((MainActivity) getActivity()).fnLoadFragAdd("ORDER DETAIL", true, bundle);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (mCallGetOrders != null && mCallGetOrders.isExecuted()) {
            mCallGetOrders.cancel();
        }
    }

    public void getOrders() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallGetOrders = Api.WEB_SERVICE.get_orders("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
            mCallGetOrders.enqueue(new Callback<GetOrdersMainObject>() {
                @Override
                public void onResponse(@NonNull Call<GetOrdersMainObject> call, @NonNull Response<GetOrdersMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                getOrdersData = response.body().getData();
                                if (getOrdersData.size() == 0) {
                                    mBind.mTvNoOrderFound.setVisibility(View.VISIBLE);
                                } else {
                                    mBind.mTvNoOrderFound.setVisibility(View.GONE);
                                }
                                ordersAdapter = new OrdersAdapter(getActivity(), getOrdersData);
                                mBind.mrvOrders.setAdapter(ordersAdapter);
                                mBind.mTvAllOrdersCount.setText("(" + response.body().getData().size() + ")");
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetOrdersMainObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

}