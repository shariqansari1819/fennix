
package com.fennix.android.prod.model.sitepages;

public class SitePagesData {

    private String pages_id;
    private String pages_name;

    public String getPages_id() {
        return pages_id;
    }

    public void setPages_id(String pages_id) {
        this.pages_id = pages_id;
    }

    public String getPages_name() {
        return pages_name;
    }

    public void setPages_name(String pages_name) {
        this.pages_name = pages_name;
    }

}
