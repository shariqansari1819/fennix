package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.model.conversation.ChatConversationData;
import com.fennix.android.prod.model.eventbus.EventBusConversation;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.fennix.android.prod.R;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ConversationHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<ChatConversationData> conversationDataList;
    private int[] avatars = {R.drawable.avatar_1, R.drawable.avatar_2, R.drawable.avatar_3, R.drawable.avatar_4};

    public ConversationAdapter(Context context, List<ChatConversationData> conversationDataList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.conversationDataList = conversationDataList;
    }

    @NonNull
    @Override
    public ConversationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_conversation, parent, false);
        return new ConversationHolder(view, new ConversationHolder.onConversationClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusConversation(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationHolder holder, int position) {
        ChatConversationData chatConversationData = conversationDataList.get(position);
        Glide.with(context)
                .load(generateRandomAvatar())
                .into(holder.imageViewAvatar);
        holder.textViewName.setText(chatConversationData.getSender_name());
        String lastMessage = chatConversationData.getLast_message();
        if (lastMessage.length() >= 27) {
            lastMessage = lastMessage.substring(0, 27) + "...";
        }
        holder.textViewLastMessage.setText(lastMessage);
        if (chatConversationData.getUnread_messages_count().equalsIgnoreCase("0")) {
            holder.textViewCounter.setVisibility(View.GONE);
        } else {
            holder.textViewCounter.setText(chatConversationData.getUnread_messages_count());
            holder.textViewCounter.setVisibility(View.VISIBLE);
        }
        try {
            Date lastMessageDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(chatConversationData.getLast_message_time());
            holder.textViewLastMessageTime.setText(((MainActivity) context).fnGetTimeAgoStatus(lastMessageDate));
        } catch (Exception e) {

        }
    }

    private int generateRandomAvatar() {
        Random random = new Random();
        int number = random.nextInt(3);
        return avatars[number];
    }

    @Override
    public int getItemCount() {
        return conversationDataList.size();
    }

    static class ConversationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageViewAvatar;
        private TextView textViewCounter, textViewName, textViewLastMessage, textViewLastMessageTime;
        private onConversationClick onConversationClick;

        ConversationHolder(View itemView, onConversationClick onConversationClick) {
            super(itemView);
            this.onConversationClick = onConversationClick;

            imageViewAvatar = itemView.findViewById(R.id.mImgAvatar);
            textViewCounter = itemView.findViewById(R.id.mTvBadgeConversation);
            textViewName = itemView.findViewById(R.id.mTvName);
            textViewLastMessage = itemView.findViewById(R.id.mTvLastMessage);
            textViewLastMessageTime = itemView.findViewById(R.id.mTvLastMessageTime);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onConversationClick.rowClick(getAdapterPosition(), view.getId());
        }

        interface onConversationClick {
            void rowClick(int position, int id);
        }
    }
}