package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;
import com.fennix.android.prod.model.usersites.Data;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import com.fennix.android.prod.R;

public class PremiumTemplateAdapter extends RecyclerView.Adapter<PremiumTemplateAdapter.PremiumTemplateHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Data> userTemplatesListData;

    public PremiumTemplateAdapter(Context context, List<Data> userTemplatesListData) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.userTemplatesListData = userTemplatesListData;
    }

    @NonNull
    @Override
    public PremiumTemplateAdapter.PremiumTemplateHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_premium_templates, parent, false);
        return new PremiumTemplateAdapter.PremiumTemplateHolder(view, new PremiumTemplateAdapter.PremiumTemplateHolder.OnTemplateClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusTemplateId(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull PremiumTemplateAdapter.PremiumTemplateHolder holder, int position) {
        Data data = userTemplatesListData.get(position);
        Glide.with(context)
                .load(((MainActivity) context).mSiteThumbnailBaseUrl + data.getSitethumb())
                .apply(new RequestOptions().centerCrop())
                .into(holder.mImgTemplate);
    }

    @Override
    public int getItemCount() {
        return userTemplatesListData.size();
    }

    static class PremiumTemplateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mImgTemplate;
        private PremiumTemplateAdapter.PremiumTemplateHolder.OnTemplateClick onTemplateClickListener;

        public PremiumTemplateHolder(View itemView, PremiumTemplateAdapter.PremiumTemplateHolder.OnTemplateClick onTemplateClickListener) {
            super(itemView);
            this.onTemplateClickListener = onTemplateClickListener;
            mImgTemplate = itemView.findViewById(R.id.mImgPremiumTemplate);

            mImgTemplate.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onTemplateClickListener.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnTemplateClick {
            void rowClick(int position, int id);
        }
    }
}
