
package com.fennix.android.prod.model.createpage;

import com.fennix.android.prod.model.createpage.CreatePageData;

public class CreatePageObject {

    private String status;
    private CreatePageData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CreatePageData getData() {
        return data;
    }

    public void setData(CreatePageData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
