package com.fennix.android.prod.frag;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragLoginBinding;
import com.fennix.android.prod.model.api.loginsignup.Data;
import com.fennix.android.prod.model.api.loginsignup.LoginSignup;
import com.fennix.android.prod.model.usersites.UserSitesModel;
import com.fennix.android.prod.utils.GlobalClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import com.fennix.android.prod.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fraglogin extends Fragment implements View.OnClickListener {

    private String mFbProfileImage, mFbFirstName, mFbLastName;
    private FragLoginBinding mBind;
    private CallbackManager mCallbackManager;
    private Call<LoginSignup> userCall;
    private Call<UserSitesModel> mRetrofitCall;

    public Fraglogin() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_login, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        ((MainActivity) getActivity()).mBind.mBottombar.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mBind.mPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        mBind.mBtnSignin.setOnClickListener(this);
        mBind.mBtnSigninFacebook.setOnClickListener(this);
        mBind.mTvDontHaveAccount.setOnClickListener(this);
        mBind.mTvForgetPassword.setOnClickListener(this);

        //facebook login process
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        loginResult.getAccessToken().getToken();
                        loginResult.getAccessToken().getUserId();
                        loginResult.getAccessToken().getApplicationId();
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject mJsonObj, GraphResponse response) {
                                        mBind.mPb.setVisibility(View.INVISIBLE);
                                        String mResponse = response.toString();
                                        try {
                                            String email = mJsonObj.getString("email");
                                            String id = mJsonObj.getString("id");
                                            String first_name = mJsonObj.getString("first_name");
                                            String last_name = mJsonObj.getString("last_name");
                                            mFbFirstName = first_name;
                                            mFbLastName = last_name;
                                            mFbProfileImage = mJsonObj.getJSONObject("picture").getJSONObject("data").getString("url");
                                            reqLogin("facebook", email, loginResult.getAccessToken().getUserId(), first_name + " " + last_name);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name,last_name, email, gender,birthday,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();
                        mBind.mPb.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancel() {
                        mBind.mPb.setVisibility(View.INVISIBLE);
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.facebooklogin_calcel), R.color.error);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        mBind.mPb.setVisibility(View.INVISIBLE);
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, exception.toString(), R.color.error);
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, "Try Login Again Now", R.color.error);
                                    }
                                }, 1000);
                            }
                        }
                    }
                }
        );
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (userCall != null && userCall.isExecuted()) {
            userCall.cancel();
        }
        if (mRetrofitCall != null && mRetrofitCall.isExecuted()) {
            mRetrofitCall.cancel();
        }
    }

    @Override
    public void onClick(View vi) {
        switch (vi.getId()) {
            case R.id.mBtnSignin:
                if (mBind.mPb.getVisibility() == View.VISIBLE) {
                    return;
                }
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                if (mBind.mEtPassword.getText().toString().trim().isEmpty() || mBind.mEtPassword.getText().toString().trim().length() == 0) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.password_is_empty), R.color.error);
                } else if (mBind.mEtEmail.getText().toString().trim().isEmpty() || mBind.mEtEmail.getText().toString().trim().length() == 0) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.email_is_empty), R.color.error);
                } else {
                    reqLogin("login", mBind.mEtEmail.getText().toString().trim(), mBind.mEtPassword.getText().toString().trim(), "");
                }
                break;
            case R.id.mTvDontHaveAccount:
                ((MainActivity) getActivity()).fnLoadFragAdd("SIGNUP", true, null);
                break;
            case R.id.mBtnSigninFacebook:
                if (!((MainActivity) getActivity()).fnIsisOnline()) {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    return;
                }
                LoginManager.getInstance().logInWithReadPermissions(Fraglogin.this, Arrays.asList("email", "public_profile"));
                break;
            case R.id.mTvForgetPassword:
                ((MainActivity) getActivity()).fnLoadFragAdd("FORGOT PASSWORD", true, null);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void fnMessageShow(String mFlag, String mMsg, String mStatus) {
        switch (mFlag) {
            case "login":
                switch (mStatus) {
                    case "0":
                        if (mMsg.contentEquals("The Email field must contain a valid email address.")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.Login1), R.color.error);
                        } else if (mMsg.contentEquals("Incorrect Username/Password.")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.Login3), R.color.error);
                        }
                        break;
                    case "1":
                        break;
                }
                break;
            case "facebook":
                switch (mStatus) {
                    case "0":
                        if (mMsg.contentEquals("The Email field must contain a valid email address.")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.login_fb1), R.color.error);
                        }
                        break;
                    case "1":
                        break;
                }
                break;
        }
    }

    private void reqLogin(final String mFlag, String mEmail, String mPassword, String mName) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            switch (mFlag) {
                case "login":
                    userCall = Api.WEB_SERVICE.login("55555", mEmail, mPassword, GlobalClass.getInstance().mPref.getString("fcm_token", ""), "3");
                    break;
                case "facebook":
                    userCall = Api.WEB_SERVICE.facebooklogin("55555", mName, mEmail, mPassword, GlobalClass.getInstance().mPref.getString("fcm_token", ""), "3");
                    break;
            }
            userCall.enqueue(new Callback<LoginSignup>() {
                @Override
                public void onResponse(@NonNull Call<LoginSignup> call, @NonNull Response<LoginSignup> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                Data mObj = response.body().getData();
                                switch (mFlag) {
                                    case "facebook":
                                        if (mObj.getProfile_image().equalsIgnoreCase("avatar.png")) {
                                            GlobalClass.getInstance().mPrefEditor.putString("profile_image", mFbProfileImage);
                                        } else {
                                            GlobalClass.getInstance().mPrefEditor.putString("profile_image", mObj.getProfile_image());
                                        }
                                        GlobalClass.getInstance().mPrefEditor.putString("first_name", mFbFirstName);
                                        GlobalClass.getInstance().mPrefEditor.putString("last_name", mFbLastName);
                                        GlobalClass.getInstance().mPrefEditor.putBoolean("fb", true);
                                        break;
                                    case "login":
                                        GlobalClass.getInstance().mPrefEditor.putString("profile_image", mObj.getProfile_image());
                                        GlobalClass.getInstance().mPrefEditor.putString("first_name", mObj.getFirst_name());
                                        GlobalClass.getInstance().mPrefEditor.putString("last_name", mObj.getLast_name());
                                        GlobalClass.getInstance().mPrefEditor.putBoolean("fb", false);
                                        break;
                                }
                                GlobalClass.getInstance().mPrefEditor.putString("id", mObj.getId());
                                GlobalClass.getInstance().mPrefEditor.putString("package_id", mObj.getPackage_id());
                                GlobalClass.getInstance().mPrefEditor.putString("username", mObj.getUsername());
                                GlobalClass.getInstance().mPrefEditor.putString("email", mObj.getEmail());
                                GlobalClass.getInstance().mPrefEditor.putString("first_name", mObj.getFirst_name());
                                GlobalClass.getInstance().mPrefEditor.putString("last_name", mObj.getLast_name());
                                GlobalClass.getInstance().mPrefEditor.putString("activation_code", mObj.getActivation_code());
                                GlobalClass.getInstance().mPrefEditor.putString("forgot_code", mObj.getForgot_code());
                                GlobalClass.getInstance().mPrefEditor.putString("remember_code", mObj.getRemember_code());
                                GlobalClass.getInstance().mPrefEditor.putString("last_login", mObj.getLast_login());
                                GlobalClass.getInstance().mPrefEditor.putString("stripe_cus_id", mObj.getStripe_cus_id());
                                GlobalClass.getInstance().mPrefEditor.putString("stripe_sub_id", mObj.getStripe_sub_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_token", mObj.getPaypal_token());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_profile_id", mObj.getPaypal_profile_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_last_transaction_id", mObj.getPaypal_last_transaction_id());
                                GlobalClass.getInstance().mPrefEditor.putString("current_subscription_gateway", mObj.getCurrent_subscription_gateway());
                                GlobalClass.getInstance().mPrefEditor.putString("payer_id", mObj.getPayer_id());
                                GlobalClass.getInstance().mPrefEditor.putString("paypal_next_payment_date", mObj.getPaypal_next_payment_date());
                                GlobalClass.getInstance().mPrefEditor.putString("type", mObj.getType());
                                GlobalClass.getInstance().mPrefEditor.putString("status", mObj.getStatus());
                                GlobalClass.getInstance().mPrefEditor.putString("created_at", mObj.getCreated_at());
                                GlobalClass.getInstance().mPrefEditor.putString("modified_at", mObj.getModified_at());
                                GlobalClass.getInstance().mPrefEditor.putString("facebook_uid", mObj.getFacebook_uid());
                                GlobalClass.getInstance().mPrefEditor.putString("password", mObj.getPassword());
                                GlobalClass.getInstance().mPrefEditor.putString("package_type", mObj.getPackage_type());
                                if (mObj.getSiteslist() != null && mObj.getSiteslist().size() > 0) {
                                    GlobalClass.getInstance().mPrefEditor.putInt("site_size", mObj.getSiteslist().size());
                                    GlobalClass.getInstance().mPrefEditor.putString("page_id", mObj.getSiteslist().get(0).getPages_id());
                                    GlobalClass.getInstance().mPrefEditor.putString("site_id", mObj.getSiteslist().get(0).getSites_id());
                                    GlobalClass.getInstance().mPrefEditor.putString("site_name", mObj.getSiteslist().get(0).getSites_name());
                                    GlobalClass.getInstance().mPrefEditor.putString("site_thumb", mObj.getSiteslist().get(0).getSitethumb());
                                    GlobalClass.getInstance().mPrefEditor.putString("sub_domain", mObj.getSiteslist().get(0).getSub_domain());
                                    GlobalClass.getInstance().mPrefEditor.putString("page_preview", mObj.getSiteslist().get(0).getPages_preview());
                                    GlobalClass.getInstance().mPrefEditor.putString("custom_domain", mObj.getSiteslist().get(0).getCustom_domain());
                                    GlobalClass.getInstance().mPrefEditor.putString("is_ssl", mObj.getSiteslist().get(0).getIs_ssl());
                                }
                                GlobalClass.getInstance().mPrefEditor.putBoolean("login", true);
                                GlobalClass.getInstance().mPrefEditor.putString("site_limit", mObj.getSitelimit());
                                GlobalClass.getInstance().mPrefEditor.putString("is_commerce", mObj.getIsCommerece());
                                GlobalClass.getInstance().mPrefEditor.putString("notification_status", mObj.getNotification_status());
                                GlobalClass.getInstance().mPrefEditor.putString("chat_status", mObj.getChat_status());
                                funGetUserSites();
                                GlobalClass.getInstance().mPrefEditor.apply();
                                break;
                            case "0":
                                fnMessageShow(mFlag, response.body().getMessage(), "0");
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginSignup> call, @NonNull Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void funGetUserSites() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mRetrofitCall = Api.WEB_SERVICE.getUserSites("55555", GlobalClass.getInstance().mPref.getString("id", ""));
        mRetrofitCall.enqueue(new Callback<UserSitesModel>() {
            @Override
            public void onResponse(Call<UserSitesModel> call, retrofit2.Response<UserSitesModel> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "0":
                            ((MainActivity) getActivity()).mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            ((MainActivity) getActivity()).fnLoadFragReplace("WELCOME", null);
                            ((MainActivity) getActivity()).fnHideKeyboardFrom(mBind.mEtEmail);
                            break;
                        case "1":
                            GlobalClass.getInstance().mPrefEditor.putBoolean("first_time", true);
                            GlobalClass.getInstance().mPrefEditor.apply();
                            ((MainActivity) getActivity()).mPosPrevious = 1;
                            ((MainActivity) getActivity()).mBind.mImgHome.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
//                            if (GlobalClass.getInstance().mPref.getString("package_type", "Free").equals("Free")) {
                            GlobalClass.getInstance().mPrefEditor.putString("page_preview", response.body().getData().get(0).getPages_preview());
                            if (GlobalClass.getInstance().mPref.getString("page_preview", "").equals("")) {
                                Bundle bundle = new Bundle();
                                bundle.putString("mFlag", "create_site");
                                ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", false, bundle);
                            } else {
                                ((MainActivity) getActivity()).fnLoadFragReplace("HOME", null);
                            }
//                            } else {
//                                ((MainActivity) getActivity()).fnLoadFragReplace("PREMIUM HOME", null);
//                            }
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<UserSitesModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}