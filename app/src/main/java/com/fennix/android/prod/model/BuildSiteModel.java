package com.fennix.android.prod.model;

public class BuildSiteModel {

    private String status;
    private String pages_preview;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPages_preview() {
        return pages_preview;
    }

    public void setPages_preview(String pages_preview) {
        this.pages_preview = pages_preview;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}