
package com.fennix.android.prod.model.addcoupons;


public class AddCouponData {

    private String coupon_id;
    private String site_id;
    private String coupon_name;
    private String coupon_code;
    private String coupon_description;
    private String coupon_type;
    private String coupon_amount;
    private String coupon_expiry;
    private String coupon_limit;
    private String coupon_limit_user;
    private String coupon_applied;
    private String coupon_usage_count;
    private String coupon_status;
    private String coupon_created;

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getCoupon_description() {
        return coupon_description;
    }

    public void setCoupon_description(String coupon_description) {
        this.coupon_description = coupon_description;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getCoupon_amount() {
        return coupon_amount;
    }

    public void setCoupon_amount(String coupon_amount) {
        this.coupon_amount = coupon_amount;
    }

    public String getCoupon_expiry() {
        return coupon_expiry;
    }

    public void setCoupon_expiry(String coupon_expiry) {
        this.coupon_expiry = coupon_expiry;
    }

    public String getCoupon_limit() {
        return coupon_limit;
    }

    public void setCoupon_limit(String coupon_limit) {
        this.coupon_limit = coupon_limit;
    }

    public String getCoupon_limit_user() {
        return coupon_limit_user;
    }

    public void setCoupon_limit_user(String coupon_limit_user) {
        this.coupon_limit_user = coupon_limit_user;
    }

    public String getCoupon_applied() {
        return coupon_applied;
    }

    public void setCoupon_applied(String coupon_applied) {
        this.coupon_applied = coupon_applied;
    }

    public String getCoupon_usage_count() {
        return coupon_usage_count;
    }

    public void setCoupon_usage_count(String coupon_usage_count) {
        this.coupon_usage_count = coupon_usage_count;
    }

    public String getCoupon_status() {
        return coupon_status;
    }

    public void setCoupon_status(String coupon_status) {
        this.coupon_status = coupon_status;
    }

    public String getCoupon_created() {
        return coupon_created;
    }

    public void setCoupon_created(String coupon_created) {
        this.coupon_created = coupon_created;
    }

}
