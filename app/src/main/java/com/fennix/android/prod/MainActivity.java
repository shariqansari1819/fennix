package com.fennix.android.prod;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.fennix.android.prod.databinding.ActivityMainBinding;
import com.fennix.android.prod.frag.FragBrowser;
import com.fennix.android.prod.frag.FragBuilderBlocks;
import com.fennix.android.prod.frag.FragChatConversation;
import com.fennix.android.prod.frag.FragChatDetail;
import com.fennix.android.prod.frag.FragChatSetting;
import com.fennix.android.prod.frag.FragCouponEdit;
import com.fennix.android.prod.frag.FragCreateFreeSite;
import com.fennix.android.prod.frag.FragEditProfile;
import com.fennix.android.prod.frag.FragEditTemplatePages;
import com.fennix.android.prod.frag.FragEmail;
import com.fennix.android.prod.frag.FragForgotPassword;
import com.fennix.android.prod.frag.FragHome;
import com.fennix.android.prod.frag.FragInputEmailPhone;
import com.fennix.android.prod.frag.FragLanguage;
import com.fennix.android.prod.frag.FragManageStore;
import com.fennix.android.prod.frag.FragOrderDetail;
import com.fennix.android.prod.frag.FragPremiumHome;
import com.fennix.android.prod.frag.FragProductEdit;
import com.fennix.android.prod.frag.FragProductOptions;
import com.fennix.android.prod.frag.FragProfile;
import com.fennix.android.prod.frag.FragRefer;
import com.fennix.android.prod.frag.FragSignup;
import com.fennix.android.prod.frag.FragSplash;
import com.fennix.android.prod.frag.FragSuccessEmailPhone;
import com.fennix.android.prod.frag.FragTerms;
import com.fennix.android.prod.frag.FragUserTemplates;
import com.fennix.android.prod.frag.FragWebsiteGif;
import com.fennix.android.prod.frag.FragWelcomeFennix;
import com.fennix.android.prod.frag.Fraglogin;
import com.fennix.android.prod.model.eventbus.EventBusLogOut;
import com.fennix.android.prod.model.eventbus.EventBusProfileImagePath;
import com.fennix.android.prod.model.eventbus.EventBusUpdateConversation;
import com.fennix.android.prod.utils.GlobalClass;
import com.fennix.android.prod.utils.gallery.GalleryActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragmentManager.OnBackStackChangedListener {

    private Context mContext;
    private Fragment mFrag;
    private Class mFragClass;
    public FragmentManager mFragManager;
    public ActivityMainBinding mBind;
    public String mFScreenName, mFBaseUrlImages, mSiteThumbnailBaseUrl, mSiteBaseUrl;
    public int mPosPrevious;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBind = DataBindingUtil.setContentView(this, R.layout.activity_main);
        GlobalClass.getInstance().mFappOpen = true;
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        GlobalClass.getInstance().mFappOpen = false;
    }

    private void init() {
        mFBaseUrlImages = "http://boss-constructions.com/uploads";
        mSiteThumbnailBaseUrl = "http://boss-constructions.com/";
        mSiteBaseUrl = "http://boss-constructions.com/";
        mContext = this;
        if (GlobalClass.getInstance().mFlanguage) {
            fnLanguageChange(GlobalClass.getInstance().mPref.getString("language", ""));
            GlobalClass.getInstance().mFlanguage = false;
        }
        mFragManager = getSupportFragmentManager();
        mFragManager.addOnBackStackChangedListener(this);

        Bundle bundle = new Bundle();
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("notification_type") != null) {
                if (getIntent().getExtras().getString("notification_type", "").equals("Message")) {
                    bundle.putString("mFlag", "message");
                } else {
                    bundle.putString("mFlag", "none");
                }
            }
        }
        fnLoadFragAdd("SPLASH", true, bundle);
        mBind.mImgBrowser.setOnClickListener(this);
        mBind.mImgChat.setOnClickListener(this);
        mBind.mImgHome.setOnClickListener(this);
        mBind.mImgProfile.setOnClickListener(this);
        mBind.mImgRefer.setOnClickListener(this);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                GlobalClass.getInstance().mPrefEditor.putString("fcm_token", deviceToken).apply();
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (mFScreenName != null) {
            if (mFragManager.getBackStackEntryCount() >= 1) {
                mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
            switch (view.getId()) {
                case R.id.mImgProfile:
                    if (!mFScreenName.contentEquals("PROFILE")) {
                        mFScreenName = "PROFILE";
                        fnLoadFragReplace("PROFILE", null);
                        fnChangeColorBottom();
                        mPosPrevious = 5;
                        mBind.mImgProfile.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    }
                    break;
                case R.id.mImgChat:
                    if (!mFScreenName.contentEquals("CONVERSATION")) {
                        mFScreenName = "CONVERSATION";
                        fnLoadFragReplace("CONVERSATION", null);
                        fnChangeColorBottom();
                        mPosPrevious = 4;
                        mBind.mImgChat.setImageDrawable(getResources().getDrawable(R.mipmap.icon_chat_selected));
                    }
                    break;
                case R.id.mImgRefer:
                    if (!mFScreenName.contentEquals("REFER")) {
                        mFScreenName = "REFER";
                        fnLoadFragReplace("REFER", null);
                        fnChangeColorBottom();
                        mPosPrevious = 3;
                        mBind.mImgRefer.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    }
                    break;
                case R.id.mImgBrowser:
                    if (!mFScreenName.contentEquals("BROWSER")) {
                        mFScreenName = "BROWSER";
                        Bundle bundle = new Bundle();
                        bundle.putString("mFlag", "browser");
                        fnLoadFragReplace("BROWSER", bundle);
                        fnChangeColorBottom();
                        mPosPrevious = 2;
                        mBind.mImgBrowser.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    }
                    break;
                case R.id.mImgHome:
                    if (!mFScreenName.contentEquals("HOME")) {
                        mFScreenName = "HOME";
//                        if (GlobalClass.getInstance().mPref.getString("package_type", "Free").equals("Free")) {
                        fnLoadFragReplace("HOME", null);
//                        } else {
//                            fnLoadFragReplace("PREMIUM HOME", null);
//                        }
                        fnChangeColorBottom();
                        mPosPrevious = 1;
                        mBind.mImgHome.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    }
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void logOutEvent(EventBusLogOut eventBusLogOut) {
        if (eventBusLogOut.isLogOut()) {
            fnLogOut();
        }
    }

    @Override
    public void onBackStackChanged() {
        if (mFragManager.findFragmentById(R.id.main_frag) != null) {
            mFScreenName = mFragManager.findFragmentById(R.id.main_frag).getTag();
            switch (mFScreenName) {
                case "LANGUAGE":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "PROFILE":
                    mBind.mBottombar.setVisibility(View.VISIBLE);
                    break;
                case "EDIT PROFILE":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "EDIT TEMPLATE":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "SELECT TEMPLATE":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "HOME":
                    mBind.mBottombar.setVisibility(View.VISIBLE);
                    break;
                case "BLOCK ITEMS":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "CHAT":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "CONVERSATION":
                    mBind.mBottombar.setVisibility(View.VISIBLE);
                    EventBus.getDefault().post(new EventBusUpdateConversation());
                    break;
                case "PRODUCT OPTIONS":
                    mBind.mBottombar.setVisibility(View.GONE);
                    break;
                case "PRODUCT EDIT":
                    mBind.mBottombar.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    public void fnChangeColorBottom() {
        switch (mPosPrevious) {
            case 1:
                mBind.mImgHome.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                break;
            case 2:
                mBind.mImgBrowser.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                break;
            case 3:
                mBind.mImgRefer.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                break;
            case 4:
                mBind.mImgChat.setImageDrawable(getResources().getDrawable(R.mipmap.icon_chat_deselected));
                break;
            case 5:
                mBind.mImgProfile.setColorFilter(ContextCompat.getColor(mContext, R.color.grey));
                break;
        }
    }

    public void fnLoadFragAdd(String mType, boolean mBackStack, Bundle mBundle) {
        mFrag = null;
        mFragClass = null;
        switch (mType) {
            case "SIGNUP":
                mFragClass = FragSignup.class;
                break;
            case "WELCOME":
                mFragClass = FragWelcomeFennix.class;
                break;
            case "LOGIN":
                mFragClass = Fraglogin.class;
                break;
            case "HOME":
                mFragClass = FragHome.class;
                break;
            case "SPLASH":
                mFragClass = FragSplash.class;
                break;
            case "FORGOT PASSWORD":
                mFragClass = FragForgotPassword.class;
                break;
            case "LANGUAGE":
                mFragClass = FragLanguage.class;
                break;
            case "EMAIL":
                mFragClass = FragEmail.class;
                break;
            case "PROFILE":
                mFragClass = FragProfile.class;
                break;
            case "EDIT PROFILE":
                mFragClass = FragEditProfile.class;
                break;
            case "CONVERSATION":
                mFragClass = FragChatConversation.class;
                break;
            case "CHAT SETTING":
                mFragClass = FragChatSetting.class;
                break;
            case "CHAT":
                mFragClass = FragChatDetail.class;
                break;
            case "REFER":
                mFragClass = FragRefer.class;
                break;
            case "BROWSER":
                mFragClass = FragBrowser.class;
                break;
            case "INPUT EMAIL PHONE":
                mFragClass = FragInputEmailPhone.class;
                break;
            case "SUCCESS EMAIL PHONE":
                mFragClass = FragSuccessEmailPhone.class;
                break;
            case "CREATE SITE":
                mFragClass = FragCreateFreeSite.class;
                break;
            case "SELECT TEMPLATE":
                mFragClass = FragUserTemplates.class;
                break;
            case "EDIT TEMPLATE":
                mFragClass = FragEditTemplatePages.class;
                break;
            case "BLOCK ITEMS":
                mFragClass = FragBuilderBlocks.class;
                break;
            case "TERMS":
                mFragClass = FragTerms.class;
                break;
            case "MANAGE STORE":
                mFragClass = FragManageStore.class;
                break;
            case "PRODUCT OPTIONS":
                mFragClass = FragProductOptions.class;
                break;
            case "COUPON EDIT":
                mFragClass = FragCouponEdit.class;
                break;
            case "PRODUCT EDIT":
                mFragClass = FragProductEdit.class;
                break;
            case "ORDER DETAIL":
                mFragClass = FragOrderDetail.class;
                break;
        }
        try {
            mFrag = (Fragment) mFragClass.newInstance();
            if (mBundle != null) {
                mFrag.setArguments(mBundle);
            }
            if (mBackStack) {
                mFragManager.beginTransaction().add(R.id.main_frag, mFrag, mType).addToBackStack(mType).commit();
            } else {
                mFragManager.beginTransaction().add(R.id.main_frag, mFrag, mType).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fnLoadFragReplace(String mType, Bundle mBundle) {
        mFrag = null;
        mFragClass = null;
        switch (mType) {
            case "HOME":
                mFragClass = FragHome.class;
                break;
            case "PROFILE":
                mFragClass = FragProfile.class;
                break;
            case "CONVERSATION":
                mFragClass = FragChatConversation.class;
                break;
            case "REFER":
                mFragClass = FragRefer.class;
                break;
            case "BROWSER":
                mFragClass = FragBrowser.class;
                break;
            case "LOGIN":
                mFragClass = Fraglogin.class;
                break;
            case "WELCOME":
                mFragClass = FragWelcomeFennix.class;
                break;
            case "LANGUAGE":
                mFragClass = FragLanguage.class;
                break;
            case "WEBSITE GIF":
                mFragClass = FragWebsiteGif.class;
                break;
            case "PREMIUM HOME":
                mFragClass = FragPremiumHome.class;
                break;
        }
        try {
            mFrag = (Fragment) mFragClass.newInstance();
            if (mBundle != null) {
                mFrag.setArguments(mBundle);
            }
            mFragManager.beginTransaction().replace(R.id.main_frag, mFrag, mType).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fnNoTitleLoadFragAdd(String mType, boolean mBackStack, Bundle mBundle, String mTitle) {
//        mFrag = null;
//        mFragClass = null;
//        switch (mType) {
//            case "DISH DETAIL":
//                mFragClass = DishDetailFrag.class;
//                break;
//        }
//        try {
//            mFrag = (Fragment) mFragClass.newInstance();
//            if (mBundle != null) {
//                mFrag.setArguments(mBundle);
//            }
//            if (mBackStack) {
//                mFragManager.beginTransaction().add(R.id.main_frag, mFrag, mType).addToBackStack(mType).commit();
//            } else {
//                mFragManager.beginTransaction().add(R.id.main_frag, mFrag, mType).commit();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void fnRequestPermission(int resultCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, resultCode);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(this, GalleryActivity.class);
                intent.putExtra("mFlag", "single");
                startActivityForResult(intent, 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Gson mGson = new Gson();
                List<String> mData = mGson.fromJson(data.getStringExtra("result"), new TypeToken<List<String>>() {
                }.getType());
                EventBus.getDefault().post(new EventBusProfileImagePath(mData.get(0)));
            }
        }
    }

    public boolean fnCheckPermission() {
        boolean isAllowed;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            isAllowed = true;
        } else {
            isAllowed = false;
        }
        return isAllowed;
    }

    // global functions
    public void fnHideKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void fnShowKeyboardFrom(View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public boolean fnIsisOnline() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void fnLanguageChange(String mFlag) {
        if (mFlag.contentEquals("")) {
            return;
        }
        String languageToLoad = "";
        if (mFlag.contentEquals("en")) {
            languageToLoad = "en";
        } else {
            languageToLoad = "es";
        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void fnLogOut() {
        GlobalClass.getInstance().mPrefEditor.putBoolean("login", false);
        GlobalClass.getInstance().mPrefEditor.putBoolean("fb", false);
        GlobalClass.getInstance().mPrefEditor.putBoolean("first_time", false);
        GlobalClass.getInstance().mPrefEditor.putString("page_id", "");
        GlobalClass.getInstance().mPrefEditor.putString("site_id", "");
        GlobalClass.getInstance().mPrefEditor.putString("site_name", "");
        GlobalClass.getInstance().mPrefEditor.putString("site_thumb", "");
        GlobalClass.getInstance().mPrefEditor.putString("sub_domain", "");
        GlobalClass.getInstance().mPrefEditor.putString("page_preview", "");
        GlobalClass.getInstance().mPrefEditor.putString("custom_domain", "");
        GlobalClass.getInstance().mPrefEditor.putString("site_limit", "");
        GlobalClass.getInstance().mPrefEditor.putString("profile_image", "");
        GlobalClass.getInstance().mPrefEditor.putString("notification_status", "");
        GlobalClass.getInstance().mPrefEditor.putString("chat_status", "");
        GlobalClass.getInstance().mPrefEditor.apply();
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
    }

    public String fnGetTimeAgoStatus(Date date) {
        int SECOND_MILLIS = 1000;
        int MINUTE_MILLIS = 60 * SECOND_MILLIS;
        int HOUR_MILLIS = 60 * MINUTE_MILLIS;
        int DAY_MILLIS = 24 * HOUR_MILLIS;
        long time = date.getTime();
        if (time < 1000000000000L) {
            time *= 1000;
        }
        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        long now = currentDate.getTime();
        if (time > now || time <= 0) {
            return "in the future";
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "moments ago";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "1 minute ago";
        } else if (diff < 60 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 2 * HOUR_MILLIS) {
            return "1 hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

}
//try {
//        PackageInfo info = getPackageManager().getPackageInfo(
//        "fennix.leadconceptsolution.com.fennix", PackageManager.GET_SIGNATURES);
//        for (Signature signature : info.signatures) {
//        MessageDigest md = MessageDigest.getInstance("SHA");
//        md.update(signature.toByteArray());
//        Log.d("Your Tag", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//        }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
//    public void fnLoadFragReplace(String mType, boolean mBackStack, Bundle mBundle) {
//        mFrag = null;
//        mFragClass = null;
//        switch (mType) {
//            case "SPLASH":
//                mFragClass = FragSplash.class;
//                break;
//        }
//        try {
//            mFrag = (Fragment) mFragClass.newInstance();
//            if (mBundle != null) {
//                mFrag.setArguments(mBundle);
//            }
//            if (mBackStack) {
//                mFragManager.beginTransaction().replace(R.id.main_frag, mFrag, mType).addToBackStack(mType).commit();
//            } else {
//                mFragManager.beginTransaction().replace(R.id.main_frag, mFrag, mType).commit();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }