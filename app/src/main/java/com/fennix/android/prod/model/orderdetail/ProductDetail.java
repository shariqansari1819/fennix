
package com.fennix.android.prod.model.orderdetail;

import java.util.HashMap;
import java.util.Map;

public class ProductDetail {

    private String order_id;
    private String order_status;
    private String order_name;
    private String order_created_date;
    private String order_phone;
    private String order_email;
    private String order_address;
    private String order_city;
    private String order_state;
    private String order_zip;
    private String order_country;
    private String note;
    private Integer orderprice;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getOrder_created_date() {
        return order_created_date;
    }

    public void setOrder_created_date(String order_created_date) {
        this.order_created_date = order_created_date;
    }

    public String getOrder_phone() {
        return order_phone;
    }

    public void setOrder_phone(String order_phone) {
        this.order_phone = order_phone;
    }

    public String getOrder_email() {
        return order_email;
    }

    public void setOrder_email(String order_email) {
        this.order_email = order_email;
    }

    public String getOrder_address() {
        return order_address;
    }

    public void setOrder_address(String order_address) {
        this.order_address = order_address;
    }

    public String getOrder_city() {
        return order_city;
    }

    public void setOrder_city(String order_city) {
        this.order_city = order_city;
    }

    public String getOrder_state() {
        return order_state;
    }

    public void setOrder_state(String order_state) {
        this.order_state = order_state;
    }

    public String getOrder_zip() {
        return order_zip;
    }

    public void setOrder_zip(String order_zip) {
        this.order_zip = order_zip;
    }

    public String getOrder_country() {
        return order_country;
    }

    public void setOrder_country(String order_country) {
        this.order_country = order_country;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getOrderprice() {
        return orderprice;
    }

    public void setOrderprice(Integer orderprice) {
        this.orderprice = orderprice;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
