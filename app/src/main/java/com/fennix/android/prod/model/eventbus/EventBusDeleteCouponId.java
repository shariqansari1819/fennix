package com.fennix.android.prod.model.eventbus;

public class EventBusDeleteCouponId {
    private String id;

    public EventBusDeleteCouponId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
