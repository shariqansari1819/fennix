package com.fennix.android.prod.frag;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragHomeBinding;
import com.fennix.android.prod.model.eventbus.EventBusRefreshHomeWebView;
import com.fennix.android.prod.model.usersites.Data;
import com.fennix.android.prod.model.usersites.UserSitesModel;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URLEncoder;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class FragHome extends Fragment implements View.OnClickListener {

    private FragHomeBinding mBind;
    private Call<UserSitesModel> mUserSitesCall;
    private List<Data> userSitesList;

    public FragHome() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_home, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    private void init() {
        GlobalClass.getInstance().mPref.getString("site_id", "");
        mBind.mTvDomainName.setText(GlobalClass.getInstance().mPref.getString("site_name", ""));
        if (!GlobalClass.getInstance().mPref.getString("package_type", "").equals("Free")) {
            mBind.mTvGoPro.setVisibility(View.GONE);
        }
        mBind.mWvUserSite.getSettings().setJavaScriptEnabled(true);
        mBind.mWvUserSite.getSettings().setLoadWithOverviewMode(true);
        mBind.mWvUserSite.getSettings().setUseWideViewPort(true);
        mBind.mWvUserSite.getSettings().setPluginState(WebSettings.PluginState.ON);
        mBind.mWvUserSite.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBind.mPb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBind.mPb.setVisibility(View.INVISIBLE);
            }
        });
        funGetUserSites();
        if (GlobalClass.getInstance().mPref.getString("is_commerce", "").equals("yes")) {
            mBind.mImgMenu.setVisibility(View.VISIBLE);
        } else {
            mBind.mImgMenu.setVisibility(View.GONE);
        }

        mBind.mWvUserSite.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        mBind.mTvEditPage.setOnClickListener(this);
        mBind.mBtnShareWebsite.setOnClickListener(this);
        mBind.mTvGoPro.setOnClickListener(this);
        mBind.mBtnViewWebsite.setOnClickListener(this);
        mBind.mImgMenu.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!((MainActivity) getActivity()).mFScreenName.equalsIgnoreCase("EDIT TEMPLATE") && !((MainActivity) getActivity()).mFScreenName.equals("SELECT TEMPLATE") && !((MainActivity) getActivity()).mFScreenName.equals("BLOCK ITEMS"))
            ((MainActivity) getActivity()).mBind.mBottombar.setVisibility(View.VISIBLE);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUserSitesCall != null && mUserSitesCall.isExecuted()) {
            mUserSitesCall.cancel();
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvEditPage:
                ((MainActivity) getActivity()).mFScreenName = "EDIT TEMPLATE";
                ((MainActivity) getActivity()).fnLoadFragAdd("EDIT TEMPLATE", true, null);
                break;
            case R.id.mBtnShareWebsite:
                String shareDomain = "";
                String shareProtocol = "";
                if (GlobalClass.getInstance().mPref.getString("is_ssl", "0").equals("1")) {
                    shareProtocol = "https://";
                } else {
                    shareProtocol = "http://";
                }
                if (GlobalClass.getInstance().mPref.getString("package_type", "").equals("Free")) {
                    shareDomain = shareProtocol + GlobalClass.getInstance().mPref.getString("sub_domain", "");
                } else {
                    if (GlobalClass.getInstance().mPref.getString("custom_domain", "").equalsIgnoreCase("")) {
                        shareDomain = shareProtocol + GlobalClass.getInstance().mPref.getString("sub_domain", "");
                    } else {
                        if (GlobalClass.getInstance().mPref.getString("is_ssl", "1").equals("1"))
                            shareDomain = shareProtocol + GlobalClass.getInstance().mPref.getString("custom_domain", "");
                        else
                            shareDomain = shareProtocol + GlobalClass.getInstance().mPref.getString("custom_domain", "");
                    }
                }
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Check out my website " + shareDomain + " created by Fennix - http://onelink.to/fennix");
                startActivity(Intent.createChooser(shareIntent, "Share your website using"));
                break;
            case R.id.mTvGoPro:
                ((MainActivity) getActivity()).mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ((MainActivity) getActivity()).mBind.mBottombar.setVisibility(View.GONE);
                ((MainActivity) getActivity()).fnLoadFragReplace("WELCOME", null);
                break;
            case R.id.mBtnViewWebsite:
                String domain = "";
                String protocol = "";
                if (GlobalClass.getInstance().mPref.getString("is_ssl", "0").equals("1")) {
                    protocol = "https://";
                } else {
                    protocol = "http://";
                }
                if (GlobalClass.getInstance().mPref.getString("package_type", "").equals("Free")) {
                    domain = protocol + GlobalClass.getInstance().mPref.getString("sub_domain", "");
                } else {
                    if (GlobalClass.getInstance().mPref.getString("custom_domain", "").equalsIgnoreCase("")) {
                        domain = protocol + GlobalClass.getInstance().mPref.getString("sub_domain", "");
                    } else {
                        if (GlobalClass.getInstance().mPref.getString("is_ssl", "1").equals("1"))
                            domain = protocol + GlobalClass.getInstance().mPref.getString("custom_domain", "");
                        else
                            domain = protocol + GlobalClass.getInstance().mPref.getString("custom_domain", "");
                    }
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(domain));
                startActivity(Intent.createChooser(browserIntent, "Open browser using"));
                break;
            case R.id.mImgMenu:
                ((MainActivity) getActivity()).fnLoadFragAdd("MANAGE STORE", true, null);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRefresh(EventBusRefreshHomeWebView event) {
        if (Build.VERSION.SDK_INT < 18) {
            mBind.mWvUserSite.clearView();
        } else {
            mBind.mWvUserSite.loadUrl("about:blank");
        }
        funGetUserSites();
    }

    private void funGetUserSites() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mUserSitesCall = Api.WEB_SERVICE.getUserSites("55555", GlobalClass.getInstance().mPref.getString("id", ""));
        mUserSitesCall.enqueue(new Callback<UserSitesModel>() {
            @Override
            public void onResponse(Call<UserSitesModel> call, retrofit2.Response<UserSitesModel> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "0":
                            break;
                        case "1":
                            userSitesList = response.body().getData();
                            GlobalClass.getInstance().mPrefEditor.putString("page_preview", userSitesList.get(0).getPages_preview());
                            GlobalClass.getInstance().mPrefEditor.putString("is_ssl", userSitesList.get(0).getIs_ssl());
                            GlobalClass.getInstance().mPrefEditor.apply();
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                                mBind.mWvUserSite.loadData(URLEncoder.encode(GlobalClass.getInstance().mPref.getString("page_preview", "")).replaceAll("\\+", " "), "text/html", Xml.Encoding.UTF_8.toString());
                            } else {
                                mBind.mWvUserSite.loadDataWithBaseURL(null, GlobalClass.getInstance().mPref.getString("page_preview", ""), "text/html; charset=utf-8", "UTF-8", null);
                            }
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<UserSitesModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}