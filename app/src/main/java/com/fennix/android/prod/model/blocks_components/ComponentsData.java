
package com.fennix.android.prod.model.blocks_components;

import com.fennix.android.prod.model.blocks_components.ComponentList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ComponentsData {

    @SerializedName("component_title")
    @Expose
    private String componentTitle;
    @SerializedName("component_list")
    @Expose
    private List<ComponentList> componentList = null;

    public String getComponentTitle() {
        return componentTitle;
    }

    public void setComponentTitle(String componentTitle) {
        this.componentTitle = componentTitle;
    }

    public List<ComponentList> getComponentList() {
        return componentList;
    }

    public void setComponentList(List<ComponentList> componentList) {
        this.componentList = componentList;
    }

}
