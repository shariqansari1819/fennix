
package com.fennix.android.prod.model.orderdetail;

import java.util.HashMap;
import java.util.Map;

public class OrderDetailMainObject {

    private String status;
    private OrderDetailData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OrderDetailData getData() {
        return data;
    }

    public void setData(OrderDetailData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
