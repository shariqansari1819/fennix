package com.fennix.android.prod.model.eventbus;

import java.util.List;

public class EventBusSelectedOptionSend {
    private List<String> selectedOptionsSendList;
    public List<String> getSelectedOptionsSendList() {
        return selectedOptionsSendList;
    }

    public void setSelectedOptionsSendList(List<String> selectedOptionsSendList) {
        this.selectedOptionsSendList = selectedOptionsSendList;
    }


    public EventBusSelectedOptionSend(List<String> selectedOptionsSendList) {
        this.selectedOptionsSendList = selectedOptionsSendList;
    }





}
