package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragBuilderItemsBinding;
import com.fennix.android.prod.model.blocks_components.BlocksComponentsModel;
import com.fennix.android.prod.model.blocks_components.BlocksData;
import com.fennix.android.prod.model.blocks_components.Category_list;
import com.fennix.android.prod.model.blocks_components.ComponentList;
import com.fennix.android.prod.model.blocks_components.ComponentsData;
import com.fennix.android.prod.model.blocks_html.BlocksHtmlData;
import com.fennix.android.prod.model.blocks_html.BlocksHtmlObject;
import com.fennix.android.prod.model.eventbus.EventBusAddBlock;
import com.fennix.android.prod.model.eventbus.EventBusAddComponent;
import com.fennix.android.prod.utils.GlobalClass;
import com.fennix.android.prod.utils.expand_recyclerview.ItemChildHolder;
import com.fennix.android.prod.utils.expand_recyclerview.ItemParentHolder;
import com.xwray.groupie.ExpandableGroup;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.OnItemClickListener;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragBuilderBlocks extends Fragment implements OnItemClickListener {

    private FragBuilderItemsBinding mBind;
    private GroupAdapter mAdopter;
    private GridLayoutManager mLayManagr;
    private Call<BlocksComponentsModel> mRetrofitCall;
    private Call<BlocksHtmlObject> mBlockHtmlCall;

    public FragBuilderBlocks() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_builder_items, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mAdopter = new GroupAdapter();
        mAdopter.setSpanCount(2);
        mLayManagr = new GridLayoutManager(getActivity(), mAdopter.getSpanCount());
        mLayManagr.setSpanSizeLookup(mAdopter.getSpanSizeLookup());
        mBind.mRv.setLayoutManager(mLayManagr);
        mBind.mRv.setAdapter(mAdopter);
        mAdopter.setOnItemClickListener(this);
        if (mBind.mPb.getVisibility() == View.VISIBLE)
            return;
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        reqBlocks();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRetrofitCall != null && mRetrofitCall.isExecuted()) {
            mRetrofitCall.cancel();
        }
        if (mBlockHtmlCall != null && mBlockHtmlCall.isExecuted()) {
            mBlockHtmlCall.cancel();
        }
    }

    @Override
    public void onItemClick(@NonNull Item item, @NonNull View view) {
        if (mBind.mPb.getVisibility() == View.VISIBLE)
            return;
        if (!((MainActivity) getActivity()).fnIsisOnline()) {
            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
            return;
        }
        if (item instanceof ItemChildHolder) {
            if (getArguments().getString("mFlag", "").equals("blocks"))
                reqBlockHtml(((ItemChildHolder) item).getBlocks_id());
            else {
                EventBus.getDefault().post(new EventBusAddComponent(((ItemChildHolder) item).getComponent_html()));
                getActivity().onBackPressed();
            }
        }
    }

    private void reqBlocks() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            if (getArguments().getString("mFlag", "").equals("blocks"))
                mRetrofitCall = Api.WEB_SERVICE.blocks("55555", GlobalClass.getInstance().mPref.getString("id", ""));
            else
                mRetrofitCall = Api.WEB_SERVICE.components("55555", "all");
            mRetrofitCall.enqueue(new Callback<BlocksComponentsModel>() {
                @Override
                public void onResponse(@NonNull Call<BlocksComponentsModel> call, @NonNull Response<BlocksComponentsModel> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                if (getArguments().getString("mFlag", "").equals("blocks")) {
                                    for (BlocksData mParent : response.body().getBlocksData()) {
                                        ItemParentHolder mParentHolder = new ItemParentHolder(mParent.getCategory_title(), true);
                                        ExpandableGroup mExpangGroup = new ExpandableGroup(mParentHolder);
                                        for (Category_list mChild : mParent.getCategory_list()) {
                                            mExpangGroup.add(new ItemChildHolder(getResources().getColor(R.color.colorPrimary), (((MainActivity) getActivity()).mSiteThumbnailBaseUrl + mChild.getBlocks_thumb()), mChild.getBlocks_id(), ""));
                                        }
                                        mAdopter.add(mExpangGroup);
                                    }
                                } else {
                                    for (ComponentsData mParent : response.body().getComponentsData()) {
                                        ItemParentHolder mParentHolder = new ItemParentHolder(mParent.getComponentTitle(), true);
                                        ExpandableGroup mExpangGroup = new ExpandableGroup(mParentHolder);
                                        for (ComponentList mChild : mParent.getComponentList()) {
                                            mExpangGroup.add(new ItemChildHolder(getResources().getColor(R.color.colorPrimary), (((MainActivity) getActivity()).mSiteThumbnailBaseUrl + mChild.getComponentsThumb()), mChild.getComponentsId(), mChild.getComponentsMarkup()));
                                        }
                                        mAdopter.add(mExpangGroup);
                                    }
                                }
                                break;
                            case "0":
                                GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, response.body().getMessage(), R.color.error);
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BlocksComponentsModel> call, @NonNull Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void reqBlockHtml(String blockId) {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mBlockHtmlCall = Api.WEB_SERVICE.getBlockHtml("55555", blockId);
            mBlockHtmlCall.enqueue(new Callback<BlocksHtmlObject>() {
                @Override
                public void onResponse(Call<BlocksHtmlObject> call, Response<BlocksHtmlObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                BlocksHtmlData blocksHtmlData = response.body().getData();
                                Document document = Jsoup.parse(blocksHtmlData.getBlock_html());
                                document.select("script").remove();
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://fennixpro.com/dragdropassets/demo.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://www.jqueryscript.net/css/jquerysctipttop.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "http://fennixpro.com/dragdropassets/draganddrop.css");
                                document.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "https://bootswatch.com/flatly/bootstrap.min.css");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/jquery-1.12.4.min.js");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/draganddrop.js");
                                document.head().appendElement("script").attr("src", "http://fennixpro.com/dragdropassets/demo.js");
                                blocksHtmlData.setBlock_html(document.html());
                                EventBus.getDefault().post(new EventBusAddBlock(blocksHtmlData));
                                getActivity().onBackPressed();
                                break;
                            case "0":
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(Call<BlocksHtmlObject> call, Throwable error) {
                    if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                        return;
                    }
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && !error.equals("")) {
                        if (error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });

        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
}
