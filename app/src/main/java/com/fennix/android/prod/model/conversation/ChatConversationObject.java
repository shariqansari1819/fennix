
package com.fennix.android.prod.model.conversation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatConversationObject {

    private String status;
    private List<ChatConversationData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ChatConversationData> getData() {
        return data;
    }

    public void setData(List<ChatConversationData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
