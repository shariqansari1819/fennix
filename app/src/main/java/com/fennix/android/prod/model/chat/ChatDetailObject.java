
package com.fennix.android.prod.model.chat;

import com.fennix.android.prod.model.chat.ChatDetailData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatDetailObject {

    private String status;
    private List<ChatDetailData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ChatDetailData> getData() {
        return data;
    }

    public void setData(List<ChatDetailData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
