package com.fennix.android.prod.utils.expand_recyclerview

import com.fennix.android.prod.R
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_parent.*

open class ItemParentHolder(val title: String, val mFlag: Boolean) : Item(), ExpandableItem {

    private lateinit var expandableGroup: ExpandableGroup

    override fun bind(viewHolder: ViewHolder, position: Int) {
//        if(mFlag){
//            viewHolder.parentimageView.visibility = View.VISIBLE;
//        }else{
//            viewHolder.parentimageView.visibility = View.INVISIBLE;
//        }
//        viewHolder.itemView.parentbtn.setOnClickListener {
//           EventBus.getDefault().post(EventBusPost(viewHolder.item as Item?));
//        }
        viewHolder.mTvParentTitle.text = title
        viewHolder.mImgParentArrow.setImageResource(getRotatedIconResId())
        viewHolder.itemView.setOnClickListener {
            expandableGroup.onToggleExpanded()
            viewHolder.mImgParentArrow.setImageResource(getRotatedIconResId())
        }
    }

    override fun getLayout() = R.layout.item_parent

    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        expandableGroup = onToggleListener
    }

    private fun getRotatedIconResId() =
            if (expandableGroup.isExpanded)
                R.drawable.arrow_up_icon
            else
                R.drawable.arrow_down_icon
}