package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.databinding.FragEmailBinding;
import com.fennix.android.prod.utils.GlobalClass;


public class FragEmail extends Fragment implements View.OnClickListener {

    private FragEmailBinding mBind;

    public FragEmail() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_email, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mTvYourOwn.setText(getString(R.string.hi) + " " + GlobalClass.getInstance().mPref.getString("first_name", "") + "! " + getString(R.string.email_frag_des));
        mBind.mBtnCallme.setOnClickListener(this);
        mBind.mBtnEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Bundle mBundle = new Bundle();
        switch (view.getId()) {
            case R.id.mBtnCallme:
                mBundle.putString("mFlag", "phone");
                ((MainActivity) getActivity()).fnLoadFragAdd("INPUT EMAIL PHONE", true, mBundle);
                break;
            case R.id.mBtnEmail:
                mBundle.putString("mFlag", "email");
                ((MainActivity) getActivity()).fnLoadFragAdd("INPUT EMAIL PHONE", true, mBundle);
                break;
        }

    }
}
