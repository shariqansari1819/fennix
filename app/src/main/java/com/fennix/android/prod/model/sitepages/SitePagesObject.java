
package com.fennix.android.prod.model.sitepages;

import com.fennix.android.prod.model.sitepages.SitePagesData;

import java.util.List;

public class SitePagesObject {

    private String status;
    private List<SitePagesData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SitePagesData> getData() {
        return data;
    }

    public void setData(List<SitePagesData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
