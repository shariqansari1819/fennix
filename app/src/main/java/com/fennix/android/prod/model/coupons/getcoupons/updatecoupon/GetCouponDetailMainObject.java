
package com.fennix.android.prod.model.coupons.getcoupons.updatecoupon;


public class GetCouponDetailMainObject {

    private String status;
    private GetDetailCouponData data;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GetDetailCouponData getData() {
        return data;
    }

    public void setData(GetDetailCouponData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
