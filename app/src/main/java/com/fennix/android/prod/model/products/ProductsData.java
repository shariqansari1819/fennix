
package com.fennix.android.prod.model.products;

import java.util.HashMap;
import java.util.Map;

public class ProductsData {

    private String pro_id;
    private String pro_title;
    private String proimg_path;
    private String pro_currency;
    private String pro_price;
    private String pro_stock_status;

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_title() {
        return pro_title;
    }

    public void setPro_title(String pro_title) {
        this.pro_title = pro_title;
    }

    public String getProimgPath() {
        return proimg_path;
    }

    public void setProimgPath(String proimgPath) {
        this.proimg_path = proimgPath;
    }

    public String getPro_currency() {
        return pro_currency;
    }

    public void setPro_currency(String pro_currency) {
        this.pro_currency = pro_currency;
    }

    public String getPro_price() {
        return pro_price;
    }

    public void setPro_price(String pro_price) {
        this.pro_price = pro_price;
    }

    public String getPro_stock_status() {
        return pro_stock_status;
    }

    public void setPro_stock_status(String pro_stock_status) {
        this.pro_stock_status = pro_stock_status;
    }

}
