package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.databinding.FragSuccessEmailBinding;
import com.fennix.android.prod.utils.GlobalClass;

public class FragSuccessEmailPhone extends Fragment {

    private FragSuccessEmailBinding mBind;
    private Handler mHandler;

    public FragSuccessEmailPhone() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_success_email, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mHandler = new Handler();
        String message = "";
        if (getArguments().getString("mFlag").equals("refer")) {
            message = getResources().getString(R.string.refer_success);
        } else {
            message = getResources().getString(R.string.success_des);
        }
        mBind.mTvSuccess.setText(message);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getArguments().getString("mFlag", "").equals("refer")) {
                    ((MainActivity) getActivity()).mFragManager.popBackStack();
                } else {
                    ((MainActivity) getActivity()).mFragManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    if (!getArguments().getBoolean("site_exist", false)) {
                        ((MainActivity) getActivity()).fnLoadFragAdd("CREATE SITE", false, null);
                    } else {
                        GlobalClass.getInstance().mPrefEditor.putBoolean("first_time", true);
                        GlobalClass.getInstance().mPrefEditor.apply();
                        ((MainActivity) getActivity()).mPosPrevious = 1;
                        ((MainActivity) getActivity()).mBind.mImgHome.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                        ((MainActivity) getActivity()).fnLoadFragReplace("HOME", null);
                    }
                }
            }
        }, 2000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacksAndMessages(null);
    }
}
