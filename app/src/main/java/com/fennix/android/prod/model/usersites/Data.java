
package com.fennix.android.prod.model.usersites;

import java.util.HashMap;
import java.util.Map;

public class Data {

    private String sites_id;
    private String sites_name;
    private String sitethumb;
    private String custom_domain;
    private String sub_domain;
    private String sub_folder;
    private String created_at;
    private String modified_at;
    private String pages_preview;
    private String is_ssl;

    public String getIs_ssl() {
        return is_ssl;
    }

    public void setIs_ssl(String is_ssl) {
        this.is_ssl = is_ssl;
    }

    public String getSites_id() {
        return sites_id;
    }

    public void setSites_id(String sites_id) {
        this.sites_id = sites_id;
    }

    public String getSites_name() {
        return sites_name;
    }

    public void setSites_name(String sites_name) {
        this.sites_name = sites_name;
    }

    public String getSitethumb() {
        return sitethumb;
    }

    public void setSitethumb(String sitethumb) {
        this.sitethumb = sitethumb;
    }

    public String getCustom_domain() {
        return custom_domain;
    }

    public void setCustom_domain(String custom_domain) {
        this.custom_domain = custom_domain;
    }

    public String getSub_domain() {
        return sub_domain;
    }

    public void setSub_domain(String sub_domain) {
        this.sub_domain = sub_domain;
    }

    public String getSub_folder() {
        return sub_folder;
    }

    public void setSub_folder(String sub_folder) {
        this.sub_folder = sub_folder;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getModified_at() {
        return modified_at;
    }

    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public String getPages_preview() {
        return pages_preview;
    }

    public void setPages_preview(String pages_preview) {
        this.pages_preview = pages_preview;
    }

}
