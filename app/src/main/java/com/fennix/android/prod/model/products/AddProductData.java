
package com.fennix.android.prod.model.products;


public class AddProductData {

    private String pro_id;
    private String pro_site_id;
    private String pro_title;
    private String pro_description;
    private String pro_featured;
    private String pro_video_url;
    private String pro_status;
    private String pro_price;
    private String pro_currency;
    private String pro_sale_status;
    private String pro_discount;
    private Object pro_after_discount;
    private String pro_type;
    private String pro_cat_id;
    private String pro_weight;
    private String pro_stock_status;
    private String pro_updatetime;
    private String pro_addtime;
    private String optionname;

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_site_id() {
        return pro_site_id;
    }

    public void setPro_site_id(String pro_site_id) {
        this.pro_site_id = pro_site_id;
    }

    public String getPro_title() {
        return pro_title;
    }

    public void setPro_title(String pro_title) {
        this.pro_title = pro_title;
    }

    public String getPro_description() {
        return pro_description;
    }

    public void setPro_description(String pro_description) {
        this.pro_description = pro_description;
    }

    public String getPro_featured() {
        return pro_featured;
    }

    public void setPro_featured(String pro_featured) {
        this.pro_featured = pro_featured;
    }

    public String getPro_video_url() {
        return pro_video_url;
    }

    public void setPro_video_url(String pro_video_url) {
        this.pro_video_url = pro_video_url;
    }

    public String getPro_status() {
        return pro_status;
    }

    public void setPro_status(String pro_status) {
        this.pro_status = pro_status;
    }

    public String getPro_price() {
        return pro_price;
    }

    public void setPro_price(String pro_price) {
        this.pro_price = pro_price;
    }

    public String getPro_currency() {
        return pro_currency;
    }

    public void setPro_currency(String pro_currency) {
        this.pro_currency = pro_currency;
    }

    public String getPro_sale_status() {
        return pro_sale_status;
    }

    public void setPro_sale_status(String pro_sale_status) {
        this.pro_sale_status = pro_sale_status;
    }

    public String getPro_discount() {
        return pro_discount;
    }

    public void setPro_discount(String pro_discount) {
        this.pro_discount = pro_discount;
    }

    public Object getPro_after_discount() {
        return pro_after_discount;
    }

    public void setPro_after_discount(Object pro_after_discount) {
        this.pro_after_discount = pro_after_discount;
    }

    public String getPro_type() {
        return pro_type;
    }

    public void setPro_type(String pro_type) {
        this.pro_type = pro_type;
    }

    public String getPro_cat_id() {
        return pro_cat_id;
    }

    public void setPro_cat_id(String pro_cat_id) {
        this.pro_cat_id = pro_cat_id;
    }

    public String getPro_weight() {
        return pro_weight;
    }

    public void setPro_weight(String pro_weight) {
        this.pro_weight = pro_weight;
    }

    public String getPro_stock_status() {
        return pro_stock_status;
    }

    public void setPro_stock_status(String pro_stock_status) {
        this.pro_stock_status = pro_stock_status;
    }

    public String getPro_updatetime() {
        return pro_updatetime;
    }

    public void setPro_updatetime(String pro_updatetime) {
        this.pro_updatetime = pro_updatetime;
    }

    public String getPro_addtime() {
        return pro_addtime;
    }

    public void setPro_addtime(String pro_addtime) {
        this.pro_addtime = pro_addtime;
    }

    public String getOptionname() {
        return optionname;
    }

    public void setOptionname(String optionname) {
        this.optionname = optionname;
    }

}
