package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.model.coupons.getcoupons.GetCouponsData;
import com.fennix.android.prod.model.eventbus.EventBusCouponId;
import com.fennix.android.prod.model.eventbus.EventBusProductId;
import com.fennix.android.prod.model.products.ProductsData;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.CouponsHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<GetCouponsData> getCouponsDataList;

    public CouponsAdapter(Context context, List<GetCouponsData> getCouponsDataList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.getCouponsDataList = getCouponsDataList;
    }

    @NonNull
    @Override
    public CouponsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_coupons, parent, false);
        return new CouponsHolder(view, new CouponsHolder.OnCouponsClick() {
            @Override
            public void rowClick(int position, int id,String clickType) {
                EventBus.getDefault().post(new EventBusCouponId(position,clickType));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull CouponsHolder holder, int position) {
        GetCouponsData getCouponsData = getCouponsDataList.get(position);
        holder.mTvOff.setText("$" + getCouponsData.getCoupon_amount() + " OFF");
        holder.mTvFixRate.setText(getCouponsData.getCoupon_type().equals("0") ? "Percentage" : "Fixed");
        holder.mTvOne.setText(getCouponsData.getCoupon_code());
        if (getCouponsData.getCoupon_status().equals("1")) {
            holder.mTvActive.setText("Active");
            holder.mTvActive.setTextColor(context.getResources().getColor(R.color.success));
        } else {
            holder.mTvActive.setText("Inactive");
            holder.mTvActive.setTextColor(context.getResources().getColor(R.color.red_light));
        }
    }

    @Override
    public int getItemCount() {
        return getCouponsDataList.size();
    }

    static class CouponsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView mImgDelete;
        TextView mTvOff, mTvFixRate, mTvOne, mTvActive;
        private OnCouponsClick onCouponsClick;

        CouponsHolder(View itemView, OnCouponsClick onCouponsClick) {
            super(itemView);
            this.onCouponsClick = onCouponsClick;
            mImgDelete = itemView.findViewById(R.id.mImgDelete);
            mTvOff = itemView.findViewById(R.id.mTvOff);
            mTvFixRate = itemView.findViewById(R.id.mTvFixRate);
            mTvOne = itemView.findViewById(R.id.mTvOne);
            mTvActive = itemView.findViewById(R.id.mTvActive);

            mImgDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.mImgDelete) {
                onCouponsClick.rowClick(getAdapterPosition(), view.getId(), "delete");
            } else {
                onCouponsClick.rowClick(getAdapterPosition(), view.getId(), "edit");
            }
        }

        interface OnCouponsClick {
            void rowClick(int position, int id,String clickType);
        }
    }
}