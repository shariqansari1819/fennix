package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.model.eventbus.EventBusTemplateId;
import com.fennix.android.prod.model.templates.Data;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import com.fennix.android.prod.R;

public class UserTemplateAdapter extends RecyclerView.Adapter<UserTemplateAdapter.UserTemplateViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Data> userTemplatesListData;

    public UserTemplateAdapter(Context context, List<Data> userTemplatesListData) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.userTemplatesListData = userTemplatesListData;
    }

    @NonNull
    @Override
    public UserTemplateViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_templates, parent, false);
        return new UserTemplateViewHolder(view, new UserTemplateViewHolder.OnTemplateClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusTemplateId(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull UserTemplateViewHolder holder, int position) {
        Data data = userTemplatesListData.get(position);
        Glide.with(context)
                .load(((MainActivity) context).mSiteThumbnailBaseUrl + data.getSitethumb())
                .into(holder.mImgTemplate);
        if (userTemplatesListData.get(position).isSelected()) {
            holder.mImgSelected.setVisibility(View.VISIBLE);
        } else {
            holder.mImgSelected.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return userTemplatesListData.size();
    }

    static class UserTemplateViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mImgTemplate, mImgSelected;
        private OnTemplateClick onTemplateClickListener;

        public UserTemplateViewHolder(View itemView, OnTemplateClick onTemplateClickListener) {
            super(itemView);
            this.onTemplateClickListener = onTemplateClickListener;
            mImgTemplate = itemView.findViewById(R.id.mImgTemplate);
            mImgSelected = itemView.findViewById(R.id.mImgSelected);

            mImgTemplate.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onTemplateClickListener.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnTemplateClick {
            void rowClick(int position, int id);
        }
    }
}