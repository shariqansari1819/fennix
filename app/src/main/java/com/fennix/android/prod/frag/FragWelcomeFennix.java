package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragWelcomeFennixBinding;
import com.fennix.android.prod.model.usersites.UserSitesModel;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.R;
import retrofit2.Call;
import retrofit2.Callback;

public class FragWelcomeFennix extends Fragment implements View.OnClickListener {

    FragWelcomeFennixBinding mBind;
    private Call<UserSitesModel> mRetrofitCall;

    public FragWelcomeFennix() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_welcome_fennix, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mBind.mBtnYesCreateSIte.setOnClickListener(this);
        mBind.mBtnNotNowFree.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mRetrofitCall != null && mRetrofitCall.isExecuted()) {
            mRetrofitCall.cancel();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mBtnYesCreateSIte:
                ((MainActivity) getActivity()).fnLoadFragAdd("EMAIL", true, null);
                break;
            case R.id.mBtnNotNowFree:
                funGetUserSites();
                break;
        }
    }

    private void funGetUserSites() {
        mBind.mPb.setVisibility(View.VISIBLE);
        mRetrofitCall = Api.WEB_SERVICE.getUserSites("55555", GlobalClass.getInstance().mPref.getString("id", ""));
        mRetrofitCall.enqueue(new Callback<UserSitesModel>() {
            @Override
            public void onResponse(Call<UserSitesModel> call, retrofit2.Response<UserSitesModel> response) {
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (response != null && response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case "0":
                            ((MainActivity) getActivity()).fnLoadFragAdd("CREATE SITE", true, null);
                            break;
                        case "1":
                            GlobalClass.getInstance().mPrefEditor.putBoolean("first_time", true);
                            GlobalClass.getInstance().mPrefEditor.apply();
                            ((MainActivity) getActivity()).mPosPrevious = 1;
                            ((MainActivity) getActivity()).mBind.mImgHome.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                            ((MainActivity) getActivity()).fnLoadFragReplace("HOME", null);
                            break;
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                }
            }

            @Override
            public void onFailure(Call<UserSitesModel> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                mBind.mPb.setVisibility(View.INVISIBLE);
                if (error != null) {
                    if (error.getMessage().contains("No address associated with hostname")) {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                    }
                } else {
                    GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                }
            }
        });
    }

}