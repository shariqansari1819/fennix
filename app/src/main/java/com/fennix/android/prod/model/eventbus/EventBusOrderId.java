package com.fennix.android.prod.model.eventbus;

public class EventBusOrderId {
    private int position;

    public EventBusOrderId(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


}
