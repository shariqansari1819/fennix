
package com.fennix.android.prod.model.products;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsMainObject {

    private String status;
    private List<ProductsData> data = null;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductsData> getData() {
        return data;
    }

    public void setData(List<ProductsData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
