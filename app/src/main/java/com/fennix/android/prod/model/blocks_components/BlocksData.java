
package com.fennix.android.prod.model.blocks_components;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BlocksData {

    @SerializedName("category_title")
    @Expose
    private String category_title;
    @SerializedName("category_list")
    @Expose
    private List<Category_list> category_list = null;

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public List<Category_list> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<Category_list> category_list) {
        this.category_list = category_list;
    }

}