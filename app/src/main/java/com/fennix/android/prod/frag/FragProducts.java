package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.ConversationAdapter;
import com.fennix.android.prod.adapter.ProductAdapter;
import com.fennix.android.prod.api.Api;
import com.fennix.android.prod.databinding.FragProductsBinding;
import com.fennix.android.prod.model.conversation.ChatConversationData;
import com.fennix.android.prod.model.conversation.ChatConversationObject;
import com.fennix.android.prod.model.eventbus.EventBusGetProduct;
import com.fennix.android.prod.model.eventbus.EventBusProductId;
import com.fennix.android.prod.model.products.ProductsData;
import com.fennix.android.prod.model.products.ProductsMainObject;
import com.fennix.android.prod.utils.GlobalClass;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragProducts extends Fragment implements View.OnClickListener {

    private FragProductsBinding mBind;
    private Call<ProductsMainObject> mCallProducts;
    private List<ProductsData> productsDataList = new ArrayList<>();
    private ProductAdapter productAdapter;

    public FragProducts() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_products, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        EventBus.getDefault().register(this);
        mBind.mRvProducts.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mBind.mRvProducts.setItemAnimator(new DefaultItemAnimator());
        getProducts();
        mBind.mTvAddProducts.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvAddProducts:
                Bundle bundle = new Bundle();
                bundle.putString("product_edit_type", "add");
                ((MainActivity) getActivity()).fnLoadFragAdd("PRODUCT EDIT", true, bundle);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusProductClick(EventBusProductId eventBusProductId) {
        String productId = productsDataList.get(eventBusProductId.getPosition()).getPro_id();
        Bundle bundle = new Bundle();
        bundle.putString("product_edit_type", "edit");
        bundle.putString("product_id", productId);
        ((MainActivity) getActivity()).fnLoadFragAdd("PRODUCT EDIT", true, bundle);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusGetProducts(EventBusGetProduct eventBusGetProduct) {
        getProducts();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCallProducts != null && mCallProducts.isExecuted()) {
            mCallProducts.cancel();
        }
        EventBus.getDefault().unregister(this);
    }

    private void getProducts() {
        try {
            mBind.mPb.setVisibility(View.VISIBLE);
            mCallProducts = Api.WEB_SERVICE.getProductsList("55555", GlobalClass.getInstance().mPref.getString("site_id", ""));
            mCallProducts.enqueue(new Callback<ProductsMainObject>() {
                @Override
                public void onResponse(@NonNull Call<ProductsMainObject> call, @NonNull Response<ProductsMainObject> response) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccessful()) {
                        switch (response.body().getStatus()) {
                            case "1":
                                productsDataList = response.body().getData();
                                if (productsDataList.size() == 0) {
                                    mBind.mTvNoProductFound.setVisibility(View.VISIBLE);
                                } else {
                                    mBind.mTvNoProductFound.setVisibility(View.GONE);
                                }
                                productAdapter = new ProductAdapter(getActivity(), productsDataList);
                                mBind.mRvProducts.setAdapter(productAdapter);
                                mBind.mTvAllProductsCount.setText("(" + response.body().getData().size() + ")");
                                break;
                        }
                    } else {
                        GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.response_is_null), R.color.error);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ProductsMainObject> call, @NonNull Throwable error) {
                    mBind.mPb.setVisibility(View.INVISIBLE);
                    if (error != null && getActivity() != null) {
                        if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.internet_or_server_error), R.color.error);
                        } else {
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, error.getMessage() + "", R.color.error);
                        }
                    } else {
                        if (getActivity() != null)
                            GlobalClass.getInstance().snakeBar(((MainActivity) getActivity()).mBind.mRoot, getString(R.string.error_is_null), R.color.error);
                    }
                }
            });
        } catch (Exception ex) {
            mBind.mPb.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

}