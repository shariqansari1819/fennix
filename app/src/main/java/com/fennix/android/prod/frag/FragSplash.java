package com.fennix.android.prod.frag;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.utils.GlobalClass;

import com.fennix.android.prod.databinding.FragSplashBinding;

public class FragSplash extends Fragment {

    private FragSplashBinding mBind;
    private Handler mHandler;

    public FragSplash() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_splash, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getActivity()).mFragManager.popBackStack();
                if (GlobalClass.getInstance().mPref.getString("language", "").contentEquals("")) {
                    Bundle bundle = new Bundle();
                    bundle.putString("mFlag", "splash");
                    ((MainActivity) getActivity()).fnLoadFragReplace("LANGUAGE", bundle);
                } else {
                    if (GlobalClass.getInstance().mPref.getBoolean("login", false)) {
                        if (getArguments() != null && getArguments().getString("mFlag", "none").equals("message")) {
                            ((MainActivity) getActivity()).mPosPrevious = 4;
                            ((MainActivity) getActivity()).mBind.mImgChat.setImageDrawable(getResources().getDrawable(R.mipmap.icon_chat_selected));
                            ((MainActivity) getActivity()).fnLoadFragReplace("CONVERSATION", null);
                            ((MainActivity) getActivity()).mFScreenName = "CONVERSATION";
                            ((MainActivity) getActivity()).mBind.mBottombar.setVisibility(View.VISIBLE);
                        } else if (!(GlobalClass.getInstance().mPref.getBoolean("first_time", false))) {
                            ((MainActivity) getActivity()).fnLoadFragReplace("WELCOME", null);
                        } else {
                            ((MainActivity) getActivity()).mPosPrevious = 1;
                            ((MainActivity) getActivity()).mBind.mImgHome.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
//                            if (GlobalClass.getInstance().mPref.getString("package_type", "Free").equals("Free")) {
                            if (GlobalClass.getInstance().mPref.getString("page_preview", "").equals("")) {
                                Bundle bundle = new Bundle();
                                bundle.putString("mFlag", "create_site");
                                ((MainActivity) getActivity()).fnLoadFragAdd("SELECT TEMPLATE", false, bundle);
                            } else {
                                ((MainActivity) getActivity()).fnLoadFragReplace("HOME", null);
                            }
//                            }
//                            else {
//                                ((MainActivity) getActivity()).fnLoadFragReplace("PREMIUM HOME", null);
//                            }
                        }
                    } else {
                        ((MainActivity) getActivity()).fnLoadFragReplace("LOGIN", null);
                    }
                }
            }
        }, 1000);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler.removeCallbacksAndMessages(null);
    }

}