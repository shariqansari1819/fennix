package com.fennix.android.prod.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fennix.android.prod.R;
import com.fennix.android.prod.model.eventbus.EventBusOrderId;
import com.fennix.android.prod.model.orders.GetOrdersData;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<GetOrdersData> getOrderList;

    public OrdersAdapter(Context context, List<GetOrdersData> getOrderList) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.getOrderList = getOrderList;
    }

    @NonNull
    @Override
    public OrdersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.cus_orders, parent, false);
        return new OrdersHolder(view, new OrdersHolder.OnOrderClick() {
            @Override
            public void rowClick(int position, int id) {
                EventBus.getDefault().post(new EventBusOrderId(position));
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersHolder holder, int position) {
        GetOrdersData getOrdersData = getOrderList.get(position);

        holder.mTvOrders.setText(getOrdersData.getOrder_name());

        if (getOrdersData.getOrder_status().equals("0")) {
            holder.mTvStatus.setText("Pending");
            holder.mTvStatus.setBackgroundColor(context.getResources().getColor(R.color.colorLightGreen));
        } else if (getOrdersData.getOrder_status().equals("1")) {
            holder.mTvStatus.setText("Processing");
            holder.mTvStatus.setBackgroundColor(context.getResources().getColor(R.color.colorLightGreen));
        } else if (getOrdersData.getOrder_status().equals("2")) {
            holder.mTvStatus.setText("Completed");
            holder.mTvStatus.setBackgroundColor(context.getResources().getColor(R.color.colorLightBlue));
        } else if (getOrdersData.getOrder_status().equals("3")) {
            holder.mTvStatus.setText("Canceled");
            holder.mTvStatus.setBackgroundColor(context.getResources().getColor(R.color.colorLightGreen));
        } else {
            holder.mTvStatus.setText("Refused");
            holder.mTvStatus.setBackgroundColor(context.getResources().getColor(R.color.colorLightGreen));
        }
        holder.mTvOrderPrice.setText("$" + getOrdersData.getPrice());
        holder.mTvOrderNumber.setText("#" + getOrdersData.getOrder_id());
    }


    @Override
    public int getItemCount() {
        return getOrderList.size();
    }

    static class OrdersHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mTvTick;
        TextView mTvOrders, mTvStatus, mTvOrderNumber, mTvOrderPrice;
        private OnOrderClick onOrderClick;

        OrdersHolder(View itemView, OnOrderClick onOrderClick) {
            super(itemView);
            this.onOrderClick = onOrderClick;
            mTvOrders = itemView.findViewById(R.id.mTvOrders);
            mTvStatus = itemView.findViewById(R.id.mTvStatus);
            mTvTick = itemView.findViewById(R.id.mImgTick);
            mTvOrderNumber = itemView.findViewById(R.id.mTvOrdersNo);
            mTvOrderPrice = itemView.findViewById(R.id.mTvOrdersPrice);
            mTvTick.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onOrderClick.rowClick(getAdapterPosition(), view.getId());
        }

        interface OnOrderClick {
            void rowClick(int position, int id);
        }
    }
}

