package com.fennix.android.prod.frag;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.fennix.android.prod.MainActivity;
import com.fennix.android.prod.R;
import com.fennix.android.prod.adapter.AddOptionsAdapter;
import com.fennix.android.prod.databinding.FragProductOptionsBinding;
import com.fennix.android.prod.model.addoptions.AddOptionModel;
import com.fennix.android.prod.model.eventbus.EventBusAddOptions;
import com.fennix.android.prod.model.eventbus.EventBusSelectedOptionSend;
import com.fennix.android.prod.model.eventbus.EventBusSelectedOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class FragProductOptions extends Fragment implements View.OnClickListener {

    private FragProductOptionsBinding mBind;

    private AlertDialog optionsDialog;
    private LayoutInflater layoutInflater;
    private EditText addOptions;
    private AddOptionsAdapter adapter;
    private List<AddOptionModel> addOptionsList = new ArrayList<>();
    private List<String> selectedOptionList = new ArrayList<>();

    public FragProductOptions() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBind = DataBindingUtil.inflate(inflater, R.layout.frag_product_options, container, false);
        return mBind.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mBind.mTvAddOption.setOnClickListener(this);
        layoutInflater = LayoutInflater.from(getActivity());
        adapter = new AddOptionsAdapter(getActivity(), addOptionsList);
        mBind.mRvAddOptions.setAdapter(adapter);
        mBind.mRvAddOptions.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBind.mRvAddOptions.setItemAnimator(new DefaultItemAnimator());
        mBind.mImgSave.setOnClickListener(this);
        mBind.mImgCross.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onOptionsClick(EventBusAddOptions eventBusAddOptions) {
        if (addOptionsList.get(eventBusAddOptions.getmPos()).getSelected()) {
            addOptionsList.get(eventBusAddOptions.getmPos()).setSelected(false);
            adapter.notifyItemChanged(eventBusAddOptions.getmPos());
            for (int i = 0; i < selectedOptionList.size(); i++) {
                String selectedOption = selectedOptionList.get(i);
                String addOption = addOptionsList.get(eventBusAddOptions.getmPos()).getmTvAddOptions();
                if (selectedOption.equalsIgnoreCase(addOption)) {
                    selectedOptionList.remove(i);
                    break;
                }
            }
        } else {
            int position = eventBusAddOptions.getmPos();
            addOptionsList.get(eventBusAddOptions.getmPos()).setSelected(true);
            adapter.notifyItemChanged(eventBusAddOptions.getmPos());
            selectedOptionList.add(addOptionsList.get(position).getmTvAddOptions());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectedOptionsClick(EventBusSelectedOptionSend eventBusSelectedOptionSend) {
        selectedOptionList = eventBusSelectedOptionSend.getSelectedOptionsSendList();
        for (int i = 0; i < selectedOptionList.size(); i++) {
            String selectedOption1 = selectedOptionList.get(i);
            addOptionsList.add(new AddOptionModel(selectedOption1, true));
            adapter.notifyItemInserted(addOptionsList.size() - 1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mTvAddOption:
                AlertDialog.Builder categoryDialogBuilder = new AlertDialog.Builder(getActivity());
                final View dialogCategoryView = layoutInflater.inflate(R.layout.dialog_add_options, null);
                categoryDialogBuilder.setView(dialogCategoryView);
                optionsDialog = categoryDialogBuilder.create();
                optionsDialog.show();
                addOptions = dialogCategoryView.findViewById(R.id.mEtAddOptions);
                Button cancel = dialogCategoryView.findViewById(R.id.mBtnCancel);
                cancel.setOnClickListener(this);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        optionsDialog.dismiss();
                    }
                });
                final Button add = dialogCategoryView.findViewById(R.id.mBtnSave);
                add.setOnClickListener(this);
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        optionsDialog.dismiss();
                        String option = addOptions.getText().toString().trim();
                        if (!option.isEmpty()) {
                            addOptionsList.add(new AddOptionModel(option, false));
                            adapter.notifyItemInserted(addOptionsList.size() - 1);
                        }
                    }
                });
                break;
            case R.id.mImgSave:
                if (addOptions != null) {
                    ((MainActivity) getActivity()).fnHideKeyboardFrom(addOptions);
                }
                EventBus.getDefault().post(new EventBusSelectedOptions(selectedOptionList));
                getActivity().onBackPressed();
                break;
            case R.id.mImgCross:
                getActivity().onBackPressed();
                break;
        }
    }

}


